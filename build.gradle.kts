plugins {
    application
    kotlin("jvm") version "1.4.32"
    id("org.jetbrains.dokka") version "1.4.30"
    id("org.jlleitschuh.gradle.ktlint") version "9.4.1"
    id("org.jlleitschuh.gradle.ktlint-idea") version "9.4.1"
}

group = "org.getme"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
    // maven("https://dl.bintray.com/kotlin/dokka")
}

val exposedVersion: String by project
val ktorVersion: String by project
val logbackVersion: String by project
val flywayVersion: String by project
val oauth1SignerVersion: String by project
val clientEncryptionVersion: String by project

application {
    mainClass.set("AppMainKt")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=true")
}

dependencies {

    implementation(kotlin("stdlib"))

    // modules that creates a server
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-gson:$ktorVersion")

    // Use the Ktor Auth to support JWT
    implementation("io.ktor:ktor-auth:$ktorVersion")
    implementation("io.ktor:ktor-auth-jwt:$ktorVersion")

    // Use the Ktor Websockets
    implementation("io.ktor:ktor-client-websockets:$ktorVersion")
    implementation("io.ktor:ktor-network:$ktorVersion")

    // Use the Ktor web client library
    implementation("io.ktor:ktor-client-core:$ktorVersion")
    implementation("io.ktor:ktor-client-gson:$ktorVersion")
    // implementation("io.ktor:ktor-client-cio:$ktorVersion")
    implementation("io.ktor:ktor-client-cio:$ktorVersion")

    implementation("io.ktor:ktor-client-logging:$ktorVersion")

    // Use the Ktor Client authentication library
    implementation("io.ktor:ktor-client-auth:$ktorVersion")

    // module that loads environment variables from a .env file
    implementation("io.github.cdimascio:dotenv-kotlin:6.2.2")

    // module that adds JDBC connection pool
    implementation("org.postgresql:postgresql:42.2.2")
    implementation("com.zaxxer:HikariCP:4.0.1")

    // module that add ORM on database operation
    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jodatime:$exposedVersion")

    // modules adds database migration feature
    implementation("org.flywaydb:flyway-core:$flywayVersion")
    // module that logs at deployment time

    // A Java standalone implementation of the bcrypt password hash function.
    implementation("at.favre.lib:bcrypt:0.9.0")
    dokkaHtmlPlugin("org.jetbrains.dokka:kotlin-as-java-plugin:1.4.30")

    /**
     * Java library for Javascript Object Signing and Encryption (JOSE) and JSON Web Tokens (JWT)
     */
    implementation("com.nimbusds:nimbus-jose-jwt:9.10")
    // A AWS SDK
    implementation(platform("software.amazon.awssdk:bom:2.16.59"))
    implementation("software.amazon.awssdk:cognitoidentityprovider")

    // A Mastercard API OAuth Signer
    implementation("com.mastercard.developer:oauth1-signer:$oauth1SignerVersion")
    implementation("com.mastercard.developer:client-encryption:$clientEncryptionVersion")

    // Use the validations library
    implementation("io.konform:konform-jvm:0.2.0")

    implementation("ch.qos.logback:logback-classic:1.2.3")

    implementation("junit:junit:4.13.1")
    testImplementation("io.ktor:ktor-server-test-host:$ktorVersion")
}

ktlint {
    verbose.set(true)
    outputToConsole.set(true)
    coloredOutput.set(true)
    reporters {
        reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.CHECKSTYLE)
        reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.JSON)
        reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.HTML)
    }
    filter {
        exclude("**/Ktlint.kt")
    }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().all {
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.useIR = true
}

tasks.dokkaHtml.configure {
    outputDirectory.set(buildDir.resolve("dokka"))
}
