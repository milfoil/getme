
import com.typesafe.config.ConfigFactory
import database.DatabaseMain
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.config.HoconApplicationConfig
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.gson.gson
import org.slf4j.event.Level

fun Application.appTestMain() {
    val env = HoconApplicationConfig(ConfigFactory.load())
    val environment = env.config("ktor").property("environment").getString()
    val databaseConfig = env.config("database.$environment")

    // configure database connection
    install(DatabaseMain) {
        username = databaseConfig.property("username").getString()
        password = databaseConfig.property("password").getString()
        host = databaseConfig.property("host").getString()
        port = databaseConfig.property("port").getString().toInt()
        database = databaseConfig.property("database").getString()
        driver = databaseConfig.property("driver").getString()
    }
    // configure logging
    install(CallLogging) {
        level = Level.INFO
        format { call -> call.request.receiveChannel().toString() }
    }

    // Configure HTTP Request & Response
    install(DefaultHeaders) {
        header(name = "X-XSS-Protection", "1; mode=block")
        header(name = "Strict-Transport-Security", "max-age=31536000; includeSubDomains; preload")
        header(name = "X-Content-Type-Options", "nosniff")
    }

    //  Configure JSON content handling
    install(ContentNegotiation) {
        gson {
            setPrettyPrinting()
            serializeNulls()
        }
    }

    // configure routers
    appRouter()
}
