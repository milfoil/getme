package user.service

import appTestMain
import io.ktor.application.Application
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication
import utils.loggerUtil
import kotlin.test.Test
import kotlin.test.assertEquals

val user =
    """{
                  "phoneNumber": "0738669361",
                | "idNumber": "9404065208089",
                | "password": "Qb&UExs21Wp]sZ]" }"""

@Test
fun registerUserTest() = withTestApplication(Application::appTestMain) {
    with(
        handleRequest(HttpMethod.Post, "/register") {
            setBody(user)
            addHeader("Accept", "application/json")
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
        }
    ) {
        loggerUtil("Register User Test Response", response.content.toString())
        assertEquals(HttpStatusCode.Created, response.status())
    }
}
