CREATE TABLE wallet
(
    id                BIGSERIAL PRIMARY KEY                 NOT NULL,
    user_id           UUID                                  NOT NULL,
    account_number    VARCHAR(100) UNIQUE                   NOT NULL,
    alias             VARCHAR(50) NULL,
    withdrawal_limit  INT       DEFAULT 4000                NOT NULL,
    transfer_limit    INT       DEFAULT 24000               NOT NULL,
    payment_limit     INT       DEFAULT 4000                NOT NULL,
    deposit_limit     INT       DEFAULT 5000                NOT NULL,
    available_balance DECIMAL(18, 2)                        NOT NULL,
    current_balance   DECIMAL(18, 2)                        NOT NULL,
    currency_id       INT                                   NOT NULL,
    reserved_amount   DECIMAL(18, 2)                        NOT NULL,
    created_at        TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    updated_at        TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    blocked_at        TIMESTAMP NULL,

    CONSTRAINT fk_user
        FOREIGN KEY (user_id)
            REFERENCES "user" (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,

    CONSTRAINT fk_currency
        FOREIGN KEY (currency_id)
            REFERENCES currency (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);