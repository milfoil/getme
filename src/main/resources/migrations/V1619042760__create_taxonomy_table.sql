CREATE TABLE taxonomy
(
    id          SMALLSERIAL PRIMARY KEY               NOT NULL,
    name        VARCHAR(160)                          NOT NULL,
    description VARCHAR(255)                          NULL,
    slug        VARCHAR(255)                          NULL,
    enabled     BOOL      DEFAULT (TRUE)              NULL,
    created_at  TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    updated_at  TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL
);

INSERT INTO taxonomy (id, name, description, slug)
VALUES ('1', 'User Account Types','User types assigned to the user account. Each user account is assigned at least one user type for which a series of permissions are granted.','user_types'),
       ('2', 'User Account Status', 'User account status assigned to the user account. Each user account is assigned at least one status for which a series of permissions are granted','user_account_status'),
       ('3', 'Phone Line Types', 'User account status assigned to the user account. Each user account is assigned at least one status for which a series of permissions are granted','user_account_status'),
       ('4', 'Address Types', 'represent an address type','user_account_status');