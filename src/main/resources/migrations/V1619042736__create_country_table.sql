CREATE TABLE country
(
    id            SMALLSERIAL PRIMARY KEY               NOT NULL,
    name          VARCHAR(180)                          NOT NULL,
    alpha2_code   VARCHAR(4)                            NOT NULL,
    alpha3_code   VARCHAR(3)                            NOT NULL,
    capital       VARCHAR(36)                           NOT NULL,
    region        VARCHAR(36)                           NOT NULL,
    flag          VARCHAR(255)                          NOT NULL,
    native_name   VARCHAR(255)                          NOT NULL,
    dialling_code VARCHAR(6)                            NOT NULL,
    latitude      VARCHAR(255)                          NULL,
    longitude     VARCHAR(255)                          NULL,
    created_at    TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    updated_at    TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    deleted_at    TIMESTAMP                             NULL
);

INSERT INTO country (id, name, alpha3_code, alpha2_code, capital, region, flag, native_name,
                     dialling_code, latitude, longitude)
VALUES ('1', 'Afghanistan', 'AFG', 'AF', 'Kabul', 'Asia', 'https://restcountries.eu/data/afg.svg', 'افغانستان', '+93',
        '33', '65'),
       ('2', 'Åland Islands', 'ALA', 'AX', 'Mariehamn', 'Europe', 'https://restcountries.eu/data/ala.svg', 'Åland',
        '+358', '60.116667', '19.9'),
       ('3', 'Albania', 'ALB', 'AL', 'Tirana', 'Europe', 'https://restcountries.eu/data/alb.svg', 'Shqipëria', '+355',
        '41', '20'),
       ('4', 'Algeria', 'DZA', 'DZ', 'Algiers', 'Africa', 'https://restcountries.eu/data/dza.svg', 'الجزائر', '+213',
        '28', '3'),
       ('5', 'American Samoa', 'ASM', 'AS', 'Pago Pago', 'Oceania', 'https://restcountries.eu/data/asm.svg',
        'American Samoa', '+1684', '-14.33333333', '-170'),
       ('6', 'Andorra', 'AND', 'AD', 'Andorra la Vella', 'Europe', 'https://restcountries.eu/data/and.svg', 'Andorra',
        '+376', '42.5', '1.5'),
       ('7', 'Angola', 'AGO', 'AO', 'Luanda', 'Africa', 'https://restcountries.eu/data/ago.svg', 'Angola', '+244',
        '-12.5', '18.5'),
       ('8', 'Anguilla', 'AIA', 'AI', 'The Valley', 'Americas', 'https://restcountries.eu/data/aia.svg', 'Anguilla',
        '+1264', '18.25', '-63.16666666'),
       ('9', 'Antarctica', 'ATA', 'AQ', '', 'Polar', 'https://restcountries.eu/data/ata.svg', 'Antarctica', '+672',
        '-74.65', '4.48'),
       ('10', 'Antigua and Barbuda', 'ATG', 'AG', 'Saint John''s', 'Americas', 'https://restcountries.eu/data/atg.svg',
        'Antigua and Barbuda', '+1268', '17.05', '-61.8'),
       ('11', 'Argentina', 'ARG', 'AR', 'Buenos Aires', 'Americas', 'https://restcountries.eu/data/arg.svg',
        'Argentina', '+54', '-34', '-64'),
       ('12', 'Armenia', 'ARM', 'AM', 'Yerevan', 'Asia', 'https://restcountries.eu/data/arm.svg', 'Հայաստան', '+374',
        '40', '45'),
       ('13', 'Aruba', 'ABW', 'AW', 'Oranjestad', 'Americas', 'https://restcountries.eu/data/abw.svg', 'Aruba', '+297',
        '12.5', '-69.96666666'),
       ('14', 'Australia', 'AUS', 'AU', 'Canberra', 'Oceania', 'https://restcountries.eu/data/aus.svg', 'Australia',
        '+61', '-27', '133'),
       ('15', 'Austria', 'AUT', 'AT', 'Vienna', 'Europe', 'https://restcountries.eu/data/aut.svg', 'Österreich', '+43',
        '47.33333333', '13.33333333'),
       ('16', 'Azerbaijan', 'AZE', 'AZ', 'Baku', 'Asia', 'https://restcountries.eu/data/aze.svg', 'Azərbaycan', '+994',
        '40.5', '47.5'),
       ('17', 'Bahamas', 'BHS', 'BS', 'Nassau', 'Americas', 'https://restcountries.eu/data/bhs.svg', 'Bahamas', '+1242',
        '24.25', '-76'),
       ('18', 'Bahrain', 'BHR', 'BH', 'Manama', 'Asia', 'https://restcountries.eu/data/bhr.svg', '‏البحرين', '+973',
        '26', '50.55'),
       ('19', 'Bangladesh', 'BGD', 'BD', 'Dhaka', 'Asia', 'https://restcountries.eu/data/bgd.svg', 'Bangladesh', '+880',
        '24', '90'),
       ('20', 'Barbados', 'BRB', 'BB', 'Bridgetown', 'Americas', 'https://restcountries.eu/data/brb.svg', 'Barbados',
        '+1246', '13.16666666', '-59.53333333'),
       ('21', 'Belarus', 'BLR', 'BY', 'Minsk', 'Europe', 'https://restcountries.eu/data/blr.svg', 'Белару́сь', '+375',
        '53', '28'),
       ('22', 'Belgium', 'BEL', 'BE', 'Brussels', 'Europe', 'https://restcountries.eu/data/bel.svg', 'België', '+32',
        '50.83333333', '4'),
       ('23', 'Belize', 'BLZ', 'BZ', 'Belmopan', 'Americas', 'https://restcountries.eu/data/blz.svg', 'Belize', '+501',
        '17.25', '-88.75'),
       ('24', 'Benin', 'BEN', 'BJ', 'Porto-Novo', 'Africa', 'https://restcountries.eu/data/ben.svg', 'Bénin', '+229',
        '9.5', '2.25'),
       ('25', 'Bermuda', 'BMU', 'BM', 'Hamilton', 'Americas', 'https://restcountries.eu/data/bmu.svg', 'Bermuda',
        '+1441', '32.33333333', '-64.75'),
       ('26', 'Bhutan', 'BTN', 'BT', 'Thimphu', 'Asia', 'https://restcountries.eu/data/btn.svg', 'ʼbrug-yul', '+975',
        '27.5', '90.5'),
       ('27', 'Bolivia (Plurinational State of)', 'BOL', 'BO', 'Sucre', 'Americas',
        'https://restcountries.eu/data/bol.svg', 'Bolivia', '+591', '-17', '-65'),
       ('28', 'Bonaire, Sint Eustatius and Saba', 'BES', 'BQ', 'Kralendijk', 'Americas',
        'https://restcountries.eu/data/bes.svg', 'Bonaire', '+5997', '12.15', '-68.266667'),
       ('29', 'Bosnia and Herzegovina', 'BIH', 'BA', 'Sarajevo', 'Europe', 'https://restcountries.eu/data/bih.svg',
        'Bosna i Hercegovina', '+387', '44', '18'),
       ('30', 'Botswana', 'BWA', 'BW', 'Gaborone', 'Africa', 'https://restcountries.eu/data/bwa.svg', 'Botswana',
        '+267', '-22', '24'),
       ('31', 'Bouvet Island', 'BVT', 'BV', '', '', 'https://restcountries.eu/data/bvt.svg', 'Bouvetøya', '+',
        '-54.43333333', '3.4'),
       ('32', 'Brazil', 'BRA', 'BR', 'Brasília', 'Americas', 'https://restcountries.eu/data/bra.svg', 'Brasil', '+55',
        '-10', '-55'),
       ('33', 'British Indian Ocean Territory', 'IOT', 'IO', 'Diego Garcia', 'Africa',
        'https://restcountries.eu/data/iot.svg', 'British Indian Ocean Territory', '+246', '-6', '71.5'),
       ('34', 'United States Minor Outlying Islands', 'UMI', 'UM', '', 'Americas',
        'https://restcountries.eu/data/umi.svg', 'United States Minor Outlying Islands', '+', NULL, NULL),
       ('35', 'Virgin Islands (British)', 'VGB', 'VG', 'Road Town', 'Americas', 'https://restcountries.eu/data/vgb.svg',
        'British Virgin Islands', '+1284', '18.431383', '-64.62305'),
       ('36', 'Virgin Islands (U.S.)', 'VIR', 'VI', 'Charlotte Amalie', 'Americas',
        'https://restcountries.eu/data/vir.svg', 'Virgin Islands of the United States', '+1340', '18.34', '-64.93'),
       ('37', 'Brunei Darussalam', 'BRN', 'BN', 'Bandar Seri Begawan', 'Asia', 'https://restcountries.eu/data/brn.svg',
        'Negara Brunei Darussalam', '+673', '4.5', '114.66666666'),
       ('38', 'Bulgaria', 'BGR', 'BG', 'Sofia', 'Europe', 'https://restcountries.eu/data/bgr.svg', 'България', '+359',
        '43', '25'),
       ('39', 'Burkina Faso', 'BFA', 'BF', 'Ouagadougou', 'Africa', 'https://restcountries.eu/data/bfa.svg',
        'Burkina Faso', '+226', '13', '-2'),
       ('40', 'Burundi', 'BDI', 'BI', 'Bujumbura', 'Africa', 'https://restcountries.eu/data/bdi.svg', 'Burundi', '+257',
        '-3.5', '30'),
       ('41', 'Cambodia', 'KHM', 'KH', 'Phnom Penh', 'Asia', 'https://restcountries.eu/data/khm.svg', 'Kâmpŭchéa',
        '+855', '13', '105'),
       ('42', 'Cameroon', 'CMR', 'CM', 'Yaoundé', 'Africa', 'https://restcountries.eu/data/cmr.svg', 'Cameroon', '+237',
        '6', '12'),
       ('43', 'Canada', 'CAN', 'CA', 'Ottawa', 'Americas', 'https://restcountries.eu/data/can.svg', 'Canada', '+1',
        '60', '-95'),
       ('44', 'Cabo Verde', 'CPV', 'CV', 'Praia', 'Africa', 'https://restcountries.eu/data/cpv.svg', 'Cabo Verde',
        '+238', '16', '-24'),
       ('45', 'Cayman Islands', 'CYM', 'KY', 'George Town', 'Americas', 'https://restcountries.eu/data/cym.svg',
        'Cayman Islands', '+1345', '19.5', '-80.5'),
       ('46', 'Central African Republic', 'CAF', 'CF', 'Bangui', 'Africa', 'https://restcountries.eu/data/caf.svg',
        'Ködörösêse tî Bêafrîka', '+236', '7', '21'),
       ('47', 'Chad', 'TCD', 'TD', 'N''Djamena', 'Africa', 'https://restcountries.eu/data/tcd.svg', 'Tchad', '+235',
        '15', '19'),
       ('48', 'Chile', 'CHL', 'CL', 'Santiago', 'Americas', 'https://restcountries.eu/data/chl.svg', 'Chile', '+56',
        '-30', '-71'),
       ('49', 'China', 'CHN', 'CN', 'Beijing', 'Asia', 'https://restcountries.eu/data/chn.svg', '中国', '+86', '35',
        '105'),
       ('50', 'Christmas Island', 'CXR', 'CX', 'Flying Fish Cove', 'Oceania', 'https://restcountries.eu/data/cxr.svg',
        'Christmas Island', '+61', '-10.5', '105.66666666'),
       ('51', 'Cocos (Keeling) Islands', 'CCK', 'CC', 'West Island', 'Oceania', 'https://restcountries.eu/data/cck.svg',
        'Cocos (Keeling) Islands', '+61', '-12.5', '96.83333333'),
       ('52', 'Colombia', 'COL', 'CO', 'Bogotá', 'Americas', 'https://restcountries.eu/data/col.svg', 'Colombia', '+57',
        '4', '-72'),
       ('53', 'Comoros', 'COM', 'KM', 'Moroni', 'Africa', 'https://restcountries.eu/data/com.svg', 'Komori', '+269',
        '-12.16666666', '44.25'),
       ('54', 'Congo', 'COG', 'CG', 'Brazzaville', 'Africa', 'https://restcountries.eu/data/cog.svg',
        'République du Congo', '+242', '-1', '15'),
       ('55', 'Congo (Democratic Republic of the)', 'COD', 'CD', 'Kinshasa', 'Africa',
        'https://restcountries.eu/data/cod.svg', 'République démocratique du Congo', '+243', '0', '25'),
       ('56', 'Cook Islands', 'COK', 'CK', 'Avarua', 'Oceania', 'https://restcountries.eu/data/cok.svg', 'Cook Islands',
        '+682', '-21.23333333', '-159.76666666'),
       ('57', 'Costa Rica', 'CRI', 'CR', 'San José', 'Americas', 'https://restcountries.eu/data/cri.svg', 'Costa Rica',
        '+506', '10', '-84'),
       ('58', 'Croatia', 'HRV', 'HR', 'Zagreb', 'Europe', 'https://restcountries.eu/data/hrv.svg', 'Hrvatska', '+385',
        '45.16666666', '15.5'),
       ('59', 'Cuba', 'CUB', 'CU', 'Havana', 'Americas', 'https://restcountries.eu/data/cub.svg', 'Cuba', '+53', '21.5',
        '-80'),
       ('60', 'Curaçao', 'CUW', 'CW', 'Willemstad', 'Americas', 'https://restcountries.eu/data/cuw.svg', 'Curaçao',
        '+599', '12.116667', '-68.933333'),
       ('61', 'Cyprus', 'CYP', 'CY', 'Nicosia', 'Europe', 'https://restcountries.eu/data/cyp.svg', 'Κύπρος', '+357',
        '35', '33'),
       ('62', 'Czech Republic', 'CZE', 'CZ', 'Prague', 'Europe', 'https://restcountries.eu/data/cze.svg',
        'Česká republika', '+420', '49.75', '15.5'),
       ('63', 'Denmark', 'DNK', 'DK', 'Copenhagen', 'Europe', 'https://restcountries.eu/data/dnk.svg', 'Danmark', '+45',
        '56', '10'),
       ('64', 'Djibouti', 'DJI', 'DJ', 'Djibouti', 'Africa', 'https://restcountries.eu/data/dji.svg', 'Djibouti',
        '+253', '11.5', '43'),
       ('65', 'Dominica', 'DMA', 'DM', 'Roseau', 'Americas', 'https://restcountries.eu/data/dma.svg', 'Dominica',
        '+1767', '15.41666666', '-61.33333333'),
       ('66', 'Dominican Republic', 'DOM', 'DO', 'Santo Domingo', 'Americas', 'https://restcountries.eu/data/dom.svg',
        'República Dominicana', '+1809', '19', '-70.66666666'),
       ('67', 'Ecuador', 'ECU', 'EC', 'Quito', 'Americas', 'https://restcountries.eu/data/ecu.svg', 'Ecuador', '+593',
        '-2', '-77.5'),
       ('68', 'Egypt', 'EGY', 'EG', 'Cairo', 'Africa', 'https://restcountries.eu/data/egy.svg', 'مصر‎', '+20', '27',
        '30'),
       ('69', 'El Salvador', 'SLV', 'SV', 'San Salvador', 'Americas', 'https://restcountries.eu/data/slv.svg',
        'El Salvador', '+503', '13.83333333', '-88.91666666'),
       ('70', 'Equatorial Guinea', 'GNQ', 'GQ', 'Malabo', 'Africa', 'https://restcountries.eu/data/gnq.svg',
        'Guinea Ecuatorial', '+240', '2', '10'),
       ('71', 'Eritrea', 'ERI', 'ER', 'Asmara', 'Africa', 'https://restcountries.eu/data/eri.svg', 'ኤርትራ', '+291', '15',
        '39'),
       ('72', 'Estonia', 'EST', 'EE', 'Tallinn', 'Europe', 'https://restcountries.eu/data/est.svg', 'Eesti', '+372',
        '59', '26'),
       ('73', 'Ethiopia', 'ETH', 'ET', 'Addis Ababa', 'Africa', 'https://restcountries.eu/data/eth.svg', 'ኢትዮጵያ',
        '+251', '8', '38'),
       ('74', 'Falkland Islands (Malvinas)', 'FLK', 'FK', 'Stanley', 'Americas',
        'https://restcountries.eu/data/flk.svg', 'Falkland Islands', '+500', '-51.75', '-59'),
       ('75', 'Faroe Islands', 'FRO', 'FO', 'Tórshavn', 'Europe', 'https://restcountries.eu/data/fro.svg', 'Føroyar',
        '+298', '62', '-7'),
       ('76', 'Fiji', 'FJI', 'FJ', 'Suva', 'Oceania', 'https://restcountries.eu/data/fji.svg', 'Fiji', '+679', '-18',
        '175'),
       ('77', 'Finland', 'FIN', 'FI', 'Helsinki', 'Europe', 'https://restcountries.eu/data/fin.svg', 'Suomi', '+358',
        '64', '26'),
       ('78', 'France', 'FRA', 'FR', 'Paris', 'Europe', 'https://restcountries.eu/data/fra.svg', 'France', '+33', '46',
        '2'),
       ('79', 'French Guiana', 'GUF', 'GF', 'Cayenne', 'Americas', 'https://restcountries.eu/data/guf.svg',
        'Guyane française', '+594', '4', '-53'),
       ('80', 'French Polynesia', 'PYF', 'PF', 'Papeetē', 'Oceania', 'https://restcountries.eu/data/pyf.svg',
        'Polynésie française', '+689', '-15', '-140'),
       ('81', 'French Southern Territories', 'ATF', 'TF', 'Port-aux-Français', 'Africa',
        'https://restcountries.eu/data/atf.svg', 'Territoire des Terres australes et antarctiques françaises', '+',
        '-49.25', '69.167'),
       ('82', 'Gabon', 'GAB', 'GA', 'Libreville', 'Africa', 'https://restcountries.eu/data/gab.svg', 'Gabon', '+241',
        '-1', '11.75'),
       ('83', 'Gambia', 'GMB', 'GM', 'Banjul', 'Africa', 'https://restcountries.eu/data/gmb.svg', 'Gambia', '+220',
        '13.46666666', '-16.56666666'),
       ('84', 'Georgia', 'GEO', 'GE', 'Tbilisi', 'Asia', 'https://restcountries.eu/data/geo.svg', 'საქართველო', '+995',
        '42', '43.5'),
       ('85', 'Germany', 'DEU', 'DE', 'Berlin', 'Europe', 'https://restcountries.eu/data/deu.svg', 'Deutschland', '+49',
        '51', '9'),
       ('86', 'Ghana', 'GHA', 'GH', 'Accra', 'Africa', 'https://restcountries.eu/data/gha.svg', 'Ghana', '+233', '8',
        '-2'),
       ('87', 'Gibraltar', 'GIB', 'GI', 'Gibraltar', 'Europe', 'https://restcountries.eu/data/gib.svg', 'Gibraltar',
        '+350', '36.13333333', '-5.35'),
       ('88', 'Greece', 'GRC', 'GR', 'Athens', 'Europe', 'https://restcountries.eu/data/grc.svg', 'Ελλάδα', '+30', '39',
        '22'),
       ('89', 'Greenland', 'GRL', 'GL', 'Nuuk', 'Americas', 'https://restcountries.eu/data/grl.svg', 'Kalaallit Nunaat',
        '+299', '72', '-40'),
       ('90', 'Grenada', 'GRD', 'GD', 'St. George''s', 'Americas', 'https://restcountries.eu/data/grd.svg', 'Grenada',
        '+1473', '12.11666666', '-61.66666666'),
       ('91', 'Guadeloupe', 'GLP', 'GP', 'Basse-Terre', 'Americas', 'https://restcountries.eu/data/glp.svg',
        'Guadeloupe', '+590', '16.25', '-61.583333'),
       ('92', 'Guam', 'GUM', 'GU', 'Hagåtña', 'Oceania', 'https://restcountries.eu/data/gum.svg', 'Guam', '+1671',
        '13.46666666', '144.78333333'),
       ('93', 'Guatemala', 'GTM', 'GT', 'Guatemala City', 'Americas', 'https://restcountries.eu/data/gtm.svg',
        'Guatemala', '+502', '15.5', '-90.25'),
       ('94', 'Guernsey', 'GGY', 'GG', 'St. Peter Port', 'Europe', 'https://restcountries.eu/data/ggy.svg', 'Guernsey',
        '+44', '49.46666666', '-2.58333333'),
       ('95', 'Guinea', 'GIN', 'GN', 'Conakry', 'Africa', 'https://restcountries.eu/data/gin.svg', 'Guinée', '+224',
        '11', '-10'),
       ('96', 'Guinea-Bissau', 'GNB', 'GW', 'Bissau', 'Africa', 'https://restcountries.eu/data/gnb.svg', 'Guiné-Bissau',
        '+245', '12', '-15'),
       ('97', 'Guyana', 'GUY', 'GY', 'Georgetown', 'Americas', 'https://restcountries.eu/data/guy.svg', 'Guyana',
        '+592', '5', '-59'),
       ('98', 'Haiti', 'HTI', 'HT', 'Port-au-Prince', 'Americas', 'https://restcountries.eu/data/hti.svg', 'Haïti',
        '+509', '19', '-72.41666666'),
       ('99', 'Heard Island and McDonald Islands', 'HMD', 'HM', '', '', 'https://restcountries.eu/data/hmd.svg',
        'Heard Island and McDonald Islands', '+', '-53.1', '72.51666666'),
       ('100', 'Holy See', 'VAT', 'VA', 'Rome', 'Europe', 'https://restcountries.eu/data/vat.svg', 'Sancta Sedes',
        '+379', '41.9', '12.45'),
       ('101', 'Honduras', 'HND', 'HN', 'Tegucigalpa', 'Americas', 'https://restcountries.eu/data/hnd.svg', 'Honduras',
        '+504', '15', '-86.5'),
       ('102', 'Hong Kong', 'HKG', 'HK', 'City of Victoria', 'Asia', 'https://restcountries.eu/data/hkg.svg', '香港',
        '+852', '22.25', '114.16666666'),
       ('103', 'Hungary', 'HUN', 'HU', 'Budapest', 'Europe', 'https://restcountries.eu/data/hun.svg', 'Magyarország',
        '+36', '47', '20'),
       ('104', 'Iceland', 'ISL', 'IS', 'Reykjavík', 'Europe', 'https://restcountries.eu/data/isl.svg', 'Ísland', '+354',
        '65', '-18'),
       ('105', 'India', 'IND', 'IN', 'New Delhi', 'Asia', 'https://restcountries.eu/data/ind.svg', 'भारत', '+91', '20',
        '77'),
       ('106', 'Indonesia', 'IDN', 'ID', 'Jakarta', 'Asia', 'https://restcountries.eu/data/idn.svg', 'Indonesia', '+62',
        '-5', '120'),
       ('107', 'Côte d''Ivoire', 'CIV', 'CI', 'Yamoussoukro', 'Africa', 'https://restcountries.eu/data/civ.svg',
        'Côte d''Ivoire', '+225', '8', '-5'),
       ('108', 'Iran (Islamic Republic of)', 'IRN', 'IR', 'Tehran', 'Asia', 'https://restcountries.eu/data/irn.svg',
        'ایران', '+98', '32', '53'),
       ('109', 'Iraq', 'IRQ', 'IQ', 'Baghdad', 'Asia', 'https://restcountries.eu/data/irq.svg', 'العراق', '+964', '33',
        '44'),
       ('110', 'Ireland', 'IRL', 'IE', 'Dublin', 'Europe', 'https://restcountries.eu/data/irl.svg', 'Éire', '+353',
        '53', '-8'),
       ('111', 'Isle of Man', 'IMN', 'IM', 'Douglas', 'Europe', 'https://restcountries.eu/data/imn.svg', 'Isle of Man',
        '+44', '54.25', '-4.5'),
       ('112', 'Israel', 'ISR', 'IL', 'Jerusalem', 'Asia', 'https://restcountries.eu/data/isr.svg', 'יִשְׂרָאֵל',
        '+972', '31.5', '34.75'),
       ('113', 'Italy', 'ITA', 'IT', 'Rome', 'Europe', 'https://restcountries.eu/data/ita.svg', 'Italia', '+39',
        '42.83333333', '12.83333333'),
       ('114', 'Jamaica', 'JAM', 'JM', 'Kingston', 'Americas', 'https://restcountries.eu/data/jam.svg', 'Jamaica',
        '+1876', '18.25', '-77.5'),
       ('115', 'Japan', 'JPN', 'JP', 'Tokyo', 'Asia', 'https://restcountries.eu/data/jpn.svg', '日本', '+81', '36',
        '138'),
       ('116', 'Jersey', 'JEY', 'JE', 'Saint Helier', 'Europe', 'https://restcountries.eu/data/jey.svg', 'Jersey',
        '+44', '49.25', '-2.16666666'),
       ('117', 'Jordan', 'JOR', 'JO', 'Amman', 'Asia', 'https://restcountries.eu/data/jor.svg', 'الأردن', '+962', '31',
        '36'),
       ('118', 'Kazakhstan', 'KAZ', 'KZ', 'Astana', 'Asia', 'https://restcountries.eu/data/kaz.svg', 'Қазақстан', '+76',
        '48', '68'),
       ('119', 'Kenya', 'KEN', 'KE', 'Nairobi', 'Africa', 'https://restcountries.eu/data/ken.svg', 'Kenya', '+254', '1',
        '38'),
       ('120', 'Kiribati', 'KIR', 'KI', 'South Tarawa', 'Oceania', 'https://restcountries.eu/data/kir.svg', 'Kiribati',
        '+686', '1.41666666', '173'),
       ('121', 'Kuwait', 'KWT', 'KW', 'Kuwait City', 'Asia', 'https://restcountries.eu/data/kwt.svg', 'الكويت', '+965',
        '29.5', '45.75'),
       ('122', 'Kyrgyzstan', 'KGZ', 'KG', 'Bishkek', 'Asia', 'https://restcountries.eu/data/kgz.svg', 'Кыргызстан',
        '+996', '41', '75'),
       ('123', 'Lao People''s Democratic Republic', 'LAO', 'LA', 'Vientiane', 'Asia',
        'https://restcountries.eu/data/lao.svg', 'ສປປລາວ', '+856', '18', '105'),
       ('124', 'Latvia', 'LVA', 'LV', 'Riga', 'Europe', 'https://restcountries.eu/data/lva.svg', 'Latvija', '+371',
        '57', '25'),
       ('125', 'Lebanon', 'LBN', 'LB', 'Beirut', 'Asia', 'https://restcountries.eu/data/lbn.svg', 'لبنان', '+961',
        '33.83333333', '35.83333333'),
       ('126', 'Lesotho', 'LSO', 'LS', 'Maseru', 'Africa', 'https://restcountries.eu/data/lso.svg', 'Lesotho', '+266',
        '-29.5', '28.5'),
       ('127', 'Liberia', 'LBR', 'LR', 'Monrovia', 'Africa', 'https://restcountries.eu/data/lbr.svg', 'Liberia', '+231',
        '6.5', '-9.5'),
       ('128', 'Libya', 'LBY', 'LY', 'Tripoli', 'Africa', 'https://restcountries.eu/data/lby.svg', '‏ليبيا', '+218',
        '25', '17'),
       ('129', 'Liechtenstein', 'LIE', 'LI', 'Vaduz', 'Europe', 'https://restcountries.eu/data/lie.svg',
        'Liechtenstein', '+423', '47.26666666', '9.53333333'),
       ('130', 'Lithuania', 'LTU', 'LT', 'Vilnius', 'Europe', 'https://restcountries.eu/data/ltu.svg', 'Lietuva',
        '+370', '56', '24'),
       ('131', 'Luxembourg', 'LUX', 'LU', 'Luxembourg', 'Europe', 'https://restcountries.eu/data/lux.svg', 'Luxembourg',
        '+352', '49.75', '6.16666666'),
       ('132', 'Macao', 'MAC', 'MO', '', 'Asia', 'https://restcountries.eu/data/mac.svg', '澳門', '+853', '22.16666666',
        '113.55'),
       ('133', 'Macedonia (the former Yugoslav Republic of)', 'MKD', 'MK', 'Skopje', 'Europe',
        'https://restcountries.eu/data/mkd.svg', 'Македонија', '+389', '41.83333333', '22'),
       ('134', 'Madagascar', 'MDG', 'MG', 'Antananarivo', 'Africa', 'https://restcountries.eu/data/mdg.svg',
        'Madagasikara', '+261', '-20', '47'),
       ('135', 'Malawi', 'MWI', 'MW', 'Lilongwe', 'Africa', 'https://restcountries.eu/data/mwi.svg', 'Malawi', '+265',
        '-13.5', '34'),
       ('136', 'Malaysia', 'MYS', 'MY', 'Kuala Lumpur', 'Asia', 'https://restcountries.eu/data/mys.svg', 'Malaysia',
        '+60', '2.5', '112.5'),
       ('137', 'Maldives', 'MDV', 'MV', 'Malé', 'Asia', 'https://restcountries.eu/data/mdv.svg', 'Maldives', '+960',
        '3.25', '73'),
       ('138', 'Mali', 'MLI', 'ML', 'Bamako', 'Africa', 'https://restcountries.eu/data/mli.svg', 'Mali', '+223', '17',
        '-4'),
       ('139', 'Malta', 'MLT', 'MT', 'Valletta', 'Europe', 'https://restcountries.eu/data/mlt.svg', 'Malta', '+356',
        '35.83333333', '14.58333333'),
       ('140', 'Marshall Islands', 'MHL', 'MH', 'Majuro', 'Oceania', 'https://restcountries.eu/data/mhl.svg', 'M̧ajeļ',
        '+692', '9', '168'),
       ('141', 'Martinique', 'MTQ', 'MQ', 'Fort-de-France', 'Americas', 'https://restcountries.eu/data/mtq.svg',
        'Martinique', '+596', '14.666667', '-61'),
       ('142', 'Mauritania', 'MRT', 'MR', 'Nouakchott', 'Africa', 'https://restcountries.eu/data/mrt.svg', 'موريتانيا',
        '+222', '20', '-12'),
       ('143', 'Mauritius', 'MUS', 'MU', 'Port Louis', 'Africa', 'https://restcountries.eu/data/mus.svg', 'Maurice',
        '+230', '-20.28333333', '57.55'),
       ('144', 'Mayotte', 'MYT', 'YT', 'Mamoudzou', 'Africa', 'https://restcountries.eu/data/myt.svg', 'Mayotte',
        '+262', '-12.83333333', '45.16666666'),
       ('145', 'Mexico', 'MEX', 'MX', 'Mexico City', 'Americas', 'https://restcountries.eu/data/mex.svg', 'México',
        '+52', '23', '-102'),
       ('146', 'Micronesia (Federated States of)', 'FSM', 'FM', 'Palikir', 'Oceania',
        'https://restcountries.eu/data/fsm.svg', 'Micronesia', '+691', '6.91666666', '158.25'),
       ('147', 'Moldova (Republic of)', 'MDA', 'MD', 'Chișinău', 'Europe', 'https://restcountries.eu/data/mda.svg',
        'Moldova', '+373', '47', '29'),
       ('148', 'Monaco', 'MCO', 'MC', 'Monaco', 'Europe', 'https://restcountries.eu/data/mco.svg', 'Monaco', '+377',
        '43.73333333', '7.4'),
       ('149', 'Mongolia', 'MNG', 'MN', 'Ulan Bator', 'Asia', 'https://restcountries.eu/data/mng.svg', 'Монгол улс',
        '+976', '46', '105'),
       ('150', 'Montenegro', 'MNE', 'ME', 'Podgorica', 'Europe', 'https://restcountries.eu/data/mne.svg', 'Црна Гора',
        '+382', '42.5', '19.3'),
       ('151', 'Montserrat', 'MSR', 'MS', 'Plymouth', 'Americas', 'https://restcountries.eu/data/msr.svg', 'Montserrat',
        '+1664', '16.75', '-62.2'),
       ('152', 'Morocco', 'MAR', 'MA', 'Rabat', 'Africa', 'https://restcountries.eu/data/mar.svg', 'المغرب', '+212',
        '32', '-5'),
       ('153', 'Mozambique', 'MOZ', 'MZ', 'Maputo', 'Africa', 'https://restcountries.eu/data/moz.svg', 'Moçambique',
        '+258', '-18.25', '35'),
       ('154', 'Myanmar', 'MMR', 'MM', 'Naypyidaw', 'Asia', 'https://restcountries.eu/data/mmr.svg', 'Myanma', '+95',
        '22', '98'),
       ('155', 'Namibia', 'NAM', 'NA', 'Windhoek', 'Africa', 'https://restcountries.eu/data/nam.svg', 'Namibia', '+264',
        '-22', '17'),
       ('156', 'Nauru', 'NRU', 'NR', 'Yaren', 'Oceania', 'https://restcountries.eu/data/nru.svg', 'Nauru', '+674',
        '-0.53333333', '166.91666666'),
       ('157', 'Nepal', 'NPL', 'NP', 'Kathmandu', 'Asia', 'https://restcountries.eu/data/npl.svg', 'नेपाल', '+977',
        '28', '84'),
       ('158', 'Netherlands', 'NLD', 'NL', 'Amsterdam', 'Europe', 'https://restcountries.eu/data/nld.svg', 'Nederland',
        '+31', '52.5', '5.75'),
       ('159', 'New Caledonia', 'NCL', 'NC', 'Nouméa', 'Oceania', 'https://restcountries.eu/data/ncl.svg',
        'Nouvelle-Calédonie', '+687', '-21.5', '165.5'),
       ('160', 'New Zealand', 'NZL', 'NZ', 'Wellington', 'Oceania', 'https://restcountries.eu/data/nzl.svg',
        'New Zealand', '+64', '-41', '174'),
       ('161', 'Nicaragua', 'NIC', 'NI', 'Managua', 'Americas', 'https://restcountries.eu/data/nic.svg', 'Nicaragua',
        '+505', '13', '-85'),
       ('162', 'Niger', 'NER', 'NE', 'Niamey', 'Africa', 'https://restcountries.eu/data/ner.svg', 'Niger', '+227', '16',
        '8'),
       ('163', 'Nigeria', 'NGA', 'NG', 'Abuja', 'Africa', 'https://restcountries.eu/data/nga.svg', 'Nigeria', '+234',
        '10', '8'),
       ('164', 'Niue', 'NIU', 'NU', 'Alofi', 'Oceania', 'https://restcountries.eu/data/niu.svg', 'Niuē', '+683',
        '-19.03333333', '-169.86666666'),
       ('165', 'Norfolk Island', 'NFK', 'NF', 'Kingston', 'Oceania', 'https://restcountries.eu/data/nfk.svg',
        'Norfolk Island', '+672', '-29.03333333', '167.95'),
       ('166', 'Korea (Democratic People''s Republic of)', 'PRK', 'KP', 'Pyongyang', 'Asia',
        'https://restcountries.eu/data/prk.svg', '북한', '+850', '40', '127'),
       ('167', 'Northern Mariana Islands', 'MNP', 'MP', 'Saipan', 'Oceania', 'https://restcountries.eu/data/mnp.svg',
        'Northern Mariana Islands', '+1670', '15.2', '145.75'),
       ('168', 'Norway', 'NOR', 'NO', 'Oslo', 'Europe', 'https://restcountries.eu/data/nor.svg', 'Norge', '+47', '62',
        '10'),
       ('169', 'Oman', 'OMN', 'OM', 'Muscat', 'Asia', 'https://restcountries.eu/data/omn.svg', 'عمان', '+968', '21',
        '57'),
       ('170', 'Pakistan', 'PAK', 'PK', 'Islamabad', 'Asia', 'https://restcountries.eu/data/pak.svg', 'Pakistan', '+92',
        '30', '70'),
       ('171', 'Palau', 'PLW', 'PW', 'Ngerulmud', 'Oceania', 'https://restcountries.eu/data/plw.svg', 'Palau', '+680',
        '7.5', '134.5'),
       ('172', 'Palestine, State of', 'PSE', 'PS', 'Ramallah', 'Asia', 'https://restcountries.eu/data/pse.svg',
        'فلسطين', '+970', '31.9', '35.2'),
       ('173', 'Panama', 'PAN', 'PA', 'Panama City', 'Americas', 'https://restcountries.eu/data/pan.svg', 'Panamá',
        '+507', '9', '-80'),
       ('174', 'Papua New Guinea', 'PNG', 'PG', 'Port Moresby', 'Oceania', 'https://restcountries.eu/data/png.svg',
        'Papua Niugini', '+675', '-6', '147'),
       ('175', 'Paraguay', 'PRY', 'PY', 'Asunción', 'Americas', 'https://restcountries.eu/data/pry.svg', 'Paraguay',
        '+595', '-23', '-58'),
       ('176', 'Peru', 'PER', 'PE', 'Lima', 'Americas', 'https://restcountries.eu/data/per.svg', 'Perú', '+51', '-10',
        '-76'),
       ('177', 'Philippines', 'PHL', 'PH', 'Manila', 'Asia', 'https://restcountries.eu/data/phl.svg', 'Pilipinas',
        '+63', '13', '122'),
       ('178', 'Pitcairn', 'PCN', 'PN', 'Adamstown', 'Oceania', 'https://restcountries.eu/data/pcn.svg',
        'Pitcairn Islands', '+64', '-25.06666666', '-130.1'),
       ('179', 'Poland', 'POL', 'PL', 'Warsaw', 'Europe', 'https://restcountries.eu/data/pol.svg', 'Polska', '+48',
        '52', '20'),
       ('180', 'Portugal', 'PRT', 'PT', 'Lisbon', 'Europe', 'https://restcountries.eu/data/prt.svg', 'Portugal', '+351',
        '39.5', '-8'),
       ('181', 'Puerto Rico', 'PRI', 'PR', 'San Juan', 'Americas', 'https://restcountries.eu/data/pri.svg',
        'Puerto Rico', '+1787', '18.25', '-66.5'),
       ('182', 'Qatar', 'QAT', 'QA', 'Doha', 'Asia', 'https://restcountries.eu/data/qat.svg', 'قطر', '+974', '25.5',
        '51.25'),
       ('183', 'Republic of Kosovo', 'KOS', 'XK', 'Pristina', 'Europe', 'https://restcountries.eu/data/kos.svg',
        'Republika e Kosovës', '+383', '42.666667', '21.166667'),
       ('184', 'Réunion', 'REU', 'RE', 'Saint-Denis', 'Africa', 'https://restcountries.eu/data/reu.svg', 'La Réunion',
        '+262', '-21.15', '55.5'),
       ('185', 'Romania', 'ROU', 'RO', 'Bucharest', 'Europe', 'https://restcountries.eu/data/rou.svg', 'România', '+40',
        '46', '25'),
       ('186', 'Russian Federation', 'RUS', 'RU', 'Moscow', 'Europe', 'https://restcountries.eu/data/rus.svg', 'Россия',
        '+7', '60', '100'),
       ('187', 'Rwanda', 'RWA', 'RW', 'Kigali', 'Africa', 'https://restcountries.eu/data/rwa.svg', 'Rwanda', '+250',
        '-2', '30'),
       ('188', 'Saint Barthélemy', 'BLM', 'BL', 'Gustavia', 'Americas', 'https://restcountries.eu/data/blm.svg',
        'Saint-Barthélemy', '+590', '18.5', '-63.41666666'),
       ('189', 'Saint Helena, Ascension and Tristan da Cunha', 'SHN', 'SH', 'Jamestown', 'Africa',
        'https://restcountries.eu/data/shn.svg', 'Saint Helena', '+290', '-15.95', '-5.7'),
       ('190', 'Saint Kitts and Nevis', 'KNA', 'KN', 'Basseterre', 'Americas', 'https://restcountries.eu/data/kna.svg',
        'Saint Kitts and Nevis', '+1869', '17.33333333', '-62.75'),
       ('191', 'Saint Lucia', 'LCA', 'LC', 'Castries', 'Americas', 'https://restcountries.eu/data/lca.svg',
        'Saint Lucia', '+1758', '13.88333333', '-60.96666666'),
       ('192', 'Saint Martin (French part)', 'MAF', 'MF', 'Marigot', 'Americas',
        'https://restcountries.eu/data/maf.svg', 'Saint-Martin', '+590', '18.08333333', '-63.95'),
       ('193', 'Saint Pierre and Miquelon', 'SPM', 'PM', 'Saint-Pierre', 'Americas',
        'https://restcountries.eu/data/spm.svg', 'Saint-Pierre-et-Miquelon', '+508', '46.83333333', '-56.33333333'),
       ('194', 'Saint Vincent and the Grenadines', 'VCT', 'VC', 'Kingstown', 'Americas',
        'https://restcountries.eu/data/vct.svg', 'Saint Vincent and the Grenadines', '+1784', '13.25', '-61.2'),
       ('195', 'Samoa', 'WSM', 'WS', 'Apia', 'Oceania', 'https://restcountries.eu/data/wsm.svg', 'Samoa', '+685',
        '-13.58333333', '-172.33333333'),
       ('196', 'San Marino', 'SMR', 'SM', 'City of San Marino', 'Europe', 'https://restcountries.eu/data/smr.svg',
        'San Marino', '+378', '43.76666666', '12.41666666'),
       ('197', 'Sao Tome and Principe', 'STP', 'ST', 'São Tomé', 'Africa', 'https://restcountries.eu/data/stp.svg',
        'São Tomé e Príncipe', '+239', '1', '7'),
       ('198', 'Saudi Arabia', 'SAU', 'SA', 'Riyadh', 'Asia', 'https://restcountries.eu/data/sau.svg',
        'العربية السعودية', '+966', '25', '45'),
       ('199', 'Senegal', 'SEN', 'SN', 'Dakar', 'Africa', 'https://restcountries.eu/data/sen.svg', 'Sénégal', '+221',
        '14', '-14'),
       ('200', 'Serbia', 'SRB', 'RS', 'Belgrade', 'Europe', 'https://restcountries.eu/data/srb.svg', 'Србија', '+381',
        '44', '21'),
       ('201', 'Seychelles', 'SYC', 'SC', 'Victoria', 'Africa', 'https://restcountries.eu/data/syc.svg', 'Seychelles',
        '+248', '-4.58333333', '55.66666666'),
       ('202', 'Sierra Leone', 'SLE', 'SL', 'Freetown', 'Africa', 'https://restcountries.eu/data/sle.svg',
        'Sierra Leone', '+232', '8.5', '-11.5'),
       ('203', 'Singapore', 'SGP', 'SG', 'Singapore', 'Asia', 'https://restcountries.eu/data/sgp.svg', 'Singapore',
        '+65', '1.36666666', '103.8'),
       ('204', 'Sint Maarten (Dutch part)', 'SXM', 'SX', 'Philipsburg', 'Americas',
        'https://restcountries.eu/data/sxm.svg', 'Sint Maarten', '+1721', '18.033333', '-63.05'),
       ('205', 'Slovakia', 'SVK', 'SK', 'Bratislava', 'Europe', 'https://restcountries.eu/data/svk.svg', 'Slovensko',
        '+421', '48.66666666', '19.5'),
       ('206', 'Slovenia', 'SVN', 'SI', 'Ljubljana', 'Europe', 'https://restcountries.eu/data/svn.svg', 'Slovenija',
        '+386', '46.11666666', '14.81666666'),
       ('207', 'Solomon Islands', 'SLB', 'SB', 'Honiara', 'Oceania', 'https://restcountries.eu/data/slb.svg',
        'Solomon Islands', '+677', '-8', '159'),
       ('208', 'Somalia', 'SOM', 'SO', 'Mogadishu', 'Africa', 'https://restcountries.eu/data/som.svg', 'Soomaaliya',
        '+252', '10', '49'),
       ('209', 'South African', 'ZAF', 'ZA', 'Pretoria', 'Africa', 'https://restcountries.eu/data/zaf.svg',
        'South Africa', '+27', '-29', '24'),
       ('210', 'South Georgia and the South Sandwich Islands', 'SGS', 'GS', 'King Edward Point', 'Americas',
        'https://restcountries.eu/data/sgs.svg', 'South Georgia', '+500', '-54.5', '-37'),
       ('211', 'Korea (Republic of)', 'KOR', 'KR', 'Seoul', 'Asia', 'https://restcountries.eu/data/kor.svg', '대한민국',
        '+82', '37', '127.5'),
       ('212', 'South Sudan', 'SSD', 'SS', 'Juba', 'Africa', 'https://restcountries.eu/data/ssd.svg', 'South Sudan',
        '+211', '7', '30'),
       ('213', 'Spain', 'ESP', 'ES', 'Madrid', 'Europe', 'https://restcountries.eu/data/esp.svg', 'España', '+34', '40',
        '-4'),
       ('214', 'Sri Lanka', 'LKA', 'LK', 'Colombo', 'Asia', 'https://restcountries.eu/data/lka.svg', 'śrī laṃkāva',
        '+94', '7', '81'),
       ('215', 'Sudan', 'SDN', 'SD', 'Khartoum', 'Africa', 'https://restcountries.eu/data/sdn.svg', 'السودان', '+249',
        '15', '30'),
       ('216', 'Suriname', 'SUR', 'SR', 'Paramaribo', 'Americas', 'https://restcountries.eu/data/sur.svg', 'Suriname',
        '+597', '4', '-56'),
       ('217', 'Svalbard and Jan Mayen', 'SJM', 'SJ', 'Longyearbyen', 'Europe', 'https://restcountries.eu/data/sjm.svg',
        'Svalbard og Jan Mayen', '+4779', '78', '20'),
       ('218', 'Swaziland', 'SWZ', 'SZ', 'Lobamba', 'Africa', 'https://restcountries.eu/data/swz.svg', 'Swaziland',
        '+268', '-26.5', '31.5'),
       ('219', 'Sweden', 'SWE', 'SE', 'Stockholm', 'Europe', 'https://restcountries.eu/data/swe.svg', 'Sverige', '+46',
        '62', '15'),
       ('220', 'Switzerland', 'CHE', 'CH', 'Bern', 'Europe', 'https://restcountries.eu/data/che.svg', 'Schweiz', '+41',
        '47', '8'),
       ('221', 'Syrian Arab Republic', 'SYR', 'SY', 'Damascus', 'Asia', 'https://restcountries.eu/data/syr.svg',
        'سوريا', '+963', '35', '38'),
       ('222', 'Taiwan', 'TWN', 'TW', 'Taipei', 'Asia', 'https://restcountries.eu/data/twn.svg', '臺灣', '+886', '23.5',
        '121'),
       ('223', 'Tajikistan', 'TJK', 'TJ', 'Dushanbe', 'Asia', 'https://restcountries.eu/data/tjk.svg', 'Тоҷикистон',
        '+992', '39', '71'),
       ('224', 'Tanzania, United Republic of', 'TZA', 'TZ', 'Dodoma', 'Africa', 'https://restcountries.eu/data/tza.svg',
        'Tanzania', '+255', '-6', '35'),
       ('225', 'Thailand', 'THA', 'TH', 'Bangkok', 'Asia', 'https://restcountries.eu/data/tha.svg', 'ประเทศไทย', '+66',
        '15', '100'),
       ('226', 'Timor-Leste', 'TLS', 'TL', 'Dili', 'Asia', 'https://restcountries.eu/data/tls.svg', 'Timor-Leste',
        '+670', '-8.83333333', '125.91666666'),
       ('227', 'Togo', 'TGO', 'TG', 'Lomé', 'Africa', 'https://restcountries.eu/data/tgo.svg', 'Togo', '+228', '8',
        '1.16666666'),
       ('228', 'Tokelau', 'TKL', 'TK', 'Fakaofo', 'Oceania', 'https://restcountries.eu/data/tkl.svg', 'Tokelau', '+690',
        '-9', '-172'),
       ('229', 'Tonga', 'TON', 'TO', 'Nuku''alofa', 'Oceania', 'https://restcountries.eu/data/ton.svg', 'Tonga', '+676',
        '-20', '-175'),
       ('230', 'Trinidad and Tobago', 'TTO', 'TT', 'Port of Spain', 'Americas', 'https://restcountries.eu/data/tto.svg',
        'Trinidad and Tobago', '+1868', '11', '-61'),
       ('231', 'Tunisia', 'TUN', 'TN', 'Tunis', 'Africa', 'https://restcountries.eu/data/tun.svg', 'تونس', '+216', '34',
        '9'),
       ('232', 'Turkey', 'TUR', 'TR', 'Ankara', 'Asia', 'https://restcountries.eu/data/tur.svg', 'Türkiye', '+90', '39',
        '35'),
       ('233', 'Turkmenistan', 'TKM', 'TM', 'Ashgabat', 'Asia', 'https://restcountries.eu/data/tkm.svg', 'Türkmenistan',
        '+993', '40', '60'),
       ('234', 'Turks and Caicos Islands', 'TCA', 'TC', 'Cockburn Town', 'Americas',
        'https://restcountries.eu/data/tca.svg', 'Turks and Caicos Islands', '+1649', '21.75', '-71.58333333'),
       ('235', 'Tuvalu', 'TUV', 'TV', 'Funafuti', 'Oceania', 'https://restcountries.eu/data/tuv.svg', 'Tuvalu', '+688',
        '-8', '178'),
       ('236', 'Uganda', 'UGA', 'UG', 'Kampala', 'Africa', 'https://restcountries.eu/data/uga.svg', 'Uganda', '+256',
        '1', '32'),
       ('237', 'Ukraine', 'UKR', 'UA', 'Kiev', 'Europe', 'https://restcountries.eu/data/ukr.svg', 'Україна', '+380',
        '49', '32'),
       ('238', 'United Arab Emirates', 'ARE', 'AE', 'Abu Dhabi', 'Asia', 'https://restcountries.eu/data/are.svg',
        'دولة الإمارات العربية المتحدة', '+971', '24', '54'),
       ('239', 'United Kingdom of Great Britain and Northern Ireland', 'GBR', 'GB', 'London', 'Europe',
        'https://restcountries.eu/data/gbr.svg', 'United Kingdom', '+44', '54', '-2'),
       ('240', 'United States of America', 'USA', 'US', 'Washington, D.C.', 'Americas',
        'https://restcountries.eu/data/usa.svg', 'United States', '+1', '38', '-97'),
       ('241', 'Uruguay', 'URY', 'UY', 'Montevideo', 'Americas', 'https://restcountries.eu/data/ury.svg', 'Uruguay',
        '+598', '-33', '-56'),
       ('242', 'Uzbekistan', 'UZB', 'UZ', 'Tashkent', 'Asia', 'https://restcountries.eu/data/uzb.svg', 'O‘zbekiston',
        '+998', '41', '64'),
       ('243', 'Vanuatu', 'VUT', 'VU', 'Port Vila', 'Oceania', 'https://restcountries.eu/data/vut.svg', 'Vanuatu',
        '+678', '-16', '167'),
       ('244', 'Venezuela (Bolivarian Republic of)', 'VEN', 'VE', 'Caracas', 'Americas',
        'https://restcountries.eu/data/ven.svg', 'Venezuela', '+58', '8', '-66'),
       ('245', 'Viet Nam', 'VNM', 'VN', 'Hanoi', 'Asia', 'https://restcountries.eu/data/vnm.svg', 'Việt Nam', '+84',
        '16.16666666', '107.83333333'),
       ('246', 'Wallis and Futuna', 'WLF', 'WF', 'Mata-Utu', 'Oceania', 'https://restcountries.eu/data/wlf.svg',
        'Wallis et Futuna', '+681', '-13.3', '-176.2'),
       ('247', 'Western Sahara', 'ESH', 'EH', 'El Aaiún', 'Africa', 'https://restcountries.eu/data/esh.svg',
        'الصحراء الغربية', '+212', '24.5', '-13'),
       ('248', 'Yemen', 'YEM', 'YE', 'Sana''a', 'Asia', 'https://restcountries.eu/data/yem.svg', 'اليَمَن', '+967',
        '15', '48'),
       ('249', 'Zambia', 'ZMB', 'ZM', 'Lusaka', 'Africa', 'https://restcountries.eu/data/zmb.svg', 'Zambia', '+260',
        '-15', '30'),
       ('250', 'Zimbabwe', 'ZWE', 'ZW', 'Harare', 'Africa', 'https://restcountries.eu/data/zwe.svg', 'Zimbabwe', '+263',
        '-20', '30');

