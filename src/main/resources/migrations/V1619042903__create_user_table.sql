CREATE TABLE "user"
(
    id              UUID PRIMARY KEY                      NOT NULL,
    identity_number VARCHAR(13) UNIQUE                    NOT NULL,
    phone_number    VARCHAR(20) UNIQUE                    NOT NULL,
    names           VARCHAR(160)                          NOT NULL,
    surname         VARCHAR(160)                          NOT NULL,
    email           VARCHAR(255) NULL,
    photo           text NULL,
    deceased_status VARCHAR(50) NULL,
    age             INT                                   NOT NULL,
    birthdate       TIMESTAMP                             NOT NULL,
    gender          VARCHAR(6)                            NOT NULL,
    password        VARCHAR(255)                          NOT NULL,
    nationality_id  INT                                   NOT NULL,
    address_id      BIGINT NULL,
    role_id         INT                                   NOT NULL,
    status_id       INT                                   NOT NULL,
    login_attempt   INT       DEFAULT (0)                 NOT NULL,
    last_login_at   TIMESTAMP NULL,
    created_at      TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    updated_at      TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    deleted_at      TIMESTAMP NULL,
    blocked_at      TIMESTAMP NULL,
    verified_at     TIMESTAMP NULL,
    deceased_at     TIMESTAMP NULL,

    CONSTRAINT fk_role
        FOREIGN KEY (role_id)
            REFERENCES taxonomy_term (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,

    CONSTRAINT fk_status
        FOREIGN KEY (status_id)
            REFERENCES taxonomy_term (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,

    CONSTRAINT fk_nationality
        FOREIGN KEY (nationality_id)
            REFERENCES country (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,

    CONSTRAINT fk_address
        FOREIGN KEY (address_id)
            REFERENCES address (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);