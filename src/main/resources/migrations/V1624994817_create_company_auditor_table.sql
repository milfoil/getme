CREATE TABLE company_auditor
(
    id              BIGSERIAL PRIMARY KEY                 NOT NULL,
    display_text            VARCHAR(255)                          NULL,
    name                    VARCHAR(255)                          NOT NULL,
    profession_no           VARCHAR(255)                          NOT NULL,
    profession_desc         VARCHAR(255)                          NOT NULL,
    type_desc               VARCHAR(255)                          NOT NULL,
    status_desc             VARCHAR(255)                          NOT NULL,
    last_updated_date       VARCHAR(255)                          NOT NULL,
    postal_address          VARCHAR(255)                          NOT NULL,
    physicalAddress         VARCHAR(255)                          NOT NULL,
    telephone_no            VARCHAR(255)                          NOT NULL,
    years_with_auditor      VARCHAR(255)                          NOT NULL,
    act_start_date          VARCHAR(255)                          NOT NULL,
    act_end_date            VARCHAR(255)                          NOT NULL,
    no_of_years_In_business VARCHAR(255)                          NOT NULL,
    created_at              TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    updated_at              TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    deleted_at              TIMESTAMP                             NULL
);
