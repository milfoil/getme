CREATE TABLE address
(
    id              BIGSERIAL PRIMARY KEY                 NOT NULL,
    unit            VARCHAR(36)                           NULL,
    building_name   VARCHAR(160)                          NULL,
    line1           VARCHAR(180)                          NOT NULL,
    line2           VARCHAR(180)                          NOT NULL,
    city            VARCHAR(36)                           NOT NULL,
    state           VARCHAR(36)                           NOT NULL,
    postal_code     VARCHAR(10)                           NOT NULL,
    latitude        VARCHAR(255)                          NULL,
    longitude       VARCHAR(255)                          NULL,
    country_id      INT                                   NOT NULL,
    address_type_id INT                                   NOT NULL,
    created_at      TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    updated_at      TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    deleted_at      TIMESTAMP                             NULL,

    CONSTRAINT fk_country
        FOREIGN KEY (country_id)
            REFERENCES country (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,

    CONSTRAINT fk_address_type
        FOREIGN KEY (address_type_id)
            REFERENCES taxonomy_term (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);