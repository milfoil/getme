CREATE TABLE taxonomy_term
(
    id          SMALLSERIAL PRIMARY KEY               NOT NULL,
    taxonomy_id INT                                   NOT NULL,
    name        VARCHAR(160)                          NOT NULL,
    slug        VARCHAR(255)                          NOT NULL,
    description VARCHAR(255)                          NULL,
    enabled     BOOL      DEFAULT (TRUE)              NULL,
    created_at  TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    updated_at  TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,

    CONSTRAINT fk_taxonomy
        FOREIGN KEY (taxonomy_id)
            REFERENCES taxonomy (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

INSERT INTO taxonomy_term (id, taxonomy_id, name, slug, description)
VALUES ('1', '1', 'System Administrators', 'system_administrator',
        'A system administrator, or sysadmin, is a person who is responsible for the upkeep, configuration, and reliable operation of computer systems'),
       ('2', '1', 'Administrator', 'administrator',
        'An administrator is a person whose job involves helping to organize and supervise the way that an application functions.'),
       ('3', '1', 'Standard', 'standard',
        'This user account has moderate privilege. This user account is not allowed to make any change in system files and properties. '),
       ('4', '1', 'Guest', 'guest',
        'This user account has the lowest privilege. It can’t make any change in any system files or properties. Usually this account is used to access the system for temporary tasks.'),
       ('5', '1', 'Merchant', 'merchant', 'A merchant represents a person or company that sells goods or services.'),
       ('6', '1', 'Driver', 'driver',
        'A Delivery Driver Responsibilities: Loading, transporting, and delivering items to clients or businesses in a safe, timely manner.'),
       ('7', '1', 'Promoter', 'promoter',
        'Promoter is a marketing professional responsible for demonstrating the features of a product to an audience or client.'),
       ('8', '1', 'Agent', 'agent', 'An Agent is responsible for customer service.'),
       ('9', '2', 'Active', 'active', 'The user has either logged in to one of the portals or enrolled a device.'),
       ('10', '2', 'Inactive', 'inactive',
        'Only the status can be modified. The only possibility is to go back to the Active status. Inactive users cannot be selected.'),
       ('11', '2', 'Deleted', 'deleted', 'The user account has been deleted.'),
       ('12', '2', 'Locked', 'locked', 'The user account has been locked. The user cannot access the application .'),
       ('13', '2', 'NotYetConfirmed', 'not_yet_confirmed',
        'The account was created in the identity pool however, the user has not responded.'),
       ('14', '2', 'Verified', 'Verified', 'The user identity has been verified.'),
       ('15', '3', 'Mobile Phone', 'mobile', 'Mobile Phone.'),
       ('16', '3', 'Landline', 'landline', 'Landline.'),
       ('17', '3', 'Special Services', 'special_services', 'Special Services (e.g. Police)'),
       ('18', '3', 'Toll-Free Numbers', 'toll_free', 'Toll-Free Numbers (e.g. hotels)'),
       ('19', '3', 'Premium Rate Numbers', 'premium_rate', 'Premium Rate Numbers (e.g. paid hotlines)'),
       ('20', '3', 'Satellite', 'satellite', 'Satellite'),
       ('21', '3', 'Paging', 'paging', 'Paging'),
       ('22', '4', 'Residential', 'residential', 'Residential address.'),
       ('23', '4', 'Business', 'business', 'Business address.'),
       ('24', '4', 'Billing', 'billing', 'Billing address.'),
       ('25', '4', 'Shipping', 'shipping', 'Shipping address.'),
       ('26', '4', 'Recipient', 'recipient', 'Recipient address of a person or thing that receives.'),
       ('27', '4', 'AR', 'business',
        'AR address of a high officer in a bank or trust company responsible for moneys received and expended');




