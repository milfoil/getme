CREATE TABLE taxi_trip
(
    id             BIGSERIAL PRIMARY KEY                 NOT NULL,
    origin         VARCHAR(255)                          NOT NULL,
    destination    VARCHAR(100)                          NOT NULL,
    departure_date TIMESTAMP                             NOT NULL,
    fare_quote     DECIMAL(18, 2)                        NULL,
    status         VARCHAR(50)                           NULL,
    latitude       VARCHAR(255)                          NULL,
    longitude      VARCHAR(255)                          NULL,
    carrier_id     VARCHAR(50)                           NULL,
    creator_id     VARCHAR(50)                           NULL,
    created_at     TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    completed_at   TIMESTAMP                             NULL,
    cancelled_at   TIMESTAMP                             NULL,
    updated_at     TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    deleted_at     TIMESTAMP                             NULL
);