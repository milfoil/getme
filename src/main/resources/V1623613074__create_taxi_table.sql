CREATE TABLE taxi_passenger
(
    id         BIGSERIAL PRIMARY KEY                 NOT NULL,
    trip_id    VARCHAR(255)                          NOT NULL,
    passenger  VARCHAR(100)                          NOT NULL,
    code       VARCHAR(50)                           NULL,
    price      VARCHAR(50)                           NULL,
    status     VARCHAR(50)                           NULL,
    payment_id VARCHAR(50)                           NULL,
    carrier_id VARCHAR(50)                           NULL,
    creator_id VARCHAR(50)                           NULL,
    created_at TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    updated_at TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    deleted_at TIMESTAMP                             NULL
)