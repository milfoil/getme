CREATE TABLE taxi_passenger
(
    id         BIGSERIAL PRIMARY KEY                 NOT NULL,
    trip_id    VARCHAR(255)                          NOT NULL,
    passenger  VARCHAR(100)                          NOT NULL,
    code       VARCHAR(50)                           NULL,
    price      VARCHAR(50)                           NULL,
    status     VARCHAR(50)                           NULL,
    payment_id VARCHAR(50)                           NULL,
    carrier_id VARCHAR(50)                           NULL,
    creator_id VARCHAR(50)                           NULL,
    created_at TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    updated_at TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    deleted_at TIMESTAMP                             NULL
)


    {
  "Status": "Success",
  "Result": {
    "Id": "i2",
    "DateOfBirth": "1979-05-01T00:00:00",
    "DocumentNumber": "40360002HJSL",
    "DocumentType": "SADL",
    "FirstNames": "",
    "FullName": "",
    "Gender": "Male",
    "Initials": "J",
    "IssueCountry": "ZA",
    "IssuePlace": "ZA",
    "LastName": "GOOFY",
    "MiddleNames": "",
    "PersonIdentificationNumber": "79050456456665",
    "ValidFrom": "2012-08-03T00:00:00",
    "ValidTo": "2017-09-07T00:00:00",
    "DriversLicenseType": 2,
    "IssueNumber": 1,
    "PermitCategories": "-",
    "PermitValidTo": "0001-01-01T00:00:00",
    "Restrictions": "0",
    "VehicleCode": {
      "string": [
        "EB",
        "-",
        "-",
        "-"
      ]
    },
    "VehicleFirstIssue": {
      "dateTime": [
        "2002-05-21T00:00:00",
        "0001-01-01T00:00:00",
        "0001-01-01T00:00:00",
        "0001-01-01T00:00:00"
      ]
    },
    "VehicleRestriction": {
      "string": [
        "0",
        "-",
        "-",
        "-"
      ]
    },
    "PhotoJPG": "base64 string of jpg",
    "IDVerification ": {
      "Result ": {
        "Status ": "ID Number Valid ",
        "Verification ": {
          "Firstnames ": "JUST ",
          "Lastname ": [
            "GOOFY "
          ],
          "Dob ": "1979 - 05 - 01 ",
          "Age ": 36,
          "Gender ": "Male ",
          "Citizenship ": "South African ",
          "DateIssued ": [
            "1997 - 07 - 25T00: 00: 00 + 02: 00"
          ]
        }
      }
    }
  }
}