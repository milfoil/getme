CREATE TABLE taxi_ticket
(
    id             BIGSERIAL PRIMARY KEY                 NOT NULL,
    redeem_at      VARCHAR(255)                          NOT NULL,
    recipient_id   VARCHAR(255)                          NOT NULL,
    transaction_id VARCHAR(255)                          NOT NULL,
    trip_id        VARCHAR(255)                          NOT NULL,
    qr_code        TEXT                                  NOT NULL,
    created_at     TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    updated_at     TIMESTAMP DEFAULT (CURRENT_TIMESTAMP) NOT NULL,
    deleted_at     TIMESTAMP                             NULL
);