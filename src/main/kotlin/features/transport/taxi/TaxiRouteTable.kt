package features.transport.taxi

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object TaxiRouteTable : LongIdTable("taxi_route") {
    /**
     * represents the taxi rank origin
     *
     * @var string
     */
    val origin = varchar("origin", length = 180) //

    /**
     * represents the taxi rank destination
     *
     * @var string
     */
    val destination = varchar("destination", length = 180)

    /**
     * represents the taxi rank province
     *
     * @var string
     */
    val province = varchar("province", length = 180)

    // /**
    //  * represents the taxi rank latitude to locate
    //  *
    //  * @var string
    //  */
    // val latitude = varchar("latitude", length = 255).nullable()
    //
    // /**
    //  * represents the taxi rank longitude to locate
    //  *
    //  * @var string
    //  */
    // val longitude = varchar("longitude", length = 255).nullable()

    /**
     * The timestamp when the record was first created in the database
     *
     * @var string
     */
    val createdAt = datetime("created_at").default(DateTime.now())

    /**
     * The timestamp of the most recent change to any of the data stored in the table.
     *
     * @var string
     */
    val updatedAt = datetime("updated_at").default(DateTime.now())

    /**
     * The timestamp when the resource was deleted in the database
     *
     * @var string
     */
    val deletedAt = datetime("deleted_at").nullable()
}
