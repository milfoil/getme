package features.transport.taxi

import features.transport.taxi.service.retrieveTaxiDestinationService
import features.transport.taxi.service.retrieveTaxiRouteService
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.routing.Routing
import io.ktor.routing.get

/**
 * Taxi API Routes
 * Represents the locations of the taxi routes within the City of Cape Town.
 */
fun Routing.taxiRouter() {
    authenticate("auth-jwk") {
        /**
         *
         */
        get("/taxi/routes/{province}") {
            try {
                retrieveTaxiRouteService(call)
            } catch (e: Exception) {
                println(e.stackTraceToString())
                throw Error(e.stackTraceToString())
            }
        }

        get("/taxi/destination/{origin}") {
            try {
                retrieveTaxiDestinationService(call)
            } catch (e: Exception) {
                println(e.stackTraceToString())
                throw Error(e.stackTraceToString())
            }
        }
    }
}
