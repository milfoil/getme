package features.transport.taxi.model

data class TaxiRoute(
    val id: Long,
    val origin: String,
    val destination: String,
    val province: String,
)
