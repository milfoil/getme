package features.transport.taxi.model

data class TaxiTripRequest(
    val passengers: List<String>,
    val origin: String,
    val destination: String,
    val date: String,
    val fare: String,
    val payment: String
)
