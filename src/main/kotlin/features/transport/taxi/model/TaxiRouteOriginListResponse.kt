package features.transport.taxi.model

data class TaxiRouteOriginListResponse(
    val id: Long,
    val name: String
)
