package features.transport.taxi

import features.transport.taxi.model.TaxiRoute
import features.transport.taxi.model.TaxiRouteOriginListResponse
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction

object TaxiRepository {
    suspend fun getTaxiRouteByOrigin(origin: String) = newSuspendedTransaction {
        return@newSuspendedTransaction TaxiRouteEntity.find { TaxiRouteTable.origin eq origin }.toList().map {
            TaxiRoute(
                id = it.id.value,
                origin = it.origin,
                province = it.province,
                destination = it.destination
            )
        }
    }

    suspend fun getTaxiRouteByProvince(value: String) = newSuspendedTransaction {
        return@newSuspendedTransaction TaxiRouteEntity.find { TaxiRouteTable.province eq value }.toList().groupByTo(
            mutableMapOf()
        ) { it.origin }.map {
            mapOf(
                it.key to it.value.map { item ->
                    TaxiRoute(
                        id = item.id.value,
                        origin = item.origin,
                        province = item.province,
                        destination = item.destination
                    )
                }
            )
        }
    }

    suspend fun getTaxiRouteOriginByProvince(value: String) = newSuspendedTransaction {
        return@newSuspendedTransaction TaxiRouteEntity.find { TaxiRouteTable.province eq value }
            .asSequence()
            .distinct().map {
                TaxiRouteOriginListResponse(
                    id = it.id.value,
                    name = it.origin,
                )
            }
            .distinctBy { it.name }
            .sortedBy { it.name }
            .toList()
    }
}
