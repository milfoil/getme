package features.transport.taxi

import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class TaxiRouteEntity(id: EntityID<Long>) : LongEntity(id) {
    var origin by TaxiRouteTable.origin
    var destination by TaxiRouteTable.destination
    var province by TaxiRouteTable.province
    var createdAt by TaxiRouteTable.createdAt
    var updatedAt by TaxiRouteTable.updatedAt
    var deletedAt by TaxiRouteTable.deletedAt

    companion object : LongEntityClass<TaxiRouteEntity>(TaxiRouteTable)
}
