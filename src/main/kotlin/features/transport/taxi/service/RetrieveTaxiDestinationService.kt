package features.transport.taxi.service

import constant.ErrorMessage
import features.transport.taxi.TaxiRepository
import http.HttpException
import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond

/**
 * represents the origin of the taxi routes within the province
 */
suspend fun retrieveTaxiDestinationService(call: ApplicationCall) {
    /**
     * extract user origin location
     */
    val form: String = runCatching {
        call.parameters["departure"] ?: throw HttpException(ErrorMessage.INVALID_PARAMETER)
    }.getOrElse {
        return call.respond(
            HttpStatusCode.UnprocessableEntity,
            it.message ?: ErrorMessage.INVALID_PARAMETER
        )
    }

    /**
     * retrieve taxi routes based on origin location
     */
    val taxiRoutesResponse = runCatching {
        TaxiRepository.getTaxiRouteByOrigin(form)
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INVALID_PARAMETER
        )
    }

    return if (taxiRoutesResponse.isEmpty()) {
        call.respond(
            HttpStatusCode.NotFound,
            ErrorMessage.RESOURCE_NOT_FOUND
        )
    } else {
        call.respond(
            HttpStatusCode.OK,
            mapOf("data" to taxiRoutesResponse)
        )
    }
}
