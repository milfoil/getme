package features.taxonomy.model

data class TaxonomyTerm(
    val id: Int,
    val name: String,
    val taxonomy: TaxonomyTerm,
    val enabled: Boolean,
    val description: String? = null,
    val slug: String? = null,
    val createdAt: String? = null,
    val updatedAt: String? = null,
)
