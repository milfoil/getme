package features.taxonomy.model

import features.taxonomy.schema.TaxonomyTermTable
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class TaxonomyTermEntity(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<TaxonomyTermEntity>(TaxonomyTermTable)

    var name by TaxonomyTermTable.name
    var taxonomyId by TaxonomyEntity referencedOn TaxonomyTermTable.taxonomyId
    var enabled by TaxonomyTermTable.enabled
    var description by TaxonomyTermTable.description
    var slug by TaxonomyTermTable.slug
    var createdAt by TaxonomyTermTable.createdAt
    var updatedAt by TaxonomyTermTable.updatedAt
}
