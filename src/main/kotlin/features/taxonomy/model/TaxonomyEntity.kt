package features.taxonomy.model

import features.taxonomy.schema.TaxonomyTable
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class TaxonomyEntity(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<TaxonomyEntity>(TaxonomyTable)

    var name by TaxonomyTable.name
    var enabled by TaxonomyTable.enabled
    var description by TaxonomyTable.description
    var slug by TaxonomyTable.slug
    var createdAt by TaxonomyTable.createdAt
    var updatedAt by TaxonomyTable.updatedAt
}
