package features.taxonomy.model

data class Taxonomy(
    val id: Int,
    val name: String,
    val enabled: Boolean,
    val description: String? = null,
    val slug: String? = null,
    val createdAt: String? = null,
    val updatedAt: String? = null,
)
