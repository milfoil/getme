package features.taxonomy.schema

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object TaxonomyTable : IntIdTable("taxonomy") {
    /**
     * The name of term
     *
     * @var string
     */
    val name = varchar("name", length = 160)
    /**
     * The description of the term
     *
     * @var string
     */
    val description = varchar("description", length = 255).nullable()
    /**
     * The slug of the term
     *
     * @var string
     */
    val slug = varchar("slug", 255).nullable()
    /**
     * The status of the term
     *
     * @var string
     */
    val enabled = bool("enabled").default(true)
    /**
     * The timestamp when the record was first created in the database
     *
     * @var datetime
     */
    val createdAt = datetime("created_at").default(DateTime.now())
    /**
     * The timestamp of the most recent change to any of the data stored in the table.
     *
     * @var datetime
     */
    val updatedAt = datetime("updated_at").default(DateTime.now())
}
