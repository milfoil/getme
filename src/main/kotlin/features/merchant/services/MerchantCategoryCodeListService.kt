package features.merchant.services

import constant.ErrorMessage
import integration.mastercard.places.MastercardPlaceService
import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond

suspend fun merchantCategoryCodeListService(call: ApplicationCall) {
    /**
     * get an array of Merchant Category Codes and Merchant Category Names.
     */
    val mastercardResponse = runCatching {
        MastercardPlaceService()
            .getMerchantCategoryCodes()
            .MerchantCategoryCodeList
            .MerchantCategoryCodeArray
            .MerchantCategoryCode
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INTERNAL_SERVER_ERROR
        )
    }

    return call.respond(
        HttpStatusCode.OK,
        mapOf("merchantCategoryCodes" to mastercardResponse)

    )
}
