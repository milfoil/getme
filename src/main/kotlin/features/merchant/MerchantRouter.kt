package features.notification

import features.merchant.services.merchantCategoryCodeListService
import features.merchant.services.merchantIndustriesService
import io.ktor.application.call
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.route

fun Routing.merchantRouter() {
    route("/merchants") {
        get("/categories") { merchantCategoryCodeListService(call) }
        get("/industries") { merchantIndustriesService(call) }
    }
}
