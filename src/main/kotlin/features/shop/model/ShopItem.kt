package features.shop.model

data class ShopItem(
    val name: String,
    val price: String,
    val imageSrc: String,
    val imageAlt: String,
    val currency: String,
)
