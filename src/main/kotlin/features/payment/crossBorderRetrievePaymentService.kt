package features.payment

import constant.ErrorMessage
import integration.mastercard.crossborder.MastercardCrossBorderService
import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond

suspend fun crossBorderRetrievePaymentService(call: ApplicationCall) {

    val transactionId: String  = runCatching {
        call.parameters["id"] ?: throw IllegalArgumentException(ErrorMessage.INVALID_PARAMETER)
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INTERNAL_SERVER_ERROR
        )
    }

    /**
     * check the status of a PENDING payment using the transaction Id or transaction Reference.
     */
    val mastercardResponse = runCatching {
        MastercardCrossBorderService().getPaymentByTransactionId(transactionId)
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INTERNAL_SERVER_ERROR
        )
    }
    return call.respond(HttpStatusCode.OK, mastercardResponse)


}