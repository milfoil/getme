package features.payment

import constant.ErrorMessage
import features.payment.model.CrossBoardQuoteTypeHelper
import features.payment.model.CrossBoardQuoteTypes
import features.payment.model.CrossBorderPaymentQuoteRequest
import integration.mastercard.crossborder.MastercardCrossBorderService
import integration.mastercard.crossborder.payment.MastercardCrossBoardPaymentAmount
import integration.mastercard.crossborder.quote.MastercardCrossBoardFxType
import integration.mastercard.crossborder.quote.MastercardCrossBoardQuoteForward
import integration.mastercard.crossborder.quote.MastercardCrossBoardQuoteReverse
import integration.mastercard.crossborder.quote.MastercardCrossBorderQuoteRequest
import io.ktor.application.ApplicationCall
import io.ktor.auth.authentication
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import user.model.UserProfile
import utils.loggerUtil

/**
 * Cross Border Payment Quotes Service
 * Obtain FX rate information on a cross-border payment before it is initiated and submitted.
 */
suspend fun crossBorderPaymentQuoteService(call: ApplicationCall) {
    /**
     * retrieve the a sender's account to fund the payment.
     */
    val sender = runCatching {
        call.authentication.principal<UserProfile>()
            ?: throw Error("You are not authorized to access this application. ")
    }.getOrElse {
        return call.respond(
            HttpStatusCode.Unauthorized,
            it.message ?: call.authentication.allErrors.toString()
        )
    }

    println(sender)
    /**
     * retrieve the details of the request message.
     */
    val request = runCatching {
        call.receive<CrossBorderPaymentQuoteRequest>().apply { validate() }
    }.getOrElse {
        loggerUtil("request", it.stackTraceToString())
        return call.respond(
            HttpStatusCode.Unauthorized,
            it.message ?: call.authentication.allErrors.toString()
        )
    }

    loggerUtil("request", request)
    /**
     * unique reference number of the transaction.
     */
    val transactionRef: String = "08" + System.currentTimeMillis() + "ACFQ"
    val quoteType = CrossBoardQuoteTypeHelper.invoke(request.quoteType.type)

    /**
     * initiate remote mastercard service access
     */
    val quoteRequest = MastercardCrossBorderQuoteRequest(
        transaction_reference = transactionRef,
        sender_account_uri = "tel:+254069832",
        recipient_account_uri = request.receiverAccount,
        payment_amount = MastercardCrossBoardPaymentAmount(
            amount = request.paymentAmount.amount.toString(),
            currency = request.paymentAmount.currency
        ),
        payment_origination_country = request.countryOrigin,
        payment_type = request.paymentType,
        quote_type = when (quoteType) {
            CrossBoardQuoteTypes.Forward -> MastercardCrossBoardFxType.Forward(
                forward = MastercardCrossBoardQuoteForward(
                    fees_included = request.quoteType.applyFees,
                    receiver_currency = request.quoteType.currency,
                )
            )

            CrossBoardQuoteTypes.Reverse -> MastercardCrossBoardFxType.Reverse(
                reverse = MastercardCrossBoardQuoteReverse(
                    sender_currency = request.quoteType.currency
                )
            )
        },
        bank_code = request.bankCode,
        // additional_data = null
    )
    val mastercardResponse = runCatching {
        MastercardCrossBorderService().getQuote(
            request = quoteRequest
        )
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INTERNAL_SERVER_ERROR
        )
    }

    return call.respond(HttpStatusCode.OK, mastercardResponse)
}
