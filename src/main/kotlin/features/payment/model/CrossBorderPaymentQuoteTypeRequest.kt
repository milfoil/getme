package features.payment.model

import constant.ErrorMessage
import http.HttpException
import http.HttpRequestContext
import io.konform.validation.Validation
import utils.ValidatorTool

data class CrossBorderPaymentQuoteTypeRequest(
    val type: String,
    val currency: String,
    val applyFees: Boolean = true
) : HttpRequestContext {
    override suspend fun validate() {
        val constraint = Validation<CrossBorderPaymentQuoteTypeRequest> {
            CrossBorderPaymentQuoteTypeRequest::type required {
                ValidatorTool.lettersOnly
                CrossBoardQuoteTypeHelper.check(type)
            }

            CrossBorderPaymentQuoteTypeRequest::currency required {
                ValidatorTool.currency
            }

            CrossBorderPaymentQuoteTypeRequest::applyFees {}
        }

        /**
         * check values that users have entered into form
         * before utilizing the values in the application
         */
        if (constraint(this).errors.isNotEmpty()) {
            throw HttpException(ErrorMessage.CONSTRAINT_VIOLATION)
        }
    }
}
