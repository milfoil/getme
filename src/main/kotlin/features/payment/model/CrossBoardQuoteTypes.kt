package features.payment.model

enum class CrossBoardQuoteTypes {
    Forward, Reverse
}

data class CrossBoardForwardQuoteType(
    /**
     * This parameter indicates how payment fees will be paid.
     * If true, fees are subtracted from the sender's amount.
     * If false, then the sender will pay fees in addition to the sender's amount.
     * Note that if no type is used then it defaults to forward type with the fees_included field set to "true".
     * A forward and a reverse type shouldn't be used together. Using forward.fees_included along with reverse.sender_currency throws an error.
     * It's an optional parameter that holds a Boolean value. Valid values are "true" or "false".
     * example: "true"
     */
    val feesIncluded: Boolean = true,
    /**
     * This parameter contains the three-letter "ISO 4217" currency code of the account to receive the funds. This parameter is required for forward type where the beneficiary Account uri = pan.
     * It's an optional parameter that holds alphabet characters with an exact length of three.
     * example: "GBP
     */
    val receiverCurrency: String,
)

data class CrossBoardReverseQuoteType(
    /**
     * This parameter contains the three-letter "ISO 4217" currency code in which the sender will send the payment. This parameter is required for reverse type.
     * Note that if no type is used then it defaults to forward type with the fees_included field set to "true." A forward and a reverse type shouldn't be used together. Using forward.fees_included along with reverse.sender_currency throws an error.
     * It's an optional parameter that holds alphabet characters with an exact length of three
     * example: "USD"
     */
    val senderCurrency: String
)

object CrossBoardQuoteTypeHelper {
    fun check(value: String?): Boolean {
        if (value == null) {
            throw IllegalArgumentException("Quote type not provided.")
        }
        val items = CrossBoardQuoteTypes.values().map { it.name.toLowerCase() }
        if (!items.contains(value.toLowerCase())) {
            throw IllegalArgumentException("Invalid quote type provided.")
        }
        return true
    }

    fun invoke(value: String): CrossBoardQuoteTypes {
        check(value)
        return CrossBoardQuoteTypes.valueOf(value)
    }

    fun create(request: CrossBorderPaymentQuoteTypeRequest): CrossBoardQuoteType {
        return when (invoke(request.type)) {
            CrossBoardQuoteTypes.Forward -> CrossBoardQuoteType.Forward(
                forward = CrossBoardForwardQuoteType(
                    feesIncluded = request.applyFees,
                    receiverCurrency = request.currency
                )
            )
            CrossBoardQuoteTypes.Reverse -> CrossBoardQuoteType.Reverse(
                reverse = CrossBoardReverseQuoteType(
                    senderCurrency = request.currency
                )
            )
        }
    }
}

sealed class CrossBoardQuoteType() {
    data class Forward(
        val forward: CrossBoardForwardQuoteType
    ) : CrossBoardQuoteType()

    data class Reverse(
        val reverse: CrossBoardReverseQuoteType
    ) : CrossBoardQuoteType()
}
