package features.payment.model

import http.HttpRequestContext
import io.konform.validation.Validation
import io.konform.validation.jsonschema.maxLength
import io.konform.validation.jsonschema.minLength
import utils.ValidatorTool

data class CrossBorderPaymentQuoteRequest(
    /**
     * This parameter contains the URI identifying a beneficiary's account to receive the payment.
     * Note that in this parameter, when using the "pan" scheme, the "exp" property is optional.
     * It holds a string of alphanumeric special characters with a maximum length of 200 and a minimum length of 1. Refer to the "Recipient Account URI" section on "Additional Resources" page for details about scheme specific data.
     * example: "tel:+254069832"
     * required: yes
     */
    val receiverAccount: String,

    /**
     * PaymentAmount
     * Amount of the payment.
     * required: yes
     */
    val paymentAmount: PaymentAmountRequest,

    /**
     * This parameter contains the three-letter "ISO 3166-1 alpha-3" country code representing the country in which the payment is being initiated.
     * It holds a string of alphabet characters with an exact length of three.
     * example: "USA"
     * required: yes
     */
    val countryOrigin: String,
    /**
     * This parameter contains the three-digit code for the type of transaction that is being submitted. Available types and their uses are provided below:
     * B2P: Business Disbursement to Person - A disbursement of funds from a business to an individual account.
     * B2B: Business to Business- A transfer of funds from one business to another.
     * G2P: Government to Person - A disbursement of funds from a government agency to a private individual person's account.
     * P2P: Person to Person - A transfer of funds from one private individual person's account to another private individual person's account.
     * P2B: Person to Business - A payment by an individual person to a business
     * It holds a string of alphabet characters with an exact length of three.
     */
    val paymentType: String,

    /**
     * Either a forward quote type or a reverse quote type should be submitted, but not both.
     * All quote type parameters are optional (forward fees included = true when quote type not provided).
     * Forward quote type should not be used with the Customer Managed Sender Pricing model.
     * example: "P2P"
     * required: yes
     */
    val quoteType: CrossBorderPaymentQuoteTypeRequest,

    /**
     * This parameter contains the bank code associated with the bank name and bank identifier code (BIC) provided by the customer.
     * This parameter is required only for specific bank service provider endpoints identified as part of corridor data details.
     * It's a conditional parameter that holds alphanumeric special characters with a maximum length of 225 and a minimum length of 1.
     * example: "NP021"
     */
    val bankCode: String,

    /**
     * This parameter contains the three-letter "ISO 4217" currency code of the account to receive the funds.
     * This parameter is required for forward type where the beneficiary Account uri = pan.
     * It's an optional parameter that holds alphabet characters with an exact length of three.
     * example: "GBP
     */
    val receiverCurrency: String,
) : HttpRequestContext {
    override suspend fun validate() {
        val constraint = Validation<CrossBorderPaymentQuoteRequest> {
            // recipient
            CrossBorderPaymentQuoteRequest::receiverAccount required {}
            // amount
            CrossBorderPaymentQuoteRequest::paymentAmount {
                ValidatorTool.amount
                ValidatorTool.currency
            }
            // country
            CrossBorderPaymentQuoteRequest::countryOrigin required {
                ValidatorTool.lettersOnly
            }
            // payment type
            CrossBorderPaymentQuoteRequest::paymentType required {
                maxLength(3)
                ValidatorTool.lettersOnly
            }
            // quote type
            CrossBorderPaymentQuoteRequest::quoteType required {

                CrossBorderPaymentQuoteTypeRequest::type required {
                    ValidatorTool.lettersOnly
                    CrossBoardQuoteTypeHelper.check(quoteType.type)
                }

                CrossBorderPaymentQuoteTypeRequest::currency required {
                    ValidatorTool.currency
                }

                CrossBorderPaymentQuoteTypeRequest::applyFees {}
            }
            // currency
            CrossBorderPaymentQuoteRequest::receiverCurrency required {
                ValidatorTool.currency
            }
            // bank code
            CrossBorderPaymentQuoteRequest::bankCode required {
                ValidatorTool.alphanumeric
                minLength(3)
                maxLength(3)
            }
        }
    }
}
