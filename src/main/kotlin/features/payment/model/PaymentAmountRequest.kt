package features.payment.model

import constant.ErrorMessage
import http.HttpException
import http.HttpRequestContext
import io.konform.validation.Validation
import utils.ValidatorTool

/**
 * PaymentAmount
 * Amount of the payment.
 * required: yes
 */
data class PaymentAmountRequest(
    /**
     * This parameter contains the amount of the payment.
     * It holds a decimal number with a maximum length of 18 and a minimum length of 1..
     * example: "192.64"
     * @avar [amount]
     */
    val amount: Double,
    /**
     * This parameter contains the three-letter "ISO 4217" currency code representing the currency of the amount.
     * It holds a string of alphabet characters with an exact length of three.
     * example: "USD"
     */
    val currency: String
) : HttpRequestContext {
    override suspend fun validate() {
        val constraint = Validation<PaymentAmountRequest> {
            ValidatorTool.amount
            ValidatorTool.currency
        }
        /**
         * check values that users have entered into form
         * before utilizing the values in the application
         */
        if (constraint(this).errors.isNotEmpty()) {
            throw HttpException(ErrorMessage.CONSTRAINT_VIOLATION)
        }
    }
}
