package features.payment.model

import constant.ErrorMessage
import http.HttpException
import http.HttpRequestContext
import io.konform.validation.Validation
import io.konform.validation.jsonschema.maxLength
import io.konform.validation.jsonschema.minLength

data class CrossBorderPaymentRequest(
    /**
     * This parameter contains the system generated ID of the proposal to execute for this payment.
     * This field is returned in the Quote service.
     * Note: For these payments, the following fields filled in the quote do not need to be provided again the payment;
     * but if they are, they will process without error as long as the data matches what was sent in the Quote that generated the proposal ID.
     * An exception is for the bank_code field. If bank_code was provided in the the quote and provided again in the payment, then the newly provided value will be used.
     * If using a proposal_id and the bank_code was provided in the quote, then that value will be automatically copied into the payment.
     * It is a conditional parameter that holds a string of alphanumeric special characters with a maximum length of 30 and a minimum length of 0.
     */
    val proposalId: String,
    /**
     *  This parameter contains the client-provided unique reference number of the transaction.
     *  Note that when utilizing a quote, this parameter's value must be identical to the value created for the associated quote.
     *  It holds a string of alphanumeric special characters with a maximum length of 40 and a minimum length of 1.
     */
    val transactionId: String,
): HttpRequestContext {

    override suspend fun validate() {
        val constraint = Validation<CrossBorderPaymentRequest> {
            // validate phone number
            CrossBorderPaymentRequest::proposalId required {
                minLength(0 )
                maxLength(30)
            }

            // validate identity Number
            CrossBorderPaymentRequest::transactionId required {
                minLength(1 )
                maxLength(40)
            }
        }

        /**
         * check values that users have entered into form
         * before utilizing the values in the application
         */
        if (constraint(this).errors.isNotEmpty()) {
            throw HttpException(ErrorMessage.CONSTRAINT_VIOLATION)
        }
    }
}
