package features.payment

import constant.ErrorMessage
import features.payment.model.CrossBorderPaymentRequest
import integration.mastercard.crossborder.MastercardCrossBorderService
import integration.mastercard.crossborder.model.MastercardCrossBoardAddress
import integration.mastercard.crossborder.model.MastercardCrossBoardRecipient
import integration.mastercard.crossborder.model.MastercardCrossBoardSender
import integration.mastercard.crossborder.payment.MastercardCrossBoardPaymentAmount
import integration.mastercard.crossborder.payment.MastercardCrossBoardPaymentRequest
import io.ktor.application.ApplicationCall
import io.ktor.auth.authentication
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import user.model.UserProfile
import utils.loggerUtil

/**
 * Cross Border Payment Service
 * Create a payment transaction to send funds to a Recipient.
 */
suspend fun crossBorderCreatePaymentService(call: ApplicationCall) {
    /**
     * retrieve the a sender's account to fund the payment.
     */
    val sender = runCatching {
        call.authentication.principal<UserProfile>()
            ?: throw Error("You are not authorized to access this application. ")
    }.getOrElse {
        return call.respond(
            HttpStatusCode.Unauthorized,
            it.message ?: call.authentication.allErrors.toString()
        )
    }
    /**
     * retrieve a payment proposal
     */

    /**
     * initiate  payment
     */
    val request = runCatching {
        call.receive<CrossBorderPaymentRequest>().apply { validate() }
    }.getOrElse {
        loggerUtil("request", it.stackTraceToString())
        return call.respond(
            HttpStatusCode.Unauthorized,
            it.message ?: call.authentication.allErrors.toString()
        )
    }

    loggerUtil("request", request)
    /**
     * initiate remote mastercard service access
     */
    val paymentRequest = MastercardCrossBoardPaymentRequest(
        transaction_reference =  "08" + System.currentTimeMillis() + "ACFQ",
        // local_date_time = DateTime.now().toString("MMDDHHMMSS"),
        proposal_id = request.proposalId,
        recipient = MastercardCrossBoardRecipient(
            first_name = "Evan",
            last_name = "Rahman",
            nationality = "ZAF",
            address = MastercardCrossBoardAddress(
                city = "MOSSAT",
                postal_code = "AB33 3JD",
                country_subdivision = "NM or New Mexico",
                country = "GBR",
                line2 = "Suite 100",
                line1 = "67  Hendford Hill"
            ),
            government_ids = listOf(),
            phone = "6361115252",
            email = "customer@gmail.com"
        ),
        sender = MastercardCrossBoardSender(
            first_name = "JOHN",
            last_name = "Adam",
            nationality = "ZAF",
            address = MastercardCrossBoardAddress(
                city = "START",
                postal_code = "71279",
                country_subdivision = "NM or New Mexico",
                country = "USA",
                line2 = "Suite 100",
                line1 = "3574  Elk Street"
            ),
            government_ids = listOf(),
            date_of_birth = "1985-06-24",
        )
        // payment_amount = MastercardCrossBoardPaymentAmount(
        //     amount = "105",
        //     currency = "ZAR",
        // )
    )

    val mastercardResponse = runCatching {
        MastercardCrossBorderService().createPayment(
            request = paymentRequest
        )
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INTERNAL_SERVER_ERROR
        )
    }
    return call.respond(HttpStatusCode.OK, mastercardResponse)
}
