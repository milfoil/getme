package features.payment

import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post

fun Routing.paymentRouter() {
    // route("/payments") {
    /**
     * Cross Border API Routes
     */
    authenticate("auth-jwk") {
        post("/quotes") {
            try {
                crossBorderPaymentQuoteService(call)
            } catch (e: Exception) {
                println(e.stackTraceToString())
                throw Error(e.stackTraceToString())
            }
        }
        post("/payment") {
            try {
                crossBorderCreatePaymentService(call)
            } catch (e: Exception) {
                println(e.stackTraceToString())
                throw Error(e.stackTraceToString())
            }
        }

        get("/payment/{id}"){
            try {
                crossBorderRetrievePaymentService(call)
            } catch (e: Exception) {
                println(e.stackTraceToString())
                throw Error(e.stackTraceToString())
            }
        }
    }

    // }
}
