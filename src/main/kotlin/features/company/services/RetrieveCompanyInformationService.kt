package features.company.services

import constant.ErrorMessage
import features.company.model.CompanySearchResultRequest
import integration.verifyID.CIPCService
import integration.verifyID.model.CIPCCompanyRequest
import integration.verifyID.model.CIPCCompanyResponse
import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond

suspend fun retrieveCompanyInformationService(call: ApplicationCall) {

    /**
     * extracts the company matched against the cipc database
     */
    val request: CompanySearchResultRequest = runCatching {
        call.receive<CompanySearchResultRequest>().apply { validate() }
    }.getOrElse {
        return call.respond(
            HttpStatusCode.UnprocessableEntity,
            it.message ?: ErrorMessage.INVALID_PARAMETER
        )
    }

    /**
     *   retrieve commercial company principle (Directorship in different companies) information from CIPC
     */
    val companyResponse: CIPCCompanyResponse = runCatching {
        CIPCService().getCompany(
            CIPCCompanyRequest(
                EnquiryID = request.enquiryId,
                EnquiryResultID = request.enquiryResultId
            )
        )
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INTERNAL_SERVER_ERROR
        )
    }

    return call.respond(
        HttpStatusCode.OK,
        companyResponse
    )
}