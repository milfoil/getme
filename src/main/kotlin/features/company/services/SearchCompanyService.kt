package features.company.services

import constant.ErrorMessage
import features.company.model.SearchCompanyRequest
import integration.verifyID.CIPCService
import integration.verifyID.model.CIPCCompanySearchResponse
import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond

suspend fun searchCompanyService(call: ApplicationCall) {
    /**
     * extracts company  registration information
     */
    val request: SearchCompanyRequest = runCatching {
        call.receive<SearchCompanyRequest>().apply { validate() }
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INVALID_PARAMETER
        )
    }

    /**
     *  Search the CIPC Database to match results before final search
     */
    val companyMatches: CIPCCompanySearchResponse = runCatching {
        CIPCService().companySearch(
            request.companyName,
            request.registrationNumber
        )
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INTERNAL_SERVER_ERROR
        )
    }

    return call.respond(
        HttpStatusCode.OK,
        companyMatches
    )
}