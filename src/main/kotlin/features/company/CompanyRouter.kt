package features.company

import features.company.services.retrieveCompanyInformationService
import features.company.services.searchCompanyService
import io.ktor.application.call
import io.ktor.routing.Routing
import io.ktor.routing.post

fun Routing.companyRouter() {
    post("/register/company") {
        searchCompanyService(call)
    }

    post("/company") {
        retrieveCompanyInformationService(call)
    }

}