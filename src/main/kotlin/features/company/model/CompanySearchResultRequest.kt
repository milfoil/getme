package features.company.model

import constant.ErrorMessage
import http.HttpException
import http.HttpRequestContext
import io.konform.validation.Validation
import io.konform.validation.jsonschema.pattern

data class CompanySearchResultRequest(
    /**
     * EnquiryID - returned from the Company Match Function
     */
    val enquiryId: String,
    /**
     * EnquiryResultID - returned from the Company Match Function
     */
    val enquiryResultId: String
)  : HttpRequestContext {
    override suspend fun validate() {

        val constraint = Validation<CompanySearchResultRequest> {
            // validate company name
            CompanySearchResultRequest::enquiryId required {
                pattern(Regex("^[0-9]*\$")) hint "invalid enquiry id"
            }

            // validate identity Number
            CompanySearchResultRequest::enquiryResultId required {
                pattern(Regex("^[0-9]*\$")) hint "invalid enquiry result id"
            }
        }

        /**
         * check values that users have entered into form
         * before utilizing the values in the application
         */
        if (constraint(this).errors.isNotEmpty()) {
            throw HttpException(ErrorMessage.CONSTRAINT_VIOLATION)
        }
    }
}
