package features.company.model

import constant.ErrorMessage
import http.HttpException
import http.HttpRequestContext
import io.konform.validation.Validation
import io.konform.validation.jsonschema.maxLength
import io.konform.validation.jsonschema.minLength
import io.konform.validation.jsonschema.pattern
import user.model.ForgotPasswordRequest
import user.repository.UserRepository

data class SearchCompanyRequest(
    /**
     * Company registration number
     * optional if Company name has been provided
     */
    val registrationNumber: String? = null,
    /**
     *  Company Name
     */
    val companyName: String,
) : HttpRequestContext {
    override suspend fun validate() {

        val constraint = Validation<SearchCompanyRequest> {
            // validate company name
            SearchCompanyRequest::companyName required {
                minLength(1)
                maxLength(160)
            }

            // validate identity Number
            SearchCompanyRequest::registrationNumber ifPresent {
                pattern(Regex("[A-Z]+\\d+/+\\d+/+\\d*")) hint "invalid identity number"
            }
        }

        /**
         * check values that users have entered into form
         * before utilizing the values in the application
         */
        if (constraint(this).errors.isNotEmpty()) {
            throw HttpException(ErrorMessage.CONSTRAINT_VIOLATION)
        }
    }
}