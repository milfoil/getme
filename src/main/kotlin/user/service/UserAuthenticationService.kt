package user.service

import constant.ErrorMessage
import integration.aws.AwsCognitoServiceProvider
import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import user.model.UserAuthenticateRequest
import utils.loggerUtil

/**
 * Initiates the authentication flow.
 */
suspend fun userAuthenticationService(call: ApplicationCall) {
    /**
     * extract credentials [data] from the request
     */
    val data: UserAuthenticateRequest = runCatching {
        call.receive<UserAuthenticateRequest>().apply { validate() }
    }.getOrElse {
        return call.respond(
            HttpStatusCode.UnprocessableEntity,
            it.message ?: ErrorMessage.INVALID_PARAMETER
        )
    }

    /**
     * Initiates user authentication
     */
    val cognitoServiceResponse = runCatching {
        AwsCognitoServiceProvider.initiateAuth(
            username = data.phoneNumber,
            password = data.password
        )
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INTERNAL_SERVER_ERROR
        )
    }

    loggerUtil("AWS User Authentication", cognitoServiceResponse)

    return call.respond(
        HttpStatusCode.OK,
        mapOf(
            "expiresIn" to cognitoServiceResponse.authenticationResult().expiresIn(),
            "accessToken" to cognitoServiceResponse.authenticationResult().accessToken(),
            "tokenType" to cognitoServiceResponse.authenticationResult().tokenType(),
        )
    )
}
