package user.service

import constant.ErrorMessage
import integration.aws.AwsCognitoServiceProvider
import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import user.model.ForgotPasswordRequest
import utils.loggerUtil

/**
 * Sent the end user a confirmation code that is required to change to change the password
 */
suspend fun forgotPasswordService(call: ApplicationCall) {
    /**
     * extract user information from request
     */
    val form: ForgotPasswordRequest = runCatching {
        call.receive<ForgotPasswordRequest>().apply { validate() }
    }.getOrElse {
        return call.respond(
            HttpStatusCode.UnprocessableEntity,
            it.message ?: ErrorMessage.INVALID_PARAMETER
        )
    }

    /**
     * initiate forgot password
     */
    val cognitoServiceResponse = runCatching {
        AwsCognitoServiceProvider.forgotPassword(form.phoneNumber)
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INTERNAL_SERVER_ERROR
        )
    }

    loggerUtil("AWS Forgot Password", cognitoServiceResponse)

    return call.respond(
        HttpStatusCode.OK,
        "A Confirmation code has been sent to ${cognitoServiceResponse.codeDeliveryDetails().destination()}"
    )
}
