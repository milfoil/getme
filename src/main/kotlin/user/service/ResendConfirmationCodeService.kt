package user.service

import constant.ErrorMessage
import integration.aws.AwsCognitoServiceProvider
import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import user.model.ResendConfirmationCodeRequest
import utils.loggerUtil

/**
 * Resends the confirmation (for confirmation of registration) to a specific user in the user pool.
 */

suspend fun resendConfirmationCodeService(call: ApplicationCall) {

    /**
     * extract user phone number from request
     */
    val form: ResendConfirmationCodeRequest = runCatching {
        call.receive<ResendConfirmationCodeRequest>().apply { validate() }
    }.getOrElse {
        return call.respond(
            HttpStatusCode.UnprocessableEntity,
            it.message ?: ErrorMessage.MISSING_PARAMETER
        )
    }

    /**
     * Confirms registration of a user
     */
    val cognitoServiceResponse = runCatching {
        AwsCognitoServiceProvider.resendConfirmationCode(form.phoneNumber)
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INTERNAL_SERVER_ERROR
        )
    }

    loggerUtil("Resend Confirmation Code", cognitoServiceResponse)

    /**
     * retrieve user account
     */
    return call.respond(
        HttpStatusCode.OK,
        "A Confirmation code has been sent to ${cognitoServiceResponse.codeDeliveryDetails().destination()}"
    )
}
