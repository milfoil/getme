package user.service

import constant.ErrorMessage
import integration.aws.AwsCognitoServiceProvider
import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import user.model.ConfirmForgotPasswordRequest
import utils.loggerUtil

/**
 * ConfirmForgotPasswordService
 * Allows a user to enter a confirmation code to reset a forgotten password.
 */
suspend fun confirmForgotPasswordService(call: ApplicationCall) {
    /**
     * extract forgot password [data] from request
     */
    val data: ConfirmForgotPasswordRequest = runCatching {
        call.receive<ConfirmForgotPasswordRequest>().apply { validate() }
    }.getOrElse {
        return call.respond(
            HttpStatusCode.UnprocessableEntity,
            it.message ?: ErrorMessage.INVALID_PARAMETER
        )
    }

    /**
     * initiate reset forgotten password.
     */
    val cognitoServiceResponse = runCatching {
        AwsCognitoServiceProvider.confirmForgotPassword(
            username = data.phoneNumber,
            password = data.password,
            confirmationCode = data.confirmationCode
        )
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INTERNAL_SERVER_ERROR
        )
    }

    loggerUtil("AWS Confirm Forgot Password", cognitoServiceResponse)

    return call.respond(
        HttpStatusCode.OK,
        "Your password has been changed successfully."
    )
}
