package user.service

import constant.ErrorMessage
import integration.aws.AwsCognitoServiceProvider
import integration.verifyID.VerifyIdService
import integration.verifyID.model.VerifyIdRealTime
import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import user.model.UserRegisterRequest
import user.repository.UserRepository
import user.repository.model.CreateUserEntity
import utils.PasswordUtil
import utils.loggerUtil
import java.util.UUID

suspend fun userRegisterRequestHandler(call: ApplicationCall) {
    /**
     * extract user registration information
     */
    val form: UserRegisterRequest = runCatching {
        call.receive<UserRegisterRequest>().apply { validate() }
    }.getOrElse {
        return call.respond(
            HttpStatusCode.UnprocessableEntity,
            it.message ?: ErrorMessage.MISSING_PARAMETER
        )
    }

    /**
     * check the validity of id number that users have entered
     * against the home affairs database
     * before submitting the values to the database
     */
    val verifyIdResponse: VerifyIdRealTime = runCatching {
        VerifyIdService(form.identityNumber).verifyIdInRealTime()
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.VALIDATION_ERROR
        )
    }

    /**
     * add user to the aws identity manager service
     */
    val cognitoServiceResponse = runCatching {
        AwsCognitoServiceProvider.signUp(form.phoneNumber, form.password)
    }.getOrElse {
        return call.respond(
            HttpStatusCode.UnprocessableEntity,
            it.message ?: ErrorMessage.MISSING_PARAMETER
        )
    }

    /**
     * generate a hashed user password
     */
    val userEntity = CreateUserEntity(
        id = UUID.fromString(cognitoServiceResponse.userSub()),
        idNumber = verifyIdResponse.idNumber,
        phoneNumber = form.phoneNumber,
        firstNames = verifyIdResponse.firstNames,
        surname = verifyIdResponse.surName,
        birthdate = verifyIdResponse.dob,
        age = verifyIdResponse.age,
        gender = verifyIdResponse.gender,
        deceasedStatus = verifyIdResponse.deceasedStatus,
        citizenship = verifyIdResponse.citizenship,
        password = PasswordUtil(form.password).hash()
    )

    /**
     * Create a new user in the database
     */
    val user = runCatching {
        UserRepository.createUser(userEntity)
    }.getOrElse {
        return call.respond(
            HttpStatusCode.UnprocessableEntity,
            it.message ?: ErrorMessage.INTERNAL_SERVER_ERROR
        )
    }

    loggerUtil("User Account Created", user)
    return call.respond(
        HttpStatusCode.Created,
        "You have successfully registered."
    )
}
