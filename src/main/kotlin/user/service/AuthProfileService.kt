package user.service

import io.ktor.application.ApplicationCall
import io.ktor.auth.authentication
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import user.model.UserProfile

suspend fun authProfileService(call: ApplicationCall) {
    /**
     * retrieve user profile from token
     */
    val auth = runCatching {
        call.authentication.principal<UserProfile>()
            ?: throw Error("You are not authorized to access this application. ")
    }.getOrElse {
        return call.respond(
            HttpStatusCode.Unauthorized,
            it.message ?: call.authentication.allErrors.toString()
        )
    }

    return call.respond(
        HttpStatusCode.OK,
        auth
    )
}
