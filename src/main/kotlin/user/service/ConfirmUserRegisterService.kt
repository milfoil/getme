package user.service

import constant.ErrorMessage
import integration.aws.AwsCognitoServiceProvider
import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import user.model.ConfirmUserRegisterRequest
import user.repository.UserRepository
import utils.loggerUtil

/**
 * Confirms registration of a user.
 */
suspend fun confirmUserRegisterService(call: ApplicationCall) {

    /**
     * extract confirm user registration information
     */
    val form: ConfirmUserRegisterRequest = runCatching {
        call.receive<ConfirmUserRegisterRequest>().apply { validate() }
    }.getOrElse {
        return call.respond(
            HttpStatusCode.UnprocessableEntity,
            it.message ?: ErrorMessage.INVALID_PARAMETER
        )
    }

    /**
     * Confirms registration of a user
     */
    val cognitoServiceResponse = runCatching {
        AwsCognitoServiceProvider.confirmSignUp(form.phoneNumber, form.confirmationCode)
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INTERNAL_SERVER_ERROR
        )
    }

    loggerUtil("AWS Confirm Register", cognitoServiceResponse)

    /**
     * retrieve user account
     */
    val user = runCatching {
        UserRepository.verifyUserPhoneNumber(form.phoneNumber)
    }.getOrElse {
        return call.respond(
            HttpStatusCode.BadRequest,
            it.message ?: ErrorMessage.INTERNAL_SERVER_ERROR
        )
    }

    loggerUtil("Confirm User Account", user)
    return call.respond(
        HttpStatusCode.OK,
        "Congratulations! Your registration is confirmed!"
    )
}
