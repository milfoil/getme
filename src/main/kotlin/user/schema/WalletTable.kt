package user.schema

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object WalletTable : LongIdTable("wallet") {
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val accountNumber = varchar("account_number", length = 100).uniqueIndex()
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val alias = varchar("alias", length = 50).nullable()
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val currencyId = reference("currency_id", CurrencyTable)
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val withdrawalLimit = integer("withdrawal_limit").default(4000)
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val transferLimit = integer("transfer_limit").default(24000)
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val paymentLimit = integer("payment_limit").default(4000)
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val depositLimit = integer("deposit_limit").default(5000)
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val availableBalance = decimal("available_balance", 18, 2)
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val currentBalance = decimal("current_balance", 18, 2)

    /**
     * The person unique who owns the wallet
     * @var uuid
     */
    val userId = UserTable.reference("user_id", UserTable)

    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val reservedAmount = decimal("reserved_amount", 18, 2)
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val createdAt = datetime("created_at").default(DateTime())
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val updatedAt = datetime("updated_at").default(DateTime())
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val deletedAt = datetime("deleted_at").nullable()
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val blockedAt = datetime("blocked_at").nullable()
}
