package user.schema

import features.taxonomy.schema.TaxonomyTermTable
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object AddressTable : LongIdTable("address") {
    /**
     * The address house/building number
     *
     * @var string
     */
    val unit = varchar("unit", length = 36).nullable() //
    /**
     * The address house/building name
     *
     * @var string
     */
    val buildingName = varchar("building_name", length = 180).nullable()
    /**
     * The address street/road name
     *
     * @var string
     */
    val line1 = varchar("line1", length = 180)
    /**
     * The address street/road name
     *
     * @var string
     */
    val line2 = varchar("line2", length = 180).nullable()
    /**
     * The address city/town name
     *
     * @var string
     */
    val city = varchar("city", length = 36)
    /**
     * The address state/province name
     *
     * @var string
     */
    val state = varchar("state", length = 36)
    /**
     *  The address postal code
     *
     * @var string
     */
    val postalCode = varchar("postal_code", length = 10)
    /**
     * The address county/region name
     *
     * @var string
     */
    val countryId = reference("country_id", CountryTable)
    /**
     * The address county/region name
     *
     * @var string
     */
    val typeId = reference("type_id", TaxonomyTermTable)
    /**
     * The address location latitude
     *
     * @var string
     */
    val latitude = varchar("latitude", length = 255).nullable()
    /**
     * The address location longitude to locate
     *
     * @var string
     */
    val longitude = varchar("longitude", length = 255).nullable()
    /**
     * The timestamp when the record was first created in the database
     *
     * @var string
     */
    val createdAt = datetime("created_at").default(DateTime.now())
    /**
     * The timestamp of the most recent change to any of the data stored in the table.
     *
     * @var string
     */
    val updatedAt = datetime("updated_at").default(DateTime.now())
    /**
     * The timestamp when the person was deleted in the database
     *
     * @var string
     */
    val deletedAt = datetime("deleted_at").nullable()
}
