package user.schema

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object CountryTable : IntIdTable("country") {
    /**
     * The name of country
     *
     * @var string
     */
    val name = varchar("name", length = 180)

    /**
     * The ISO 3166-1 2-letter country code
     *
     * @var string
     */
    val alpha2Code = varchar("alpha2_code", length = 4)

    /**
     * The ISO 3166-1 3-letter country code
     *
     * @var string
     */
    val alpha3Code = varchar("alpha3_code", length = 3)

    /**
     * The capital city of country code
     *
     * @var string
     */
    val capital = varchar("capital", length = 36)

    /**
     * The region of country
     *
     * @var string
     */
    val region = varchar("region", length = 36)

    /**
     * The flag of country
     *
     * @var string
     */
    val flag = varchar("flag", length = 255)

    /**
     * The native name of country
     *
     * @var string
     */
    val nativeName = varchar("native_name", length = 255)

    /**
     * The calling code of country
     *
     * @var string
     */
    val diallingCode = varchar("dialling_code", length = 4)

    /**
     * The location latitude of country
     *
     * @var string
     */
    val latitude = varchar("latitude", length = 255).nullable()

    /**
     * The location longitude of country
     *
     * @var string
     */
    val longitude = varchar("longitude", length = 255).nullable()
    /**
     * The timestamp when the record was first created in the database
     *
     * @var datetime
     */
    val createdAt = datetime("created_at").default(DateTime.now())

    /**
     * The timestamp of the most recent change to any of the data stored in the table.
     *
     * @var datetime
     */
    val updatedAt = datetime("updated_at").default(DateTime.now())

    /**
     * The timestamp when the person was deleted in the database
     *
     * @var datetime
     */
    val deletedAt = datetime("deleted_at").nullable()
}
