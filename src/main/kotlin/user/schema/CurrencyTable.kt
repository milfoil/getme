package user.schema

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object CurrencyTable : IntIdTable("currency") {
    val name = varchar("name", length = 180)
    val code = varchar("code", length = 5)
    val symbol = varchar("symbol", length = 5)
    val countryId = reference("country_id", CountryTable)
    val createdAt = datetime("created_at").default(DateTime.now())
    val updatedAt = datetime("updated_at").default(DateTime.now())
    val deletedAt = datetime("deleted_at").nullable()
}
