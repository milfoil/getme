package user.schema.model

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import user.schema.CurrencyTable

class CurrencyEntity(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<CurrencyEntity>(CurrencyTable)
}
