package user.schema.model

import features.taxonomy.model.TaxonomyTermEntity
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import user.schema.UserTable
import java.util.UUID

class UserEntity(id: EntityID<UUID>) : UUIDEntity(id) {
    var identityNumber by UserTable.identityNumber
    var phoneNumber by UserTable.phoneNumber

    var names by UserTable.names
    var surname by UserTable.surname
    var age by UserTable.age
    var birthdate by UserTable.birthdate
    var gender by UserTable.gender
    var nationality by CountryEntity referencedOn UserTable.nationality
    var email by UserTable.email
    var photo by UserTable.photo
    var deceasedStatus by UserTable.deceasedStatus
    var password by UserTable.password
    var addressId by AddressEntity optionalReferencedOn UserTable.addressId
    var statusId by TaxonomyTermEntity referencedOn UserTable.statusId
    var roleId by TaxonomyTermEntity referencedOn UserTable.roleId
    var lastLoginAt by UserTable.lastLoginAt
    var loginAttempt by UserTable.loginAttempt
    var createdAt by UserTable.createdAt
    var updatedAt by UserTable.updatedAt
    var deletedAt by UserTable.deletedAt
    var blockedAt by UserTable.blockedAt
    var verifiedAt by UserTable.verifiedAt
    var deceasedAt by UserTable.deceasedAt

    companion object : UUIDEntityClass<UserEntity>(UserTable)
}
