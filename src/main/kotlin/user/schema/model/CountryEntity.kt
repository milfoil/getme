package user.schema.model

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import user.schema.CountryTable

class CountryEntity(id: EntityID<Int>) : IntEntity(id) {
    var name by CountryTable.name
    var alpha2Code by CountryTable.alpha2Code
    var alpha3Code by CountryTable.alpha3Code
    var capital by CountryTable.capital
    var region by CountryTable.region
    var flag by CountryTable.flag
    var nativeName by CountryTable.nativeName
    var diallingCode by CountryTable.diallingCode
    var latitude by CountryTable.latitude
    var longitude by CountryTable.longitude
    val createdAt by CountryTable.createdAt
    val updatedAt by CountryTable.updatedAt
    var deletedAt by CountryTable.updatedAt

    companion object : IntEntityClass<CountryEntity>(CountryTable) {
    }
}
