package user.schema.model

import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import user.schema.AddressTable

class AddressEntity(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<AddressEntity>(AddressTable)

    var unit by AddressTable.unit
    var buildingName by AddressTable.buildingName
    var line1 by AddressTable.line1
    var line2 by AddressTable.line2
    var city by AddressTable.city
    var state by AddressTable.state
    var postalCode by AddressTable.postalCode
    val countryId by CountryEntity referrersOn AddressTable.countryId
    var latitude by AddressTable.latitude
    var longitude by AddressTable.longitude
    val createdAt by AddressTable.createdAt
    val updatedAt by AddressTable.updatedAt
    var deletedAt by AddressTable.deletedAt
}
