package user.schema.model

import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import user.schema.WalletTable

class WalletEntity(id: EntityID<Long>) : LongEntity(id) {
    var accountNumber by WalletTable.accountNumber
    var alias by WalletTable.alias
    var withdrawalLimit by WalletTable.withdrawalLimit
    var paymentLimit by WalletTable.paymentLimit
    var depositLimit by WalletTable.depositLimit
    var transferLimit by WalletTable.transferLimit
    var availableBalance by WalletTable.availableBalance
    var currentBalance by WalletTable.currentBalance
    var reservedAmount by WalletTable.reservedAmount
    var currencyId by CurrencyEntity referencedOn WalletTable.currencyId
    var createdAt by WalletTable.createdAt
    var updatedAt by WalletTable.updatedAt
    var deletedAt by WalletTable.deletedAt
    var blockedAt by WalletTable.blockedAt

    companion object : LongEntityClass<WalletEntity>(WalletTable)
}
