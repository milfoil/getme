package user.schema

import features.taxonomy.schema.TaxonomyTermTable
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object UserTable : UUIDTable("user") {
    /**
     * The person national identity number .
     *
     * @var string
     */
    val identityNumber = varchar("identity_number", length = 13).uniqueIndex()

    /**
     * The person contact number .
     *
     * @var string
     */
    val phoneNumber = varchar("phone_number", length = 20).uniqueIndex()

    /**
     * The person given names.
     *
     * @var string
     */
    val names = varchar("names", length = 160)

    /**
     * The person family name.
     *
     * @var string
     */
    val surname = varchar("surname", length = 160)

    /**
     * The person age
     *
     * @var integer
     */
    val age = integer("age")

    /**
     * the person birthdate
     *
     * @var datetime
     */
    val birthdate = datetime("birthdate")

    /**
     * The person gender
     *
     * @var string
     */
    val gender = varchar("gender", length = 6)

    /**
     * The person nationality
     *
     * @var integer
     */
    val nationality = reference("nationality_id", CountryTable)

    /**
     * The person contact email address
     *
     * @var string
     */
    val email = varchar("email", length = 255).nullable()

    /**
     * The person deceased status
     *
     * @var string
     */
    val deceasedStatus = varchar("deceased_status", length = 50).nullable()

    /**
     * The picture of the person
     *
     * @var long
     */
    val photo = varchar("photo", length = 255).nullable()
    /**
     * The person residential address
     *
     * @var long
     */
    val addressId = reference("address_id", AddressTable).nullable()
    /**
     * Unique identifier of the secret key used to gain access
     *
     * @var string
     */
    val password = varchar("password", 255)
    /**
     * The role assigned to the user
     *
     * @var integer
     */
    val roleId = reference("role_id", TaxonomyTermTable)
    /**
     * The status assigned to the user
     *
     * @var integer
     */
    val statusId = reference("status_id", TaxonomyTermTable)

    /**
     * The last time user log in
     *
     * @var string
     */
    val lastLoginAt = datetime("last_login_at").nullable()
    /**
     * show the number of attempts against a existing user with an incorrect password.
     *
     * @var string
     */
    val loginAttempt = integer("login_attempt")
    /**
     * The timestamp when the record was first created in the database
     *
     * @var datetime
     */
    val createdAt = datetime("created_at").default(DateTime.now())
    /**
     * The timestamp of the most recent change to any of the data stored in the table.
     *
     * @var datetime
     */
    val updatedAt = datetime("updated_at").default(DateTime.now())
    /**
     * The timestamp when the person was deleted in the database
     *
     * @var datetime
     */
    val deletedAt = datetime("deleted_at").nullable()
    /**
     * The timestamp when the person was blocked in the database
     *
     * @var datetime
     */
    val blockedAt = datetime("blocked_at").nullable()
    /**
     * The timestamp when the person was blocked in the database
     *
     * @var datetime
     */
    val verifiedAt = datetime("verified_at").nullable()

    /**
     * The timestamp when the person was verified in the database
     *
     * @var datetime
     */
    val deceasedAt = datetime("deceased_at").nullable()
}
