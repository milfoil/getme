package user.modules.address

import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import user.schema.CountryTable
import user.schema.model.CountryEntity

object AddressRepository {
    /**
     * find country or fail
     */
    suspend fun getCountryByNameOrFail(name: String): CountryEntity = newSuspendedTransaction {
        return@newSuspendedTransaction CountryEntity.find {
            CountryTable.name eq name
        }.firstOrNull() ?: throw Error("The country '$name' you are looking for is not available in our records.")
    }
}
