package user.modules.address

data class Address(
    val id: Long,
    var unit: String? = null,
    var buildingName: String? = null,
    var line1: String,
    var line2: String? = null,
    var city: String,
    var state: String,
    var postalCode: String,
    val country: String,
    var latitude: String? = null,
    var longitude: String? = null,
)
