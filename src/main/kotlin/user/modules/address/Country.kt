package user.modules.address

import org.joda.time.DateTime

data class Country(
    val id: Int,
    var name: String,
    var alpha2Code: String,
    var alpha3Code: String,
    var capital: String,
    var region: String,
    var flag: String,
    var nativeName: String,
    var diallingCode: String,
    var latitude: String,
    var longitude: String,
    val createdAt: DateTime,
    val updatedAt: DateTime,
    var deletedAt: DateTime? = null
)
