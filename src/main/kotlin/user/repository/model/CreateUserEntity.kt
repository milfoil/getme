package user.repository.model

import java.util.UUID

data class CreateUserEntity(
    /**
     * unique identifier of the person
     * @var string
     */
    val id: UUID,
    /**
     * Firstnames of the person
     * @var string
     */
    val idNumber: String,

    /**
     * Phone number of the person
     * @var string
     */
    val phoneNumber: String,

    /**
     * Firstnames of the person
     * @var string
     */
    val firstNames: String,
    /**
     * The family name of the person
     * @var string
     */
    val surname: String,
    /**
     * The birthdate of the person
     * @var string
     */
    val birthdate: String,
    /**
     * The current age of the person
     * @var Int
     */
    val age: Int,
    /**
     * The gender of the person
     * @var string
     */
    val gender: String,
    /**
     * The citizenship of the person
     * @var string
     */
    val deceasedStatus: String,
    /**
     * The citizenship of the person
     * @var string
     */
    val citizenship: String,

    /**
     * The date which the identity was issued
     */
    val photo: String? = null,

    /**
     * Email address of the person
     */
    val email: String? = null,

    /**
     * Role of the person
     */
    var roleId: Int = 4,

    /**
     * The access key of the person
     */
    val password: String,

)
