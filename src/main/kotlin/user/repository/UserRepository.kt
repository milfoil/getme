package user.repository

import features.taxonomy.model.TaxonomyTermEntity
import http.HttpException
import org.jetbrains.exposed.dao.with
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.joda.time.DateTime
import user.model.UserProfile
import user.model.UserRegisterRequest
import user.modules.address.Address
import user.modules.address.AddressRepository
import user.repository.model.CreateUserEntity
import user.schema.UserTable
import user.schema.model.CountryEntity
import user.schema.model.UserEntity
import utils.loggerUtil

object UserRepository {
    /**
     * check whether a user account already exist
     */
    suspend fun isFound(user: UserRegisterRequest): UserEntity? = newSuspendedTransaction {
        return@newSuspendedTransaction UserEntity.find {
            UserTable.phoneNumber eq user.phoneNumber or (UserTable.identityNumber eq user.identityNumber)
        }.firstOrNull()
    }

    /**
     * create a new user in the database
     */
    suspend fun createUser(context: CreateUserEntity): UserEntity = newSuspendedTransaction {
        /**
         * fetch the user role based on the user role id
         */
        val role: TaxonomyTermEntity =
            TaxonomyTermEntity.findById(context.roleId)
                ?: throw Error("The user role you are looking for is not available.")

        /**
         * fetch the default user account status based on the status id
         */
        val status: TaxonomyTermEntity =
            TaxonomyTermEntity.findById(13)
                ?: throw Error("The user account status you are looking for is not available.")

        /**
         * fetch the nationality based on the user citizenship
         */
        val country: CountryEntity = AddressRepository.getCountryByNameOrFail(context.citizenship)

        /**
         * create a new user resource
         */
        return@newSuspendedTransaction runCatching {
            UserEntity.new(context.id) {
                identityNumber = context.idNumber
                phoneNumber = context.phoneNumber
                names = context.firstNames
                surname = context.surname
                email = context.email
                photo = context.photo
                age = context.age
                birthdate = DateTime.parse(context.birthdate)
                gender = context.gender
                deceasedStatus = context.deceasedStatus
                addressId = null
                roleId = role
                statusId = status
                loginAttempt = 0
                nationality = country
                password = context.password
                createdAt = DateTime.now()
                updatedAt = DateTime.now()
            }
        }.getOrElse {
            loggerUtil("CreateUserEntity", it)
            throw HttpException(it.message, it.cause)
        }
    }

    /**
     * find user by phone number or fail
     */
    suspend fun getUserByPhoneNumberOrFail(phoneNumber: String): UserEntity = newSuspendedTransaction {
        return@newSuspendedTransaction UserEntity.find {
            UserTable.phoneNumber eq phoneNumber
        }.firstOrNull()
            ?: throw Error("The phone number ($phoneNumber) you specified is not available in our records.")
    }

    /**
     * set the time user confirmed registration
     */
    suspend fun verifyUserPhoneNumber(phoneNumber: String) = newSuspendedTransaction {
        val user = UserEntity.find {
            UserTable.phoneNumber eq phoneNumber
        }.firstOrNull()
            ?: throw Error("The phone number ($phoneNumber) you specified is not available in our records.")

        // set the current timestamp
        user.verifiedAt = DateTime()
        return@newSuspendedTransaction user
    }

    /**
     * find user associated with the provided identity and phone number
     */
    suspend fun getUserByCredentials(phoneNumber: String, identityNumber: String) = newSuspendedTransaction {
        return@newSuspendedTransaction UserEntity.find {
            UserTable.phoneNumber eq phoneNumber and (UserTable.identityNumber eq identityNumber)
        }.firstOrNull() ?: throw Error("No matches found for that user identity/phone number combination.")
    }

    /**
     * find the full user profile with the provided phone number
     */
    suspend fun getUserProfileOrFail(phoneNumber: String): UserProfile = newSuspendedTransaction {
        val userEntity = UserEntity.find {
            UserTable.phoneNumber eq phoneNumber
        }.with(UserEntity::nationality, UserEntity::roleId, UserEntity::statusId, UserEntity::addressId).firstOrNull()
            .let {
                it ?: throw Error("No matches found for that user in our records.")
            }
        return@newSuspendedTransaction UserProfile(
            id = userEntity.id.value,
            identityNumber = userEntity.identityNumber,
            phoneNumber = userEntity.phoneNumber,
            names = userEntity.names,
            surname = userEntity.surname,
            age = userEntity.age,
            birthdate = userEntity.birthdate.toString("dd-MM-yyyy"),
            address = userEntity.addressId?.let {
                Address(
                    id = it.id.value,
                    unit = it.unit,
                    buildingName = it.buildingName,
                    line1 = it.line1,
                    line2 = it.line2,
                    city = it.city,
                    state = it.state,
                    postalCode = it.postalCode,
                    country = userEntity.nationality.name,
                    latitude = it.latitude,
                    longitude = it.latitude,
                )
            },
            gender = userEntity.gender,
            nationality = userEntity.nationality.name,
            email = userEntity.email,
            photo = userEntity.photo,
            deceasedStatus = userEntity.deceasedStatus,
            status = userEntity.statusId.name,
            role = userEntity.roleId.name,
            lastLoginAt = userEntity.lastLoginAt?.toString("dd/MM/yyyy H:m:s"),
            createdAt = userEntity.createdAt.toString("dd/MM/yyyy H:m:s"),
            verifiedAt = userEntity.verifiedAt?.toString("dd/MM/yyyy H:m:s"),
            deceasedAt = userEntity.deceasedAt?.toString("dd/MM/yyyy H:m:s"),
            blockedAt = userEntity.blockedAt?.toString("dd/MM/yyyy H:m:s"),
        )
    }

    /**
     * log user last session
     */
}
