package user

import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post
import user.service.authProfileService
import user.service.confirmForgotPasswordService
import user.service.confirmUserRegisterService
import user.service.forgotPasswordService
import user.service.resendConfirmationCodeService
import user.service.userAuthenticationService
import user.service.userRegisterRequestHandler

fun Routing.userRouter() {
    /**
     * public routes
     */
    post("/register/user") { userRegisterRequestHandler(call) }
    post("/confirmRegister") { confirmUserRegisterService(call) }
    post("/resendConfirmationCode") { resendConfirmationCodeService(call) }
    post("/forgotPassword") { forgotPasswordService(call) }
    post("/confirmForgotPassword") { confirmForgotPasswordService(call) }
    post("/login") { userAuthenticationService(call) }
    /**
     * protected routes
     */
    authenticate("auth-jwk") {
        get("/me") { authProfileService(call) }
    }
}
