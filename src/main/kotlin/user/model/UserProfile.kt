package user.model

import io.ktor.auth.Principal
import user.modules.address.Address
import java.util.UUID

data class UserProfile(
    val id: UUID,
    val identityNumber: String,
    val phoneNumber: String,
    val names: String,
    val surname: String,
    val age: Int,
    val birthdate: String,
    val gender: String,
    val address: Address? = null,
    val nationality: String,
    val email: String? = null,
    val photo: String? = null,
    val deceasedStatus: String?,
    val status: String,
    val role: String,
    val lastLoginAt: String? = null,
    val createdAt: String,
    val blockedAt: String? = null,
    val verifiedAt: String? = null,
    val deceasedAt: String? = null,
) : Principal
