package user.model

import constant.ErrorMessage
import http.HttpException
import http.HttpRequestContext
import io.konform.validation.Validation
import io.konform.validation.jsonschema.pattern
import user.repository.UserRepository
import utils.PasswordUtil

data class UserRegisterRequest(
    val identityNumber: String,
    val phoneNumber: String,
    val password: String,
) : HttpRequestContext {
    override suspend fun validate() {
        val constraint = Validation<UserRegisterRequest> {
            // validate phone number
            UserRegisterRequest::phoneNumber required {
                pattern(Regex("^[0-9]{10}\$")) hint "invalid phone number"
            }
            // validate identity Number
            UserRegisterRequest::identityNumber required {
                pattern(Regex("^\\d{13}$")) hint "invalid identity number"
            }

            // validate Password
            UserRegisterRequest::password required {
                pattern(PasswordUtil.PASSWORD_REGEX) hint "invalid password"
            }
        }

        /**
         * check values that users have entered into form
         * before utilizing the values in the application
         */

        println(constraint(this).errors)
        if (constraint(this).errors.isNotEmpty()) {
            throw HttpException(ErrorMessage.CONSTRAINT_VIOLATION)
        }

        /**
         * check values that users entered into form
         * does not exist before submitting the values to the database
         */
        UserRepository.isFound(this)?.let {
            throw HttpException("An account with this phone or id number already exist.")
        }
    }
}
