package user.model

import constant.ErrorMessage
import http.HttpException
import http.HttpRequestContext
import io.konform.validation.Validation
import io.konform.validation.jsonschema.pattern

data class ConfirmUserRegisterRequest(
    /**
     * The phone number of the user whose registration you wish to confirm.
     * @var [phoneNumber] String
     */
    val phoneNumber: String,
    /**
     * The confirmation code sent by a user's request to confirm registration.
     * @var [confirmationCode] String
     */
    val confirmationCode: String,
) : HttpRequestContext {
    override suspend fun validate() {
        val constraint = Validation<ConfirmUserRegisterRequest> {
            // validate phone number
            ConfirmUserRegisterRequest::phoneNumber required {
                pattern(Regex("^\\d{10}$")) hint "invalid phone number"
            }
            // validate identity Number
            ConfirmUserRegisterRequest::confirmationCode required {
                pattern(Regex("^\\d{6}$")) hint "invalid confirmation code"
            }
        }
        /**
         * check values that users have entered into form
         * before utilizing the values in the application
         */
        if (constraint(this).errors.isNotEmpty()) {
            throw HttpException(ErrorMessage.CONSTRAINT_VIOLATION)
        }
    }
}
