package user.model

import constant.ErrorMessage
import http.HttpException
import http.HttpRequestContext
import io.konform.validation.Validation
import io.konform.validation.jsonschema.pattern
import user.repository.UserRepository
import utils.PasswordUtil

data class ConfirmForgotPasswordRequest(
    /**
     * The phone number of the user whose registration you wish to confirm.
     * @var [phoneNumber] String
     */
    val phoneNumber: String,
    /**
     * The identity number of the user whose registration you wish to confirm.
     * @var [identityNumber] String
     */
    val identityNumber: String,
    /**
     * The confirmation code sent by a user's request to retrieve a forgotten password.
     * @var [confirmationCode] String
     */
    val confirmationCode: String,

    /**
     * The password sent by a user's request to retrieve a forgotten password.
     */
    val password: String,
) : HttpRequestContext {
    override suspend fun validate() {
        val constraint = Validation<ConfirmForgotPasswordRequest> {
            // validate phone number
            ConfirmForgotPasswordRequest::phoneNumber required {
                pattern(Regex("^\\d{10}$")) hint "invalid phone number"
            }
            // validate identity Number
            ConfirmForgotPasswordRequest::identityNumber required {
                pattern(Regex("^\\d{13}$")) hint "invalid identity number"
            }

            // validate Password
            ConfirmForgotPasswordRequest::password required {
                pattern(PasswordUtil.PASSWORD_REGEX) hint "invalid password"
            }

            // validate identity Number
            ConfirmForgotPasswordRequest::confirmationCode required {
                pattern(Regex("^\\d{6}$")) hint "invalid confirmation code"
            }
        }

        /**
         * check values that users have entered into form
         * before utilizing the values in the application
         */
        if (constraint(this).errors.isNotEmpty()) {
            throw HttpException(ErrorMessage.CONSTRAINT_VIOLATION)
        }

        /**
         * check values that users entered into form
         * does exist before submitting the values to the database
         */
        UserRepository.getUserByCredentials(this.phoneNumber, this.identityNumber)
    }
}
