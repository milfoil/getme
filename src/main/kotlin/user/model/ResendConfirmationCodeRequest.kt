package user.model

import constant.ErrorMessage
import http.HttpException
import http.HttpRequestContext
import io.konform.validation.Validation
import io.konform.validation.jsonschema.pattern
import user.repository.UserRepository

data class ResendConfirmationCodeRequest(
    val phoneNumber: String
) : HttpRequestContext {
    override suspend fun validate() {

        val constraint = Validation<ResendConfirmationCodeRequest> {
            // validate phone number
            ResendConfirmationCodeRequest::phoneNumber required {
                pattern(Regex("^\\d{10}$")) hint "invalid phone number"
            }
        }

        /**
         * check values that users have entered into form
         * before utilizing the values in the application
         */
        if (constraint(this).errors.isNotEmpty()) {
            throw HttpException(ErrorMessage.CONSTRAINT_VIOLATION)
        }

        /**
         * check values that users entered into form
         * does not exist before submitting the values to the database
         */
        UserRepository.getUserByPhoneNumberOrFail(this.phoneNumber)
    }
}
