package user.model

import constant.ErrorMessage
import http.HttpException
import http.HttpRequestContext
import io.konform.validation.Validation
import io.konform.validation.jsonschema.pattern
import user.repository.UserRepository

data class ForgotPasswordRequest(
    /**
     * The id number associated with the user account
     * @var [identityNumber] String
     */
    val identityNumber: String,

    /**
     * The phone number associated with the user account
     * @var [phoneNumber] String
     */
    val phoneNumber: String,
) : HttpRequestContext {
    override suspend fun validate() {

        val constraint = Validation<ForgotPasswordRequest> {
            // validate phone number
            ForgotPasswordRequest::phoneNumber required {
                pattern(Regex("^\\d{10}$")) hint "invalid phone number"
            }

            // validate identity Number
            ForgotPasswordRequest::identityNumber required {
                pattern(Regex("^\\d{13}$")) hint "invalid identity number"
            }
        }

        /**
         * check values that users have entered into form
         * before utilizing the values in the application
         */
        if (constraint(this).errors.isNotEmpty()) {
            throw HttpException(ErrorMessage.CONSTRAINT_VIOLATION)
        }

        /**
         * check values that users entered into form
         * does exist before submitting the values to the database
         */
        UserRepository.getUserByCredentials(this.phoneNumber, this.identityNumber)
    }
}
