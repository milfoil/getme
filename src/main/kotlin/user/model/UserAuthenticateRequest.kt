package user.model

import constant.ErrorMessage
import http.HttpException
import http.HttpRequestContext
import io.konform.validation.Validation
import io.konform.validation.jsonschema.pattern
import utils.PasswordUtil

data class UserAuthenticateRequest(
    val phoneNumber: String,
    val password: String,
) : HttpRequestContext {
    override suspend fun validate() {

        val constraint = Validation<UserAuthenticateRequest> {
            // validate phone number
            UserAuthenticateRequest::phoneNumber required {
                pattern(Regex("^\\d{10}$")) hint "invalid phone number"
            }

            // validate Password
            UserAuthenticateRequest::password required {
                pattern(PasswordUtil.PASSWORD_REGEX) hint "invalid password"
            }
        }

        /**
         * check values that users have entered into form
         * before utilizing the values in the application
         */
        if (constraint(this).errors.isNotEmpty()) {
            throw HttpException(ErrorMessage.CONSTRAINT_VIOLATION)
        }
    }
}
