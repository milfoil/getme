import com.typesafe.config.ConfigFactory
import database.DatabaseMain
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.jwt.jwt
import io.ktor.config.HoconApplicationConfig
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.gson.gson
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.slf4j.event.Level
import utils.JwtUtil

fun main() {
    embeddedServer(Netty, 8081, watchPaths = listOf("getme")) {
        val env = HoconApplicationConfig(ConfigFactory.load())
        val environment = env.config("ktor").property("environment").getString()
        val databaseConfig = env.config("database.$environment")

        // configure database connection
        install(DatabaseMain) {
            username = databaseConfig.property("username").getString()
            password = databaseConfig.property("password").getString()
            host = databaseConfig.property("host").getString()
            port = databaseConfig.property("port").getString().toInt()
            database = databaseConfig.property("database").getString()
            driver = databaseConfig.property("driver").getString()
        }
        // configure logging
        install(CallLogging) {
            level = Level.DEBUG
            // format { call -> call.request.toString() }
        }

        // install double receive feteature into the application call
        // provides the ability to invoke receive several times with no
        // request already consumed exception

        // Configure HTTP Request & Response
        install(DefaultHeaders) {
            header(name = "X-XSS-Protection", "1; mode=block")
            header(name = "Strict-Transport-Security", "max-age=31536000; includeSubDomains; preload")
            header(name = "X-Content-Type-Options", "nosniff")
        }

        // Configure JWT and JWK authentication
        install(Authentication) {
            jwt("auth-jwk") {
                realm = JwtUtil.realm
                verifier(JwtUtil.provider, JwtUtil.issuer)
                validate {

                    try {
                        JwtUtil.getUserProfile(it)
                    } catch (ex: Exception) {
                        throw Error(ex.stackTraceToString())
                    }
                }
            }
        }

        //  Configure JSON content handling
        install(ContentNegotiation) {
            gson {
                setPrettyPrinting()
                serializeNulls()
            }
        }

        // configure routers
        appRouter()
    }.start(wait = true)
}
