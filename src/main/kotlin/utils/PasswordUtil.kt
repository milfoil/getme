package utils

import at.favre.lib.crypto.bcrypt.BCrypt
import kotlin.random.Random

/**
 * The allowed characters for the password.
 *
 * Note that the number 0 and the letter 'O' have been removed to avoid
 * confusion between the two. The same is true of 'I', 1, and 'l'.
 *
 * @var string
 */
const val PASSWORD_ALLOWED_CHARS: String = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789"

// internal val PASSWORD: String = "/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\\^\$*.\\[\\]{}\\(\\)?\\-“!@#%&\\/,><\\’:;|_~`])\\S{8,99}\$"

class PasswordUtil(val password: String) {
    /**
     * The cost (log2 factor) used to create the hash
     */
    private val cost: Int = 12

    /**
     * Hash a password using a secure hash.
     * @return string
     *   A string containing the hashed password, or FALSE on failure.
     */
    fun hash() = BCrypt.withDefaults().hashToString(cost, password.toCharArray())

    /**
     * Check whether a plain text password matches a hashed password.
     *
     * @param string $hash
     *   A hashed password.
     *
     * @return bool
     *   TRUE if the password is valid, FALSE if not.
     */
    fun validity(hash: String) = BCrypt.verifyer().verify(password.toCharArray(), hash).verified

    companion object {
        /**
         * Generates a password.
         *
         * @param length
         *   (optional) The length of the password.
         *
         * @return string
         *   The password.
         */
        fun generate(length: Int): String {
            // The maximum integer we want from random_int().
            val max = PASSWORD_ALLOWED_CHARS.length - 1
            var pass = ""

            for (i in 0..length) {
                pass += PASSWORD_ALLOWED_CHARS[Random.nextInt(0, max)]
            }
            return pass
        }

        /**
         * / Indicates the start of a regular expression
         * ^ Beginning. Matches the beginning of the string.
         * (?=.*[a-z]) Requires lowercase letters
         * (?=.*[A-Z]) Requires uppercase letters
         * (?=.*[0-9]) Requires numbers
         * (?=.*[\^$*.\[\]{}\(\)?\-“!@#%R&><\’:;|_~`]) Requires special characters (only the special characters listed by AWS Cognito).
         * \S Whitespace (space, tab, carriage return) not allowed
         * {8,99} Minimum 8 characters, maximum 99 characters
         * $ End. Matches the end of the string.
         * / Close.
         */
        val PASSWORD_REGEX = Regex("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-|]).{8,}$")
    }
}
