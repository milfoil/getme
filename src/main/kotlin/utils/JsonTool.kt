package utils

import com.google.gson.Gson

object JsonTool {
    private val gson = Gson()
    fun <T> stringify(value: T): String = gson.toJson(value)

    fun <T> toJson(request: String, schema: Class<T> ): T = gson.fromJson(request, schema)
}
