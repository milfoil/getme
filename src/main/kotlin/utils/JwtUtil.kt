package utils

import com.auth0.jwk.JwkProvider
import com.auth0.jwk.JwkProviderBuilder
import com.typesafe.config.ConfigFactory
import io.ktor.auth.jwt.JWTCredential
import io.ktor.config.HoconApplicationConfig
import user.model.UserProfile
import user.repository.UserRepository
import java.util.Date

object JwtUtil {
    private val env = HoconApplicationConfig(ConfigFactory.load()).config("jwt")
    val issuer = env.property("issuer").getString()
    private val audience = env.property("audience").getString()
    val realm = env.property("realm").getString()
    val provider: JwkProvider = JwkProviderBuilder(issuer).build()

    /**
     * retrieve user profile from token
     */
    suspend fun getUserProfile(credential: JWTCredential): UserProfile? {
        try {
            val expired = credential.payload.expiresAt.after(Date())
            val aud = credential.payload.getClaim("client_id").asString()
            val username = credential.payload.getClaim("username").asString()
            return if (expired && audience.contains(aud)) {
                UserRepository.getUserProfileOrFail(username)
            } else {
                null
            }
        } catch (e: Throwable) {
            // https://youtrack.jetbrains.com/issue/KTOR-636
            loggerUtil("Authentication failed.", e)
            throw e
        }
    }
}
