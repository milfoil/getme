package utils

import java.io.File

object FileUtil {
    fun load(name: String): File {
        return File("./src/main/resources/$name")
    }
}
