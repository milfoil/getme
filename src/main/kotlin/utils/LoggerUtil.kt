package utils

import org.slf4j.Logger
import org.slf4j.LoggerFactory

fun loggerUtil(operation: String, data: Any) {
    val logger: Logger = LoggerFactory.getLogger("io.netty")

    logger.info("------------------- $operation ------------------\n")

    logger.info(data.toString())
    logger.info("----------------------- END ---------------------\n")
}
