package utils

import features.payment.model.PaymentAmountRequest
import io.konform.validation.Validation
import io.konform.validation.jsonschema.maxLength
import io.konform.validation.jsonschema.minLength
import io.konform.validation.jsonschema.minimum
import io.konform.validation.jsonschema.pattern

object ValidatorTool {
    val amount = run {
        Validation<PaymentAmountRequest> {
            PaymentAmountRequest::amount required {
                minimum(1.00)
            }
        }
    }
    val currency = run {
        Validation<PaymentAmountRequest> {
            PaymentAmountRequest::currency required {
                maxLength(3)
                pattern("\\p{Alpha}*")
            }
        }
    }

    /**
     * This parameter contains the three-letter "ISO 3166-1 alpha-3"
     */
    val lettersOnly = Validation<String> {
        maxLength(3)
        pattern("\\p{Alpha}*")
    }

    val alphanumeric = Validation<String> {
        minLength(1)
        maxLength(225)
        pattern("^[a-zA-Z0-9]+\$")
    }
}
