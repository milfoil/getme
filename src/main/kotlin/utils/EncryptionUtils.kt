package utils

import com.nimbusds.jose.EncryptionMethod
import com.nimbusds.jose.JOSEException
import com.nimbusds.jose.JWEAlgorithm
import com.nimbusds.jose.JWEHeader
import com.nimbusds.jose.JWEObject
import com.nimbusds.jose.Payload
import com.nimbusds.jose.crypto.RSADecrypter
import com.nimbusds.jose.crypto.RSAEncrypter
import http.HttpException
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.security.KeyFactory
import java.security.NoSuchAlgorithmException
import java.security.NoSuchProviderException
import java.security.PrivateKey
import java.security.PublicKey
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.security.interfaces.RSAPublicKey
import java.security.spec.InvalidKeySpecException
import java.security.spec.PKCS8EncodedKeySpec
import java.text.ParseException

class EncryptionUtils {
    private val ALG = JWEAlgorithm.RSA_OAEP_256
    private val ENC_MTHD = EncryptionMethod.A256GCM

    private val requestContentType = "application/json"
    fun jweEncrypt(
        plainData: String,
        crtFileName: File,
        keyFingerPrint: String?
    ): String? {
        return try {
            val rsaPublicKey = getPublicKeyFromCrt(crtFileName) as RSAPublicKey
            encryptWithPublicKey(plainData, rsaPublicKey, keyFingerPrint)
        } catch (je: JOSEException) {
            throw HttpException(je.message)
        }
    }

    fun jweDecrypt(cipher: String?, privateKeyFile: File): String? {
        return try {
            // Decrypt JWE with CEK directly, with the DirectDecrypter in promiscuous mode
            val jwe = JWEObject.parse(cipher)
            jwe.decrypt(RSADecrypter(getPrivateKeyFromPKCS8KeyFile(privateKeyFile)))
            jwe.payload.toString()
        } catch (e: ParseException) {
            throw HttpException(e.message)
        } catch (e: JOSEException) {
            throw HttpException(e.message)
        }
    }

    private fun encryptWithPublicKey(
        plainData: String,
        rsaPublicKey: RSAPublicKey?,
        keyFingerPrint: String?,
    ): String? {
        // Encrypt the JWE with the RSA public key + specified AES CEK
        val builder = JWEHeader.Builder(ALG, ENC_MTHD)
        if (keyFingerPrint != null) {
            builder.keyID(keyFingerPrint)
        }
        if (requestContentType != null) {
            builder.contentType(requestContentType)
        }
        val jweHeader = builder.build()
        val jwe = JWEObject(
            jweHeader,
            Payload(plainData)
        )
        val encrypter = RSAEncrypter(rsaPublicKey)
        jwe.encrypt(encrypter)
        return jwe.serialize()
    }

    private fun getPrivateKeyFromPKCS8KeyFile(keyPath: File): PrivateKey? {
        return try {
            val `is`: InputStream = keyPath.inputStream()
            val kf = KeyFactory.getInstance("RSA", "SunRsaSign")
            val kspec = PKCS8EncodedKeySpec(`is`.readBytes())
            kf.generatePrivate(kspec)
        } catch (ke: IOException) {
            throw HttpException(ke.message)
        } catch (ke: InvalidKeySpecException) {
            throw HttpException(ke.message)
        } catch (ke: NoSuchAlgorithmException) {
            throw HttpException(ke.message)
        } catch (ke: NoSuchProviderException) {
            throw HttpException(ke.message)
        }
    }

    private fun getPublicKeyFromCrt(filePath: File): PublicKey? {
        return try {
            val `is`: InputStream = filePath.inputStream()
            val factory = CertificateFactory.getInstance("X.509", "SUN") // "X.509"
            factory.generateCertificate(`is`).publicKey
        } catch (ke: IOException) {
            throw HttpException(ke.message)
        } catch (ke: CertificateException) {
            throw HttpException(ke.message)
        } catch (ke: NoSuchProviderException) {
            throw HttpException(ke.message)
        }
    }
}
