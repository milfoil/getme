package http

interface HttpRequestContext {
    /**
     *
     * validate of request payload.
     *
     * @throws ConstraintViolationException
     */
    suspend fun validate(): Unit
}
