package http

import io.ktor.http.HttpStatusCode

open class HttpException : Exception {
    var statusCode: HttpStatusCode = HttpStatusCode.InternalServerError
        protected set

    constructor(message: String?) : super(message)
    constructor(message: String?, cause: Throwable?) : super(message, cause)
    constructor(type: String, httpMethod: String, message: String, cause: Throwable) : super()
}

class ConstraintViolationException(message: String) : Exception(message)
