package http

import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.features.json.GsonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.logging.DEFAULT
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logger
import io.ktor.client.features.logging.Logging

val httpClient: HttpClient = HttpClient(CIO) {
    //  Configure JSON content handling
    install(JsonFeature) {
        serializer = GsonSerializer {
            serializeNulls()
            setPrettyPrinting()
            disableHtmlEscaping()
        }
    }
    // configure logging
    install(Logging) {
        logger = Logger.DEFAULT
        level = LogLevel.ALL
    }
}

// val httpWebSocketClient: HttpClient = HttpClient(CIO) {
//     install(WebSockets)
//     //  Configure JSON content handling
//     install(JsonFeature) {
//         serializer = GsonSerializer {
//             serializeNulls()
//             setPrettyPrinting()
//             disableHtmlEscaping()
//         }
//     }
//     // configure logging
//     install(Logging) {
//         logger = Logger.DEFAULT
//         level = LogLevel.BODY
//     }
// }
