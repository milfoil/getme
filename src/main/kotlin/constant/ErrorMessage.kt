package constant

object ErrorMessage {
    const val DEFAULT = "We are unable to process your request at the moment."
    const val ACCESS_DENIED_EXCEPTION = "You do not have sufficient access to perform this action."
    const val INCOMPLETE_SIGNATURE = "The request signature does not conform to AWS standards."
    const val INTERNAL_SERVER_ERROR = "The request processing has failed because of an unknown error."
    const val INVALID_ACTION =
        "The action or operation requested is invalid. Verify that the action is typed correctly."
    const val INVALID_PARAMETER =
        "The action or operation requested is invalid. Verify that the action is typed correctly."
    const val invalidClientTokenId = "The access key ID provided does not exist in our records."
    const val INVALID_PARAMETER_COMBINATION = "Parameters that must not be used together were used together."
    const val INVALID_PARAMETER_VALUE = "An invalid or out-of-range value was supplied. " +
        "Verify that the value is typed correctly"
    const val INVALID_QUERY_PARAMETER = "The query string is malformed or does not adhere to our standards."
    const val MALFORMED_QUERY_STRING = "The query string contains an invalid value."
    const val MISSING_ACTION = "The request is missing an action or a required parameter."
    const val MISSING_AUTHENTICATION_TOKEN = "The request is missing an action or a required parameter."
    const val MISSING_PARAMETER = "A required parameter for the specified action is not supplied."
    const val NOT_AUTHORIZED = "You do not have permission to perform this action."
    const val OPT_IN_REQUIRED = "The AWS access key ID needs a subscription for the service."
    const val REQUEST_EXPIRED = "The request reached the service after the expiration date stamp."
    const val SERVICE_UNAVAILABLE = "The request has failed due to a temporary failure of the server."
    const val THROTTLING = "The request was denied due to request throttling."
    const val CONSTRAINT_VIOLATION = "The input fails to satisfy the constraints specified by the service."
    const val VALIDATION_ERROR = "The input fails to satisfy the constraints specified by the service."
    const val RESOURCE_NOT_FOUND = "We cannot find the requested resource."
    const val TOO_MANY_REQUEST = "The service has made too many requests for a given request."
    const val TOO_MANY_FAILED_ATTEMPT = "The service has made too many failed attempts for a given action."
    const val UNEXPECTED_EXCEPTION = "The request encountered an unexpected exception with the external service."
    const val LIMIT_EXCEEDED = "The request has reached it limit for a given service."
}
