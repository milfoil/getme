package database

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.ApplicationFeature
import io.ktor.util.AttributeKey
import org.jetbrains.exposed.sql.Database

/**
 *  represents an engine for running a database connection.
 *  The DBEngineMain function is used to start a a database connection with the selected engine
 *  and loads the database module specified in the external application.conf file
 */

class DatabaseMain(configuration: Configuration) {
    /**
     * copies a snapshot of the mutable config into an immutable property
     */
    private val username: String = configuration.username
    private val password: String = configuration.password
    private val host: String = configuration.host
    private val port: Int = configuration.port
    private val database: String = configuration.database
    private val tablePrefix: String? = configuration.tablePrefix
    private val driver: String = configuration.driver

    class Configuration : DatabaseConfiguration {
        override var username = ""
        override var password = ""
        override var host = ""
        override var port = 0
        override var database = ""
        override val tablePrefix: String? = null
        override var driver = ""
    }

    companion object Feature : ApplicationFeature<ApplicationCallPipeline, Configuration, DatabaseMain> {
        /**
         * a unique key for the feature
         */
        override val key = AttributeKey<DatabaseMain>("DatabaseEngineFeature")

        /**
         * execute when installing the feature
         */
        override fun install(
            pipeline: ApplicationCallPipeline,
            configure: Configuration.() -> Unit
        ): DatabaseMain {
            val config = Configuration().apply(configure)
            val engineMain = DatabaseMain(config)

            /**
             *  build a connection pool
             */
            val dataSource = HikariDataSource(
                HikariConfig().apply {
                    jdbcUrl = "jdbc:${config.driver}://${config.host}:${config.port}/${config.database}"
                    username = config.username
                    password = config.password
                    validate()
                }
            )
            /**
             * create an instance of database connections
             */
            Database.connect(dataSource)
            /**
             * run migration instance of database connections
             */
            DatabaseMigrationMain(
                location = "migrations",
                commands = setOf(InfoCommand, MigrateCommand),
                dataSource = dataSource
            )
            return engineMain
        }
    }
}
