package database

import org.flywaydb.core.Flyway

abstract class DatabaseCommandMain {
    abstract fun run(flyway: Flyway)
}

object BaselineCommand : DatabaseCommandMain() {
    override fun run(flyway: Flyway) {
        flyway.baseline()
    }
}

object CleanCommand : DatabaseCommandMain() {
    override fun run(flyway: Flyway) {
        flyway.clean()
    }
}

object InfoCommand : DatabaseCommandMain() {
    override fun run(flyway: Flyway) {
        flyway.info()
    }
}

object MigrateCommand : DatabaseCommandMain() {
    override fun run(flyway: Flyway) {
        flyway.migrate()
    }
}

object RepairCommand : DatabaseCommandMain() {
    override fun run(flyway: Flyway) {
        flyway.repair()
    }
}

object ValidateCommand : DatabaseCommandMain() {
    override fun run(flyway: Flyway) {
        flyway.validate()
    }
}
