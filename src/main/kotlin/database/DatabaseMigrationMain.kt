package database

import com.zaxxer.hikari.HikariDataSource
import org.flywaydb.core.Flyway
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class DatabaseMigrationMain(
    val location: String,
    val dataSource: HikariDataSource,
    var commands: Set<DatabaseCommandMain>
) {
    private val logger: Logger = LoggerFactory.getLogger("org.flywaydb.core")

    // configure
    private val migration = Flyway
        .configure()
        .dataSource(dataSource)
        .locations(location)
        .load()

    private fun commands(vararg commandsToExecute: DatabaseCommandMain) {
        commands = commandsToExecute.toSet()
    }

    init {
        logger.info("Database migration has started")
        commands.map { command ->
            logger.info("Running command: ${command.javaClass.simpleName}")
            command.run(migration)
        }
        logger.info("Database migration has finished")
    }
}
