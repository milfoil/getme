package database

interface DatabaseConfiguration {
    /**
     * The access id of the connected database.
     *
     * @var string
     */
    val username: String

    /**
     * The access key of the connected database.
     *
     * @var string
     */
    val password: String

    /**
     * The location of the connected database.
     *
     * @var integer
     */
    val host: String

    /**
     * The port of the connected database.
     *
     * @var integer
     */
    val port: Int

    /**
     * The name of the connected database.
     *
     * @var string
     */
    val database: String

    /**
     * The table prefix for the connection.
     *
     * @var string
     */
    val tablePrefix: String?

    /**
     * The driver of the connected database.
     *
     * @var string
     */
    val driver: String
}
