package integration.mastercard

/**
 * Mastercard uses one-legged OAuth 1.0a for authenticating and authorizing client applications.
 * It means every request sent to us must be digitally signed,
 * and only requests with valid signatures created by authorized clients are granted access to our services.
 */
class MastercardAuthenticationAPI : MastercardConfiguration() {

    /**
     * The Consumer Key
     * Consumer keys are 97-character identifiers for client applications consuming Mastercard APIs.
     * The consumer key is not a secret (unlike the signing key), but rather a kind of username which appears in the OAuth header value sent with each request
     */
    val consumerKey: String = "C7VBN5JefLghqjQ6U4qOEyvWfuRvcc0hFavhN1N426089275!e5ba19cf261847529ef9094aa5a411cc0000000000000000"

    /**
     * The Signing Key
     * The signing key is a 2048-bit private RSA key used to generate request signatures and confirms ownership of the consumer key.
     *  This private key is part of a public/private key pair and certified by Mastercard.
     */
    val signingKey = ""
    val signatureMethod: String = "RSA-SHA256"
    // val signatureMethod: String = "RSA-SHA256"
    /**
     * In addition to that, requests with a body must be signed using the Google Request Body Hash extension for OAuth.
     */

    /**
     * Signed Requests
     * The OAuth signature process:
     * 1. Normalizes requests (URL, body, …) and OAuth parameters into a single string to sign
     * 2.
     */

    /**
     * The Authorization Header
     */

    /**
     * Enable Encryption
     */

    /**
     * Encryption Scheme
     * Sensitive data sent in requests are encrypted by your application for Mastercard,
     * and sensitive data returned in responses are encrypted by Mastercard for your application.
     *
     */

    /**
     * The Encryption Keys
     * Data encrypted using a public key can only be decrypted using the corresponding private key.
     * This is the symmetric AES session key encrypted with the recipient’s RSA public key.
     */
    val encryptedKey: String = ""

    /**
     * Client Encryption Keys
     * The public key used to encrypt requests Client Encryption Keys
     *
     */
    val clientEncryptionKey = ""

    /**
     * Mastercard Encryption Key
     * The public key Mastercard uses to encrypt responses
     *
     */
    val mastercardEncryptionKey = ""

    /**
     * The Encryption Process
     * 1. An AES session key is generated along with some encryption parameters
     * 2. Sensitive data are encrypted using the AES key
     * 3. The AES key is encrypted using the recipient’s RSA public key
     * 4. The payload is sent with the encrypted session key and parameters
     */
    val encryptedPayload: String = ""

    /**
     * The Decryption Process
     * Here are the steps for decrypting a payload:
     * 1. The AES session key is decrypted using the recipient’s RSA private key
     * 2. Sensitive data are decrypted using the AES key
     */
}

// 1. encrypts request payload
// 2. sends request
// 3. decrypts request payload
// 4. encrypts response payload
// 5. send response
// 6. decrypts response
