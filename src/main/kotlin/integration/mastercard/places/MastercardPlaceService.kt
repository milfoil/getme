package integration.mastercard.places

import http.HttpException
import http.httpClient
import integration.mastercard.MastercardConfiguration
import integration.mastercard.places.model.MerchantCategoryCodeResponse
import integration.mastercard.places.model.MerchantIndustryResponse
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.http.ContentType
import io.ktor.http.contentType

/**
 * Merchant Category Codes
 * This API will return detailed information on merchant locations based on transactional data seen by MasterCard.
 */
class MastercardPlaceService : MastercardConfiguration() {
    private val baseURL = "$host/merchantpoi/v1/merchantpoi"
    /**
     * Returns a list of Merchant Category Codes
     * The list will be ordered by Merchant Category Name by default.
     * @param code - Orders this list via the MCC Codes (merchant category codes) instead of by merchant category name
     *               Boolean
     * @return [MerchantCategoryCodeResponse]
     */
    suspend fun getMerchantCategoryCodes(code: Boolean = false): MerchantCategoryCodeResponse = runCatching {
        val serviceURL = "$baseURL/merchantcategorycodes?MCC_Codes=$code"
        val token = getAccessToken(serviceURL, "GET", "")
        println(token)
        httpClient.get<MerchantCategoryCodeResponse>(serviceURL) {
            contentType(ContentType.Application.Json)
            header("Authorization", token)
            body = ""
        }
    }.getOrElse {
        throw HttpException(it.message, it.cause)
    }

    /**
     * Merchant Industries
     * Returns a list of Merchant Industries
     * The list will be ordered by Merchant Industry Name by default.
     * @param code - Orders this list via the Industry Codes instead of by industry name
     *               Boolean
     */
    suspend fun getMerchantIndustries(code: Boolean = false): MerchantIndustryResponse = runCatching {
        val serviceURL = "$baseURL/merchantindustries?IND_Codes=$code"
        val token = getAccessToken(serviceURL, "GET", "")
        httpClient.get<MerchantIndustryResponse>(serviceURL) {
            contentType(ContentType.Application.Json)
            header("Authorization", token)
            body = ""
        }
    }.getOrElse {
        throw HttpException(it.message, it.cause)
    }
}
