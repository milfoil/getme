package integration.mastercard.places.model

/**
 * An array of Merchant Category Codes and Merchant Category Names.
 */
data class MerchantCategoryCodeResponse(
    val MerchantCategoryCodeList: MerchantCategoryCodeList
)

data class MerchantCategoryCodeList(
    val MerchantCategoryCodeArray: MerchantCategoryCodeArray
)
data class MerchantCategoryCodeArray(
    val MerchantCategoryCode: List<MerchantCategoryCode>
)

data class MerchantCategoryCode(
    /**
     * This code represents the category that a merchant location may be under and is supported by most payment providers.
     * @var [MerchantCatCode] String
     */
    val MerchantCatCode: String,
    /**
     * This is the name of the Merchant Category that accompanies the MCC Code which identifies the category a merchant falls under.\
     * @var [MerchantCategoryName] String
     */
    val MerchantCategoryName: String
)
