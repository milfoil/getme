package integration.mastercard.places.model

/**
 * A list of all the Merchant Industry Codes and Merchant Industry Names
 */
data class MerchantIndustryResponse(
    val MerchantIndustryList: MerchantIndustryList
)

data class MerchantIndustryList(
    val MerchantIndustryArray: MerchantIndustryArray
)

data class MerchantIndustryArray(
    val MerchantIndustry: List<MerchantIndustry>
)

data class MerchantIndustry(
    /**
     * This code represents the industry that a merchant location may be under and is supported by most payments provide
     * @var [Industry] string
     */
    val Industry: String,
    /**
     * This is the name of the Industry that accompanies the Industry Code which identifies the industry a merchant falls under.
     * @var [Industry] string
     */
    val IndustryName: String
)
