package integration.mastercard

import com.mastercard.developer.encryption.JweConfig
import com.mastercard.developer.encryption.JweConfigBuilder
import com.mastercard.developer.encryption.JweEncryption
import com.mastercard.developer.utils.EncryptionUtils
import utils.FileUtil
import java.nio.charset.Charset
import java.nio.file.Files
import java.security.PrivateKey


/**
 * Mastercard APIs are secured through an authentication process where only authorized clients, with a valid digital signature are granted access. Payload encryption provides an additional layer of security.
 * When the API payloads are encrypted, a new data structure forming the JSON Web Encryption(JWE) payload is created.
 */
open class MastercardPayloadEncryption : MastercardConfiguration() {

    var key = String(Files.readAllBytes(FileUtil.load("/certs/$certificateFile").toPath()), Charset.defaultCharset())

    // use the Client Encryption Key to encrypt the data you send to Mastercard
    /**
     * Loading the Encryption Certificate
     */
    private val encryptionCertificate =
    EncryptionUtils.loadEncryptionCertificate(FileUtil.load("/certs/$certificateFile").absolutePath)

    /**
     * Loading the Decryption Key
     */
    // use the Mastercard Encryption Key to encrypt the data sent to you by Mastercard.
    private val decryptionKey: PrivateKey = EncryptionUtils.loadDecryptionKey(
            FileUtil.load("/certs/$decryptionKeyFile").absolutePath,
            "MastercardEncryptionKey",
            "Moroa@404M"
        )

    /**
     *  Responsible for payload encryption
     */

    fun encryptedRequestPayload(payload: String): String = JweEncryption.encryptPayload(payload, jweConfig)

    // JweEncryption.encryptPayload(payload, jweConfig)

    /**
     * Responsible for payload decryption
     */
    fun decryptResponsePayload(response: String): String = JweEncryption.decryptPayload(response, jweConfig)

    /**
     * Configuring the Field Level Encryption
     */

    private val jweConfig: JweConfig = JweConfigBuilder.aJweEncryptionConfig()
        .withEncryptionCertificate(encryptionCertificate)
        .withDecryptionKey(decryptionKey)
        .withEncryptionPath("$", "$.encrypted_payload")
        .withDecryptionPath("$.encrypted_payload.data", "$")
        .withEncryptedValueFieldName("data")
        .build()
}
