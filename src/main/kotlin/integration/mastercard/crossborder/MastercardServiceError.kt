package integration.mastercard.crossborder


data class MastercardServiceErrorWrapper(
    val  Errors: MastercardServiceErrors
)

data class MastercardServiceErrors(
    val  Error: MastercardServiceError
)

data class MastercardServiceError(
    /**
     *  A unique identifier for an API web service request.
     */
    val RequestId: String,
    /**
     *   A unique identifier that attempts to define the field in error when available.
     *   In case of an error, when source =“Gateway” and the http status code = 4xx, transaction can be resubmitted using the same transaction reference Id.
     */
    val Source: String,
    /**
     *  Identifies the reason for the error.
     */
    val ReasonCode: String,
    /**
     *  Describes the reason for the problem.
     */
    val Description: String,
    /**
     *  A true/false response, intended for future use.
     */
    val Recoverable: String,
    /**
     *   A name/value pair for the error detail.
     */
    val Details: MastercardServiceErrorDetailWrapper,
)

data class MastercardServiceErrorDetailWrapper(
    val Detail: MastercardServiceErrorDetail,
)

data class MastercardServiceErrorDetail(
    val Name: String,
    val Value: String,
)
