package integration.mastercard.crossborder.payment

import integration.mastercard.crossborder.model.MastercardAdditionalDataWrapper

data class MastercardCrossBorderCancelPaymentResponse(
    /**
     * Status of the cancel request for payment.
     * Details- string, 1-30
     */
    val status: String,

    /**
     * This parameter contains the list of name/value pairs containing additional parameter values.
     * It is a conditional parameter where there can be zero to many fields sent in the payload.
     * The number of fields, and when they are required, is based on the specific RSP and transaction type
     */
    val additional_data_list: MastercardAdditionalDataWrapper?,

    )
