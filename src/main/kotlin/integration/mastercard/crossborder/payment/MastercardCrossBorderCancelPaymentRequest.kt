package integration.mastercard.crossborder.payment

import integration.mastercard.crossborder.model.MastercardAdditionalDataWrapper


data class MastercardCrossBorderCancelPaymentRequestWrapper(
    /**
     * Contains the details of the request message
     */
    val cancelpaymentrequest: MastercardCrossBorderCancelPaymentRequest
)

data class MastercardCrossBorderCancelPaymentRequest(
    /**
     * This parameter contains the list of name/value pairs containing additional parameter values.
     * This parameter is conditional, and only required for specific receiving service provider endpoints identified as part of corridor data details.
     * If this field is needed, then necessary names and values will be supplied during onboarding.
     * There can be zero to many fields sent in the payload.
     */
    val additional_data: MastercardAdditionalDataWrapper? = null
)
