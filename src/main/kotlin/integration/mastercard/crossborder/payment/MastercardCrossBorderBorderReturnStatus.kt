package integration.mastercard.crossborder.payment

data class MastercardCrossBorderBorderReturnStatus(
    /**
     * This field will only be provided when a transaction is returned to OI.
     * The Field contains English representation of why the transaction is being returned.
     * One of the values below can be provided:
     * Returned Per Sending service provider's request.
     * Beneficiary account is not valid or unable to locate account.
     * Beneficiary account is inactive.
     * Beneficiary name does not match account.
     * Invalid account type.
     * Credit refused by beneficiary.
     * Unspecified reason.
     */
    val Message: String,
)
