package integration.mastercard.crossborder.payment

data class MastercardCrossBorderRateResponse(
    val rates: MastercardCrossBorderRateWrapper
)
