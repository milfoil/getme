package integration.mastercard.crossborder.payment

data class MastercardCrossBoardPaymentAmount(
    /**
     * This parameter contains the amount of the payment.
     * It holds a decimal number with a maximum length of 18 and a minimum length of 1..
     * example: "192.64"
     * @avar [amount]
     */
    val amount: String,
    /**
     * This parameter contains the three-letter "ISO 4217" currency code representing the currency of the amount.
     * It holds a string of alphabet characters with an exact length of three.
     * example: "USD"
     */
    val currency: String

)

