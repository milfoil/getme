package integration.mastercard.crossborder.payment

import integration.mastercard.crossborder.model.MastercardAdditionalDataWrapper
import integration.mastercard.crossborder.model.MastercardCrossBoardRecipient
import integration.mastercard.crossborder.model.MastercardCrossBoardSender
import integration.mastercard.crossborder.quote.MastercardCrossBoardFxType

data class MastercardCrossBoardPaymentRequestWrapper(
    /**
     *  Contains the details of the request message.
     */
    val paymentrequest: MastercardCrossBoardPaymentRequest
)

data class MastercardCrossBoardPaymentRequest(
    /**
     *  This parameter contains the client-provided unique reference number of the transaction.
     *  Note that when utilizing a quote, this parameter's value must be identical to the value created for the associated quote.
     *  It holds a string of alphanumeric special characters with a maximum length of 40 and a minimum length of 1.
     */
    val transaction_reference: String,
    /**
     * This parameter contains the system generated ID of the proposal to execute for this payment.
     * This field is returned in the Quote service.
     * Note: For these payments, the following fields filled in the quote do not need to be provided again the payment;
     * but if they are, they will process without error as long as the data matches what was sent in the Quote that generated the proposal ID.
     * An exception is for the bank_code field. If bank_code was provided in the the quote and provided again in the payment, then the newly provided value will be used.
     * If using a proposal_id and the bank_code was provided in the quote, then that value will be automatically copied into the payment.
     * It is a conditional parameter that holds a string of alphanumeric special characters with a maximum length of 30 and a minimum length of 0.
     */
    val proposal_id: String? = null,
    /**
     * This parameter contains the local date for the location where the payment is originating.
     * Note that it is required when the recipient_account_uri = pan.
     * It is a conditional parameter that holds a numeric value of the format MMDDHHMMSS.
     */
    val local_date_time: String? = null,
    /**
     * This parameter contains the URI identifying the sender's account to fund the payment.
     * Note that you would not provide this if proposal_id was provided because the data will be based on the quote provided.
     * If not utilizing proposal_id, this field is required at the time of the payment request.
     * It is a conditional parameter that holds alphanumeric special characters with a maximum length of 200 and a minimum length of 1.
     * Note: Where a bank account is the funding source, the iban or ban scheme must be provided.
     *
     */
    val sender_account_uri: String? = null,
    /**
     * This parameter contains the URI identifying the beneficiary's account to receive the payment.
     * Note that you would not provide this if proposal_id was provided because the data will be based on the quote provided.
     * If not utilizing proposal_id , this field is required at the time of the payment request.
     * When using the "pan" scheme, the "exp" property is optional.
     * It is a conditional parameter that holds alphanumeric special characters with a maximum length of 200 and a minimum length of 1.
     *
     */
    val recipient_account_uri: String? = null,
    /**
     * Amount of the payment.
     */
    val payment_amount: MastercardCrossBoardPaymentAmount? = null,
    /**
     * This parameter contains the three-letter "ISO 3166-1 alpha-3" country code representing the country in which the payment is being initiated.
     * Note that you would not provide this if proposal_id was provided because the data will be based on the quote provided.
     * If not utilizing proposal_id, this field is required at the time of the payment request.
     * It holds a string of alphabet characters with an exact length of three.
     */
    val payment_origination_country: String? = null,
    /**
     * Either a forward fx_type or a reverse fx_type should be submitted, but not both.
     * All fx_type parameters are optional (forward fees included = true when fx_type not provided).
     * Forward fx_type should not be used with the Customer Managed Sender Pricing model.
     * Note that you would not provide these fields if proposal_id was provided because the data will be based on the quote provided
     */
    val fx_type: MastercardCrossBoardFxType? = null,
    /**
     * Name of the Bank holding the receiving account.
     * This is required only for specific bank service provider end points identified as part of corridor data details.
     * It is a conditional parameter that contains a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.
     */
    val receiving_bank_name: String? = null,
    /**
     * Name of the Bank Branch holding the receiving account.
     * This is required only for specific bank service provider end points identified as part of corridor data details.
     * It is a conditional parameter that contains a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.
     */
    val receiving_bank_branch_name: String? = null,
    /**
     * This parameter contains a bank code associated with the bank name and bank identification code (BIC) provided by the customer.
     * It is required only for specific bank service provider endpoints identified as part of corridor data details.
     * Note that if you provided this in the quote and provide again here, then the newly provided value will be used.
     * If using a proposal_id and the bank_code was provided in the quote, then that value will be automatically copied here for you.
     * It is a conditional parameter that holds a string of alphanumeric special characters with a maximum length of 225 and a minimum length of 1.
     */
    val bank_code: String? = null,
    /**
     * This parameter contains a three-digit code for the type of transaction that is being submitted.
     *  Note that you would not provide this if proposal_id was provided because the data will be based on the quote provided.
     *  If not utilizing proposal_id, this field is required at the time of the payment request.
     *  Available types and their uses are:
     *  B2P: Business Disbursement to Person - A disbursement of funds from a business to an individual account.
     *  B2B: Business to Business- A transfer of funds from one business to another.
     *  G2P: Government to Person - A disbursement of funds from a government agency to a private individual person's account
     *  P2P: Person to Person - A transfer of funds from one private individual person's account to another private individual person's account.
     *  P2B: Person to Business - A payment by an individual person to a business.
     *  It is a conditional parameter that holds a string of alphabet characters with an exact length of three.
     */
    val payment_type: String? = null,
    /**
     * Sender's identification of the source of the funds being submitted.
     * Required only for specific receiving service provider end points identified as part of corridor data details.
     * Valid values will be provided during onboarding.
     * It is a conditional parameter that contains a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 1.
     */
    val source_of_income: String? = null,
    /**
     * Sender name and address information is required for a payment transfer.
     */
    val sender: MastercardCrossBoardSender,
    /**
     *  Recipient name and address information.
     */
    val recipient: MastercardCrossBoardRecipient,
    /**
     * This parameter contains the purpose of payment.
     * Note that it is required for specific receiving service provider end points identified as part of corridor data details.
     * Valid Values: Family Maintenance, Household Maintenance, Donation or Gifts, Payment of Loan, Purchase of Property, Funeral Expenses, Medical Expenses, Wedding Expenses, Payment of bills, Education, Savings, Employee Colleague, Business/Investment, Salary, Payment of goods and services *Remark: *
     * This can be free form text if one of the above valid values are not applicable.
     * It is a conditional parameter that holds a string of alphanumeric special characters with a maximum length of 39 and a minimum length of 1.
     */
    val purpose_of_payment: String? = null,
    /**
     * This parameter identifies the payment file in which this specific payment was submitted by the originator
     * Note that it is required if the payment was generated via a batch payment file. It will be blank if wasn't generated via a batch payment file.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 1.
     */
    val payment_file_identifier: String? = null,
    /**
     * This Parameter contains the System generated Rate ID associated with a currency pair that will be used in a transaction.
     * This ID will be included by the OI in the Payment request and provided to the OI in the Rate File / API for those customers configured to receive this rate information.
     * Note this value must not be provided if a proposal_id is being used in the payment because the rate applied to the payment will be based on the quote provided.
     * It is an optional parameter that holds a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 1.
     */
    val card_rate_id: String? = null,
    /**
     * This parameter contains the list of name/value pairs containing additional parameter values
     * This parameter is conditional, and only required for specific receiving service provider endpoints identified as part of corridor data details
     * If this field is needed, then necessary names and values will be supplied during onboarding.
     * There can be zero to many fields sent in the payload.
     */
    val additional_data: MastercardAdditionalDataWrapper? = null,
)
