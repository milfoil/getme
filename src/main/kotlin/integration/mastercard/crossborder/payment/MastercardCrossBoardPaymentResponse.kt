package integration.mastercard.crossborder.payment

import integration.mastercard.crossborder.model.MastercardAdditionalDataWrapper
import integration.mastercard.crossborder.quote.MastercardCrossBoardFxType
import integration.mastercard.crossborder.quote.MastercardCrossBoardQuoteForward

data class MastercardCrossBoardPaymentResponseWrapper(
    val payment: MastercardCrossBoardForwardPaymentResponse,
)

data class MastercardCrossBoardForwardPaymentResponse(
    /**
     *   This parameter contains the client provided unique reference number.
     *   It holds a string of alphanumeric special characters with a maximum length of 40 and a minimum length of 1.
     */
    val transaction_reference: String,

    /**
     * This parameter contains the system generated unique payment identifier.
     * It holds a string of alphanumeric special characters with a maximum length of 31 and a minimum length of 0.
     */
    val id: String,

    /**
     * The resource type of the payment. This will always be set to the value of "payment"
     */
    val resource_type: String,

    /**
     *  Date and time the original payment was created.
     *  It holds an "ISO 8601" timestamp of the format "YYYY-MM-DDTHH:MM:SS±hh[:mm]"
     */
    val created: String,

    /**
     *  This parameter specifies the proposal that was executed for this payment.
     *  It holds a string of alphanumeric special characters with a maximum length of 30 and a minimum length of 0.
     *
     */
    val proposal_id: String,

    /**
     * This parameter contains the status of the payment. The value will be one of the following: SUCCESS, PENDING, REJECTED.
     */
    val status: String,

    /**
     * Timestamp of when the status was changed to its current value.
     * It holds an "ISO 8601" timestamp of the format "YYYY-MM-DDTHH:MM:SS±hh[:mm]"
     */
    val status_timestamp: String,
    /**
     * This parameter contains an identifier showing the current transaction's pending stage. Note that this parameter is only returned if the status is PENDING. Refer to the "Pending Stages section in Payment API" for valid values.
     * It is conditional and holds a string of alphabet characters with a maximum length of 30 and a minimum length of 1.
     */
    val pending_stage: String? = null,
    /**
     * This value is used for Mastercard Send internal processes only and it SHOULD NOT be used as an indicator of funds delivery into the beneficiary account or passed to the sending customers by an OI.
     *  It is conditional and only returned if the status is PENDING.
     * It holds an "ISO 8601" timestamp of the format. For JSON: YYYY-MMDDTHH:MM:SS±hh[:mm] , For XML: YYYY-MMDDTHH:MM:SS.SSS±hh[:mm]
     */
    val pending_max_completion_date: String? = null,

    /**
     * Contains the fees amount for the transaction (actual or proposed), in sender's currency.
     */
    val fees_amount: MastercardCrossBoardPaymentAmount,
    /**
     * The amount to be charged to the sender.
     */
    val charged_amount: MastercardCrossBoardPaymentAmount,
    /**
     * The amount credited on the beneficiary's account.
     */
    val credited_amount: MastercardCrossBoardPaymentAmount,
    /**
     * The amount that was used to calculate fees.
     */
    val principal_amount: MastercardCrossBoardPaymentAmount,

    /**
     * This parameter contains the URI identifying sender's account to fund the payment.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 200 and a minimum length of 1. Refer to the "Sender Account URI" section on "Additional Resources" page for details about scheme specific data.
     */
    val sender_account_uri: String? = null,

    /**
     * This parameter contains the URI identifying beneficiary's account to receive the payment.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 200 and a minimum length of 1. Refer to the "Recipient Account URI" section on "Additional Resources" page for details about scheme specific data.
     */
    val recipient_account_uri: String? = null,

    /**
     * Amount of the payment.
     */
    val payment_amount: MastercardCrossBoardPaymentAmount,

    /**
     * This parameter contains the three-letter "ISO 3166-1 alpha-3" country code representing the country in which the payment is being initiated.
     * It holds a string of alphabet characters with an exact length of three.

     */
    val payment_origination_country: String,

    /**
     * Either a forward fx_type or a reverse fx_type should be submitted, but not both. All fx_type parameters are optional (forward fees included = true when fx_type not provided). Forward fx_type should not be used with the Customer Managed Sender Pricing model.
     * Note that you would not provide these fields if proposal_id was provided because the data will be based on the quote provided.
     */
    val fx_type: MastercardCrossBoardQuoteForward,

    /**
     * This parameter contains the name of the bank holding the receiving account.Note that it is provided only for specific bank service provider end points identified as part of corridor data details.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.
     */
    val receiving_bank_name: String? = null,

    /**
     * This parameter contains the name of the bank branch holding the receiving account.
     * Note that it is provided only for specific bank service provider end points identified as part of corridor data details.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1
     */
    val receiving_bank_branch_name: String? = null,

    /**
     * This parameter contains the bank code associated with the bank name and bank identifier code (BIC) provided by the customer. Note that it is provided only for specific bank service provider end points identified as part of corridor data details.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 225 and a minimum length of 1.
     */

    val bank_code: String? = null,

    /**
     * This parameter contains the three-digit code for the type of transaction that is being submitted. Available types and their uses are provided below:
     * B2P: Business Disbursement to Person - A disbursement of funds from a business to an individual account.
     * B2B: Business to Business- A transfer of funds from one business to another.
     * G2P: Government to Person - A disbursement of funds from a government agency to a private individual person's account.
     * P2P: Person to Person - A transfer of funds from one private individual person's account to another private individual person's account.
     * P2B: Person to Business - A payment by an individual person to a business
     * It holds a string of alphabet characters with a maximum length of three and a minimum length of zero.
     */
    val payment_type: String,

    /**
     * This parameter contains the sender's identification of the source of the funds being submitted.
     * Note that it is provided only for specific receiving service provider end points identified as part of corridor data details.
     * Valid values will be provided during onboarding.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 1.
     */
    val source_of_income: String? = null,

    /**
     * The settlement amount charged to the OI for this transaction, including currency and amount
     */
    val settlement_details: MastercardCrossBoardPaymentAmount,

    /**
     * This parameter contains the code or phrase passed by cash out receiving providers. This value needs to be accepted and passed to the sending consumer in P2P transactions.
     * The sending consumer will need to provide this value to the beneficiary in order for the beneficiary to pick up the funds from the receiving service provider.
     * Note that is will only be provided when the receiving service provider is a cash-out location.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 30 and a minimum length of 1.
     */
    val cashout_code: String? = null,

    /**
     * This parameter contains the rate used for the given corridor transaction.
     * It is the rate used to calculate the sender or originating institution amount into the beneficiary amount.
     * The format for this value is 1 to 10 numbers to the left of the decimal and 1 to 10 numbers to the right of the decimal.
     */
    val fx_rate: String,

    /**
     * This parameter contains the list of name/value pairs containing additional parameter values.
     * It is a conditional parameter where there can be zero to many fields sent in the payload.
     * The number of fields, and when they are required, is based on the specific RSP and transaction type.
     */
    val additional_data_list: MastercardAdditionalDataWrapper?,

    /**
     * This parameter identifies the payment file in which this specific payment was submitted by the originator.
     * Note that it is provided if the payment was generated via a batch payment file.
     * Will be blank if not generated via a batch payment file
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 1.
     */
    val payment_file_identifier: String? =  null,
    /**
     * This Parameter contains the System generated Rate ID associated with a currency pair that will be used in a transaction.
     * This ID will be included by the OI in the Payment request and provided to the OI in the Rate File / API for those customers configured to receive this rate information.
     * Note this value must not be provided if a proposal_id is being used in the payment because the rate applied to the payment will be based on the quote provided.\
     * It is an optional parameter that holds a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 1.
     */
    val card_rate_id: String? = null,
)

data class MastercardCrossBoardReversePaymentResponse(
    /**
     *   This parameter contains the client provided unique reference number.
     *   It holds a string of alphanumeric special characters with a maximum length of 40 and a minimum length of 1.
     */
    val transaction_reference: String,

    /**
     * This parameter contains the system generated unique payment identifier.
     * It holds a string of alphanumeric special characters with a maximum length of 31 and a minimum length of 0.
     */
    val id: String,

    /**
     * The resource type of the payment. This will always be set to the value of "payment"
     */
    val resource_type: String,

    /**
     *  Date and time the original payment was created.
     *  It holds an "ISO 8601" timestamp of the format "YYYY-MM-DDTHH:MM:SS±hh[:mm]"
     */
    val created: String,

    /**
     *  This parameter specifies the proposal that was executed for this payment.
     *  It holds a string of alphanumeric special characters with a maximum length of 30 and a minimum length of 0.
     *
     */
    val proposal_id: String,

    /**
     * This parameter contains the status of the payment. The value will be one of the following: SUCCESS, PENDING, REJECTED.
     */
    val status: String,

    /**
     * Timestamp of when the status was changed to its current value.
     * It holds an "ISO 8601" timestamp of the format "YYYY-MM-DDTHH:MM:SS±hh[:mm]"
     */
    val status_timestamp: String,
    /**
     * This parameter contains an identifier showing the current transaction's pending stage. Note that this parameter is only returned if the status is PENDING. Refer to the "Pending Stages section in Payment API" for valid values.
     * It is conditional and holds a string of alphabet characters with a maximum length of 30 and a minimum length of 1.
     */
    val pending_stage: String? = null,
    /**
     * This value is used for Mastercard Send internal processes only and it SHOULD NOT be used as an indicator of funds delivery into the beneficiary account or passed to the sending customers by an OI.
     *  It is conditional and only returned if the status is PENDING.
     * It holds an "ISO 8601" timestamp of the format. For JSON: YYYY-MMDDTHH:MM:SS±hh[:mm] , For XML: YYYY-MMDDTHH:MM:SS.SSS±hh[:mm]
     */
    val pending_max_completion_date: String? = null,

    /**
     * Contains the fees amount for the transaction (actual or proposed), in sender's currency.
     */
    val fees_amount: MastercardCrossBoardPaymentAmount,
    /**
     * The amount to be charged to the sender.
     */
    val charged_amount: MastercardCrossBoardPaymentAmount,
    /**
     * The amount credited on the beneficiary's account.
     */
    val credited_amount: MastercardCrossBoardPaymentAmount,
    /**
     * The amount that was used to calculate fees.
     */
    val principal_amount: MastercardCrossBoardPaymentAmount,

    /**
     * This parameter contains the URI identifying sender's account to fund the payment.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 200 and a minimum length of 1. Refer to the "Sender Account URI" section on "Additional Resources" page for details about scheme specific data.
     */
    val sender_account_uri: String? = null,

    /**
     * This parameter contains the URI identifying beneficiary's account to receive the payment.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 200 and a minimum length of 1. Refer to the "Recipient Account URI" section on "Additional Resources" page for details about scheme specific data.
     */
    val recipient_account_uri: String? = null,

    /**
     * Amount of the payment.
     */
    val payment_amount: MastercardCrossBoardPaymentAmount,

    /**
     * This parameter contains the three-letter "ISO 3166-1 alpha-3" country code representing the country in which the payment is being initiated.
     * It holds a string of alphabet characters with an exact length of three.

     */
    val payment_origination_country: String,

    /**
     * Either a forward fx_type or a reverse fx_type should be submitted, but not both. All fx_type parameters are optional (forward fees included = true when fx_type not provided). Forward fx_type should not be used with the Customer Managed Sender Pricing model.
     * Note that you would not provide these fields if proposal_id was provided because the data will be based on the quote provided.
     */
    val fx_type: MastercardCrossBoardFxType,

    /**
     * This parameter contains the name of the bank holding the receiving account.Note that it is provided only for specific bank service provider end points identified as part of corridor data details.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.
     */
    val receiving_bank_name: String? = null,

    /**
     * This parameter contains the name of the bank branch holding the receiving account.
     * Note that it is provided only for specific bank service provider end points identified as part of corridor data details.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1
     */
    val receiving_bank_branch_name: String? = null,

    /**
     * This parameter contains the bank code associated with the bank name and bank identifier code (BIC) provided by the customer. Note that it is provided only for specific bank service provider end points identified as part of corridor data details.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 225 and a minimum length of 1.
     */

    val bank_code: String? = null,

    /**
     * This parameter contains the three-digit code for the type of transaction that is being submitted. Available types and their uses are provided below:
     * B2P: Business Disbursement to Person - A disbursement of funds from a business to an individual account.
     * B2B: Business to Business- A transfer of funds from one business to another.
     * G2P: Government to Person - A disbursement of funds from a government agency to a private individual person's account.
     * P2P: Person to Person - A transfer of funds from one private individual person's account to another private individual person's account.
     * P2B: Person to Business - A payment by an individual person to a business
     * It holds a string of alphabet characters with a maximum length of three and a minimum length of zero.
     */
    val payment_type: String,

    /**
     * This parameter contains the sender's identification of the source of the funds being submitted.
     * Note that it is provided only for specific receiving service provider end points identified as part of corridor data details.
     * Valid values will be provided during onboarding.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 1.
     */
    val source_of_income: String? = null,

    /**
     * The settlement amount charged to the OI for this transaction, including currency and amount
     */
    val settlement_details: MastercardCrossBoardPaymentAmount,

    /**
     * This parameter contains the code or phrase passed by cash out receiving providers. This value needs to be accepted and passed to the sending consumer in P2P transactions.
     * The sending consumer will need to provide this value to the beneficiary in order for the beneficiary to pick up the funds from the receiving service provider.
     * Note that is will only be provided when the receiving service provider is a cash-out location.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 30 and a minimum length of 1.
     */
    val cashout_code: String? = null,

    /**
     * This parameter contains the rate used for the given corridor transaction.
     * It is the rate used to calculate the sender or originating institution amount into the beneficiary amount.
     * The format for this value is 1 to 10 numbers to the left of the decimal and 1 to 10 numbers to the right of the decimal.
     */
    val fx_rate: String,

    /**
     * This parameter contains the list of name/value pairs containing additional parameter values.
     * It is a conditional parameter where there can be zero to many fields sent in the payload.
     * The number of fields, and when they are required, is based on the specific RSP and transaction type.
     */
    val additional_data_list: MastercardAdditionalDataWrapper?,

    /**
     * This parameter identifies the payment file in which this specific payment was submitted by the originator.
     * Note that it is provided if the payment was generated via a batch payment file.
     * Will be blank if not generated via a batch payment file
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 1.
     */
    val payment_file_identifier: String? =  null,
    /**
     * This Parameter contains the System generated Rate ID associated with a currency pair that will be used in a transaction.
     * This ID will be included by the OI in the Payment request and provided to the OI in the Rate File / API for those customers configured to receive this rate information.
     * Note this value must not be provided if a proposal_id is being used in the payment because the rate applied to the payment will be based on the quote provided.\
     * It is an optional parameter that holds a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 1.
     */
    val card_rate_id: String? = null,
)


data class MastercardCrossBoardPaymentResponse(
    /**
     *   This parameter contains the client provided unique reference number.
     *   It holds a string of alphanumeric special characters with a maximum length of 40 and a minimum length of 1.
     */
    val transaction_reference: String,

    /**
     * This parameter contains the system generated unique payment identifier.
     * It holds a string of alphanumeric special characters with a maximum length of 31 and a minimum length of 0.
     */
    val id: String,

    /**
     * The resource type of the payment. This will always be set to the value of "payment"
     */
    val resource_type: String,

    /**
     *  Date and time the original payment was created.
     *  It holds an "ISO 8601" timestamp of the format "YYYY-MM-DDTHH:MM:SS±hh[:mm]"
     */
    val created: String,

    /**
     *  This parameter specifies the proposal that was executed for this payment.
     *  It holds a string of alphanumeric special characters with a maximum length of 30 and a minimum length of 0.
     *
     */
    val proposal_id: String,

    /**
     * This parameter contains the status of the payment. The value will be one of the following: SUCCESS, PENDING, REJECTED.
     */
    val status: String,

    /**
     * Timestamp of when the status was changed to its current value.
     * It holds an "ISO 8601" timestamp of the format "YYYY-MM-DDTHH:MM:SS±hh[:mm]"
     */
    val status_timestamp: String,
    /**
     * This parameter contains an identifier showing the current transaction's pending stage. Note that this parameter is only returned if the status is PENDING. Refer to the "Pending Stages section in Payment API" for valid values.
     * It is conditional and holds a string of alphabet characters with a maximum length of 30 and a minimum length of 1.
     */
    val pending_stage: String? = null,
    /**
     * This value is used for Mastercard Send internal processes only and it SHOULD NOT be used as an indicator of funds delivery into the beneficiary account or passed to the sending customers by an OI.
     *  It is conditional and only returned if the status is PENDING.
     * It holds an "ISO 8601" timestamp of the format. For JSON: YYYY-MMDDTHH:MM:SS±hh[:mm] , For XML: YYYY-MMDDTHH:MM:SS.SSS±hh[:mm]
     */
    val pending_max_completion_date: String? = null,

    /**
     * Contains the fees amount for the transaction (actual or proposed), in sender's currency.
     */
    val fees_amount: MastercardCrossBoardPaymentAmount,
    /**
     * The amount to be charged to the sender.
     */
    val charged_amount: MastercardCrossBoardPaymentAmount,
    /**
     * The amount credited on the beneficiary's account.
     */
    val credited_amount: MastercardCrossBoardPaymentAmount,
    /**
     * The amount that was used to calculate fees.
     */
    val principal_amount: MastercardCrossBoardPaymentAmount,

    /**
     * This parameter contains the URI identifying sender's account to fund the payment.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 200 and a minimum length of 1. Refer to the "Sender Account URI" section on "Additional Resources" page for details about scheme specific data.
     */
    val sender_account_uri: String? = null,

    /**
     * This parameter contains the URI identifying beneficiary's account to receive the payment.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 200 and a minimum length of 1. Refer to the "Recipient Account URI" section on "Additional Resources" page for details about scheme specific data.
     */
    val recipient_account_uri: String? = null,

    /**
     * Amount of the payment.
     */
    val payment_amount: MastercardCrossBoardPaymentAmount,

    /**
     * This parameter contains the three-letter "ISO 3166-1 alpha-3" country code representing the country in which the payment is being initiated.
     * It holds a string of alphabet characters with an exact length of three.

     */
    val payment_origination_country: String,

    /**
     * Either a forward fx_type or a reverse fx_type should be submitted, but not both. All fx_type parameters are optional (forward fees included = true when fx_type not provided). Forward fx_type should not be used with the Customer Managed Sender Pricing model.
     * Note that you would not provide these fields if proposal_id was provided because the data will be based on the quote provided.
     */
    val fx_type: MastercardCrossBoardFxType,

    /**
     * This parameter contains the name of the bank holding the receiving account.Note that it is provided only for specific bank service provider end points identified as part of corridor data details.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.
     */
    val receiving_bank_name: String? = null,

    /**
     * This parameter contains the name of the bank branch holding the receiving account.
     * Note that it is provided only for specific bank service provider end points identified as part of corridor data details.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1
     */
    val receiving_bank_branch_name: String? = null,

    /**
     * This parameter contains the bank code associated with the bank name and bank identifier code (BIC) provided by the customer. Note that it is provided only for specific bank service provider end points identified as part of corridor data details.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 225 and a minimum length of 1.
     */

    val bank_code: String? = null,

    /**
     * This parameter contains the three-digit code for the type of transaction that is being submitted. Available types and their uses are provided below:
     * B2P: Business Disbursement to Person - A disbursement of funds from a business to an individual account.
     * B2B: Business to Business- A transfer of funds from one business to another.
     * G2P: Government to Person - A disbursement of funds from a government agency to a private individual person's account.
     * P2P: Person to Person - A transfer of funds from one private individual person's account to another private individual person's account.
     * P2B: Person to Business - A payment by an individual person to a business
     * It holds a string of alphabet characters with a maximum length of three and a minimum length of zero.
     */
    val payment_type: String,

    /**
     * This parameter contains the sender's identification of the source of the funds being submitted.
     * Note that it is provided only for specific receiving service provider end points identified as part of corridor data details.
     * Valid values will be provided during onboarding.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 1.
     */
    val source_of_income: String? = null,

    /**
     * The settlement amount charged to the OI for this transaction, including currency and amount
     */
    val settlement_details: MastercardCrossBoardPaymentAmount,

    /**
     * This parameter contains the code or phrase passed by cash out receiving providers. This value needs to be accepted and passed to the sending consumer in P2P transactions.
     * The sending consumer will need to provide this value to the beneficiary in order for the beneficiary to pick up the funds from the receiving service provider.
     * Note that is will only be provided when the receiving service provider is a cash-out location.
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 30 and a minimum length of 1.
     */
    val cashout_code: String? = null,

    /**
     * This parameter contains the rate used for the given corridor transaction.
     * It is the rate used to calculate the sender or originating institution amount into the beneficiary amount.
     * The format for this value is 1 to 10 numbers to the left of the decimal and 1 to 10 numbers to the right of the decimal.
     */
    val fx_rate: String,

    /**
     * This parameter contains the list of name/value pairs containing additional parameter values.
     * It is a conditional parameter where there can be zero to many fields sent in the payload.
     * The number of fields, and when they are required, is based on the specific RSP and transaction type.
     */
    val additional_data_list: MastercardAdditionalDataWrapper?,

    /**
     * This parameter identifies the payment file in which this specific payment was submitted by the originator.
     * Note that it is provided if the payment was generated via a batch payment file.
     * Will be blank if not generated via a batch payment file
     * It is conditional and holds a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 1.
     */
    val payment_file_identifier: String? =  null,
    /**
     * This Parameter contains the System generated Rate ID associated with a currency pair that will be used in a transaction.
     * This ID will be included by the OI in the Payment request and provided to the OI in the Rate File / API for those customers configured to receive this rate information.
     * Note this value must not be provided if a proposal_id is being used in the payment because the rate applied to the payment will be based on the quote provided.\
     * It is an optional parameter that holds a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 1.
     */
    val card_rate_id: String? = null,
)
