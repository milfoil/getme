package integration.mastercard.crossborder

import http.HttpException
import integration.mastercard.MastercardAPIClient
import integration.mastercard.crossborder.payment.MastercardCrossBoardPaymentRequest
import integration.mastercard.crossborder.payment.MastercardCrossBoardPaymentRequestWrapper
import integration.mastercard.crossborder.payment.MastercardCrossBoardPaymentResponseWrapper
import integration.mastercard.crossborder.payment.MastercardCrossBorderCancelPaymentRequest
import integration.mastercard.crossborder.payment.MastercardCrossBorderCancelPaymentRequestWrapper
import integration.mastercard.crossborder.payment.MastercardCrossBorderCancelPaymentResponse
import integration.mastercard.crossborder.payment.MastercardCrossBorderRateResponse
import integration.mastercard.crossborder.payment.MastercardCrossBorderRetrievePaymentResponse
import integration.mastercard.crossborder.payment.MastercardCrossBorderRetrievePaymentResponseWrapper
import integration.mastercard.crossborder.quote.MastercardCrossBorderQuoteRequest
import integration.mastercard.crossborder.quote.MastercardCrossBorderQuoteRequestWrapper
import integration.mastercard.crossborder.quote.MastercardCrossBorderQuoteResponse
import utils.JsonTool

class MastercardCrossBorderService : MastercardAPIClient() {
    private val baseURL = "$host/send/v1/partners/$partnerId/crossborder"

    /**
     * Quotes API
     * You can use this API to obtain a FX Rate quote before a payment is submitted. It also provides information like amount to be credited to the beneficiary, amount to be charged to sender, the origination Institution settlement amount and more.
     * Quotes API is informational only and doesn’t initiate the movement of funds.
     * See Payment with Quote to understand how you can use Quote API with a payment transaction.
     */
    fun getQuote(request: MastercardCrossBorderQuoteRequest) = runCatching {
        val serviceURL = "$baseURL/quotes"

        /**
         * Construct the original JSON Request per the API specification.
         */
        val response = post(
            serviceURL, MastercardCrossBorderQuoteRequestWrapper(
                quoterequest = request
            )
        )
        JsonTool.toJson(response, MastercardCrossBorderQuoteResponse::class.java)
    }.getOrElse {
        println(it.stackTraceToString())
        throw HttpException(it.message, it.cause)
    }

    /**
     *  Payment API
     * Create a payment transaction to send funds to a Recipient.
     * @param request  a cross-border payment.
     */
    fun createPayment(request: MastercardCrossBoardPaymentRequest) = runCatching {
        val serviceURI = "$baseURL/payment"
        /**
         * Construct the original JSON Request per the API specification.
         */
        println(JsonTool.stringify(request))
        val response = post(serviceURI, MastercardCrossBoardPaymentRequestWrapper(request))
        JsonTool.toJson(response, MastercardCrossBoardPaymentResponseWrapper::class.java)
    }.getOrElse {
        println(it.stackTraceToString())
        throw HttpException(it.message, it.cause)
    }

    /**
     *  Retrieve Payment API
     * Search for details of a payment transaction by transaction Id or transaction Reference.
     */
    fun getPaymentByTransactionId(transactionId: String) = runCatching {
        val serviceURI = "$baseURL/$transactionId"
        get<MastercardCrossBorderRetrievePaymentResponseWrapper>(serviceURI)
    }.getOrElse {
        println(it.stackTraceToString())
        throw HttpException(it.message, it.cause)
    }

    /**
     *  Retrieve Payment API
     * Search for details of a payment transaction by  transaction Reference.
     */
    fun getPaymentByTransactionRef(ref: String) = runCatching {
        val serviceURI = "$baseURL?ref=$ref"
        get<MastercardCrossBorderRetrievePaymentResponseWrapper>(serviceURI)
    }.getOrElse {
        println(it.stackTraceToString())
        throw HttpException(it.message, it.cause)
    }

    /**
     * Cancel Payment API
     * Cancel a payment transaction that was initiated earlier.
     * ONLY applicable to cash-out and some mobile money providers.
     */
    fun cancelPayment(paymentId: String) = runCatching {
        val serviceURI = "$baseURL/$paymentId/cancel"
        val response = post(
            serviceURI, MastercardCrossBorderCancelPaymentRequestWrapper(
                MastercardCrossBorderCancelPaymentRequest(null)
            )
        )

        JsonTool.toJson(response, MastercardCrossBorderCancelPaymentResponse::class.java)
    }.getOrElse {
        println(it.stackTraceToString())
        throw HttpException(it.message, it.cause)
    }


    /**
     *  Carded Rate API
     * Obtain FX rates for currency pairs before the payment transaction is initiated.
     * Carded Rate is offered as an opt-in functionality to obtain the FX rates for the currency pairs that you, as the originating institution (OI) support, for a valid period of time.
     * Please see Payment with Carded Rate for more information on usage of the carded rates.
     */
    fun getFxRatePull() = runCatching {
        val serviceURI = "$baseURL/rates"
        get<MastercardCrossBorderRateResponse>(serviceURI)
    }.getOrElse {
        println(it.stackTraceToString())
        throw HttpException(it.message, it.cause)
    }

    /**
     * The FX Rate API - Push, once coded to, will provide the ability for Cross-Border Services to push all FX rates to the OI that are refreshing at the time of the push without any requests by the OI.
     * Depending on the number and FX refresh schedules of the rates, the OI should expect to receive multiple pushes a day.
     */
   // fun getFxRatePush() = runCatching {
   //
   // }


    /**
     *  @TODO Status Change API
     * The Status Change API will provide near real-time payment status updates to those OIs who opt-in for the service.
     */
}
