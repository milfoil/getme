package integration.mastercard.crossborder

import http.HttpException
import integration.mastercard.MastercardAPIClient
import integration.mastercard.MastercardConfiguration
import integration.mastercard.crossborder.quote.MastercardCrossBorderQuoteRequest
import integration.mastercard.crossborder.quote.MastercardCrossBorderQuoteRequestWrapper
import integration.mastercard.crossborder.quote.MastercardCrossBorderQuoteResponse
import utils.JsonTool

/**
 * Quotes API
 * You can use this API to obtain a FX Rate quote before a payment is submitted. It also provides information like amount to be credited to the beneficiary, amount to be charged to sender, the origination Institution settlement amount and more.
 * Quotes API is informational only and doesn’t initiate the movement of funds.
 * See Payment with Quote to understand how you can use Quote API with a payment transaction.
 */
class MastercardCrossBorderQuoteService :  MastercardAPIClient() {
    private val baseURL = "$host/send/v1/partners/$partnerId/crossborder"

    /**
     * Usecase - 1 - **FORWARD QUOTE WITH FEES INCLUDED**
     */

    suspend fun getForwardQuoteWithFeesIncluded(payload: MastercardCrossBorderQuoteRequest) = runCatching {
        val wrapper = MastercardCrossBorderQuoteRequestWrapper( payload )
        val response = post( "$baseURL/quotes", wrapper )
        JsonTool.toJson(response, MastercardCrossBorderQuoteResponse::class.java)
    }.getOrElse {
        println(it.stackTraceToString())
        throw HttpException(it.message, it.cause)
    }

    /**
   * Usecase - 2 - **FORWARD QUOTE WITH FEES NOT INCLUDED**
    */
    suspend fun getForwardQuoteWithFeesNotIncluded() {

    }

    suspend fun getReverseQuote() {

    }
}
