package integration.mastercard.crossborder.model

data class MastercardCrossBorderGovernmentIdUri(
    val government_id_uri: String
)
