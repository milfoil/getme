package integration.mastercard.crossborder.model

data class MastercardCrossBoardAddress(
    /**
     * This parameter contains the city where the person resides.
     * For transactions where it is not a person, this is the city where the business/entity resides.
     * (State, zip code, or other address elements must not be added to this field or transactions rejections may occur)
     * It holds a string of alphanumeric special characters with a maximum length of 35 and a minimum length of 1
     */
    val  city: String,
    /**
     * This parameter contains the postal code of the person.
     * For transactions where it is not a person, this is the postal code of the Business/Entity.
     * This parameter is only required for countries that use postal codes.
     * This parameter holds a string of alphanumeric special characters with a maximum length of 16 and a minimum length of 1.
     */
    val  postal_code: String,
    /**
     * This parameter contains the state/province of the person.
     * For transactions where it is a Business/Entity, this is the state/province where the business/entity resides.
     * For Countries that do not have a country subdivision, this field should not be provided in the payment request.
     * (City, zip code, or other address elements must not be added to this field or transactions rejections may occur)
     */
    val  country_subdivision: String,
    /**
     *  This parameter contains the three-letter "ISO 3166-1 alpha-3" country code of the person.
     *  When it is not a person, this is the country of the business/entity.
     *  (City, state, zip code, or other address elements must not be added to this field or transactions rejections may occur)
     *  This parameter holds a string of alphabet characters with an exact length of three.
     */
    val  country: String,
    /**
     * This parameter contains the value in address line 2 of the person.
     * For transactions where it is not a person, this is address line 2 for the business/entity.
     * This field should contain additional information about the address only (as applicable): Apartment/room/suite identifier.
     * (City, State, zip code, or other address elements must not be added to this field or transactions rejections may occur)
     * It holds a string of alphanumeric special characters with a maximum length of 500 and a minimum length of 1
     */
    val  line2: String,
    /**
     *  This parameter contains the value in address line 1 of the person.
     *  For transactions where it is not a person, this is address line 1 for the business/entity.
     *  This field should contain the following components of the address only (each part as applicable):
     *  Physical address number, Directional indicators (N, W, SW, etc), Street Name, Thoroughfare type (LN, BLVD. etc).
     *  (City, State, zip code, or other address elements must not be added to this field or transactions rejections may occur)
     *  It holds a string of alphanumeric special characters with a maximum length of 500 and a minimum length of 1.
     */
    val  line1: String,
)
