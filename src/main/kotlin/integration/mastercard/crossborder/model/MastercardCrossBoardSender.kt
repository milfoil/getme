package integration.mastercard.crossborder.model

data class MastercardCrossBoardSender(
    val  first_name: String,
    val  middle_name: String? = null,
    val  last_name: String,
    val  organization_name: String? = null,
    val  nationality: String,
    val  address: MastercardCrossBoardAddress,
    val  government_ids: List<MastercardCrossBorderGovernmentIdUri>,
    val  date_of_birth: String,
)
