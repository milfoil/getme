package integration.mastercard.crossborder.model

data class MastercardCrossBoardRecipient(
    /**
     * This parameter contains the beneficiary's first name for transactions where the beneficiary is a person.
     * For transactions where the beneficiary is not a person, this field is not allowed.
     * Note that it is required for transactions where the beneficiary is a person, i.e. the value of quoterequest or paymentrequest.payment_type is B2P, G2P, P2P.
     * Providing this with other payment types will cause an error.
     * It is a conditional parameter that holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.
     */
    val  first_name: String? = null,
    /**
     * This parameter contains the beneficiary's middle name For transactions where the beneficiary is a person.
     * For transactions where the beneficiary is not a person, this field is not allowed.
     * Note that this parameter is optional for transactions where the beneficiary is a person, i.e. the value of quoterequest or paymentrequest.payment_type is B2P, G2P, P2P.
     * Providing this with other payment types will cause an error.
     * It is an optional parameter that holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.
     */
    val  middle_name: String? = null,
    /**
     * This parameter contains the beneficiary's last name for transactions where the beneficiary is a person.
     * For transactions where the beneficiary is not a person, this field is not allowed.
     * Note that this parameter is required for transactions where the beneficiary is a person, i.e. the value of quoterequest or paymentrequest.payment_type is B2P, G2P, P2P.
     * Providing this with other payment types will cause an error.
     * It is a conditional parameter that holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.
     */
    val  last_name: String? =null,
    /**
     *  This parameter contains an organization's name for transactions where the beneficiary is not a person.
     *  For transactions where the beneficiary is a person, this field is not allowed.
     *
     *  Note that this parameter is required for transactions where the beneficiary is an organization, i.e. the value of quoterequest or paymentrequest.payment_type is B2B, P2B.
     *  Providing this with other payment types will cause an error.
     *  It is a conditional parameter that holds a string of alphanumeric special characters with a maximum length of 140 and a minimum length of 1.
     *
     */
    val  organization_name: String? = null,
    /**
     *  For transactions where the beneficiary is a person, this is the beneficiary's nationality.
     *  This parameter contains the three-letter "ISO 3166-1 alpha-3" country code representing beneficiary's nationality.
     *  In the case of a business or government organization, this is the registered country of the organization.
     *  It holds a string of alphabet characters with an exact length of three.
     */
    val  nationality: String,
    /**
     * For transactions where there is a person, this is the address of the person.
     * For transactions where there is no person, this is the address for the business/entity
     */
    val  address: MastercardCrossBoardAddress,
    /**
     * List of recipient government ids. Note: There can be 0 to many of these fields required and used for each transaction.
     */
    val  government_ids: List<MastercardCrossBorderGovernmentIdUri>,
    /**
     *  This parameter contains the phone number of the beneficiary for transactions where the beneficiary is a person.
     *  For transactions where the beneficiary is a business/entity, this this is the phone number of a contact at the business/entity
     *  It holds a numeric value with a maximum length of 30 and a minimum length of 1.
     *
     */
    val  phone: String,
    /**
     *  For transactions where the beneficiary is a person, this is the email address of the beneficiary.
     *  For transactions where the beneficiary is a Business/Entity, this is the email address of a contact at the business/entity.
     *  It holds a string of alphanumeric special characters with a maximum length of 1000 and a minimum length of 5.
     */
    val  email: String,
)
