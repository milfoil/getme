package integration.mastercard.crossborder.model


data class MastercardAdditionalDataWrapper(
    val data_field: List<MastercardAdditionalData>,
)

data class MastercardAdditionalDataListWrapper(
    /**
     * This parameter contains the type of additional data list resource type.
     * Note that it is provided if additional_data_list is populated.
     * It is conditional and will only hold a string with the value "list".
     */
    val resource_type: String,
    /**
     * This parameter contains the number of items in additional_data_list.
     * Note it is provided if additional_data_list is populated.
     * It is conditional and holds a numeric value with a maximum length of two and a minimum length of one.
     */
    val item_count: String,

    /**
     * Set of data contained in additional_data_list.
     */
    val data: List<MastercardAdditionalData>,
)

data class MastercardAdditionalData(
    /**
     * This parameter contains an identifier for an additional field. It is required if the data.data_field.value is populated.
     * It is a conditional parameter that holds a string of numeric values with a maximum length of five and a minimum length of one.
     */
    val name: String,
    /**
     *  This parameter contains detailed information for an additional field.
     *  This parameter is required if the data.data_field.name is populated.
     *  It is a conditional parameter that holds a string of alphanumeric special characters with a maximum length of 1000 and a minimum length of 1.
     */
    val value: String,
)



