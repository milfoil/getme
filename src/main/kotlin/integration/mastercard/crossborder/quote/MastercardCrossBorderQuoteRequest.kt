package integration.mastercard.crossborder.quote

import integration.mastercard.crossborder.payment.MastercardCrossBoardPaymentAmount

data class MastercardCrossBorderQuoteRequestWrapper(
    val quoterequest: MastercardCrossBorderQuoteRequest
)

data class MastercardCrossBorderQuoteRequest(
    /**
     * This parameter contains the client-provided unique reference number of the transaction. This value will be utilized as the transaction_reference ID of the payment if a consumer decides to move forward with a payment using the submitted quote. The string held must be unique per transaction.
     * This parameter holds a string of alphanumeric special characters with a maximum length of 40 and a minimum length of 1.
     * example: "0734311ASRWQYYHJsawe5636421307734RTYH_1"
     * required: yes
     */
    val transaction_reference: String,
    /**
     * This parameter contains the URI identifying a sender's account to fund the payment.
     * It holds a string of alphanumeric special characters with a maximum length of 200 and a minimum length of 1. Refer to the "Sender Account URI" section on "Additional Resources" page for details about scheme specific data.
     * Note: Where a bank account is the funding source, the iban or ban scheme must be provided.
     * example: "tel:+25406005"
     * required: yes
     */
    val sender_account_uri: String,

    /**
     * This parameter contains the URI identifying a beneficiary's account to receive the payment.
     * Note that in this parameter, when using the "pan" scheme, the "exp" property is optional.
     * It holds a string of alphanumeric special characters with a maximum length of 200 and a minimum length of 1. Refer to the "Recipient Account URI" section on "Additional Resources" page for details about scheme specific data.
     * example: "tel:+254069832"
     * required: yes
     */
    val recipient_account_uri: String,

    /**
     * PaymentAmount
     * Amount of the payment.
     * required: yes
     */
    val payment_amount: MastercardCrossBoardPaymentAmount,

    /**
     * This parameter contains the three-letter "ISO 3166-1 alpha-3" country code representing the country in which the payment is being initiated.
     * It holds a string of alphabet characters with an exact length of three.
     * example: "USA"
     * required: yes
     */
    val payment_origination_country: String,

    /**
     * This parameter contains the three-digit code for the type of transaction that is being submitted. Available types and their uses are provided below:
     * B2P: Business Disbursement to Person - A disbursement of funds from a business to an individual account.
     * B2B: Business to Business- A transfer of funds from one business to another.
     * G2P: Government to Person - A disbursement of funds from a government agency to a private individual person's account.
     * P2P: Person to Person - A transfer of funds from one private individual person's account to another private individual person's account.
     * P2B: Person to Business - A payment by an individual person to a business
     * It holds a string of alphabet characters with an exact length of three.
     */
    val payment_type: String,

    /**
     * Either a forward quote type or a reverse quote type should be submitted, but not both.
     * All quote type parameters are optional (forward fees included = true when quote type not provided).
     * Forward quote type should not be used with the Customer Managed Sender Pricing model.
     * example: "P2P"
     * required: yes
     */
    val quote_type: MastercardCrossBoardFxType,
    /**
     * This parameter contains the bank code associated with the bank name and bank identifier code (BIC) provided by the customer. This parameter is required only for specific bank service provider endpoints identified as part of corridor data details.
     * It's a conditional parameter that holds alphanumeric special characters with a maximum length of 225 and a minimum length of 1.
     * example: "NP021"
     */
    val bank_code: String,
    /**
     * This parameter contains the list of name/value pairs containing additional parameter values.
     * This parameter is conditional, and only required for specific receiving service provider endpoints identified as part of corridor data details.
     * If this field is needed, then necessary names and values will be supplied during onboarding.
     * There can be zero to many fields sent in the payload.
     */
    // val additional_data: MastercardAdditionalData?
)
