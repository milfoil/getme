package integration.mastercard.crossborder.quote

import integration.mastercard.crossborder.payment.MastercardCrossBoardPaymentAmount


data class MasterCrossBorderQuoteProposalWrapper (
    val proposal: List<MastercardCrossBorderQuoteProposal>
    )

data class MastercardCrossBorderQuoteProposal(
    /**
     * This parameter contains a system generated unique proposal identifier.
     * It holds a string of alphanumeric special characters with a maximum length of 30 and a minimum length of 0.
     * example: "pen_4000287127042877181470275"
     */
    val id: String,

    /**
     * This parameter contains the type of proposals.proposal[].resource type. It is conditional and will only hold a string with the value "proposal". Note that it is provided if proposals.proposal[] is populated.
     * example: "proposal"
     */
    val resource_type: String,

    /**
     * This parameter indicates how the sender_amount was interpreted to compute the amount fields in this proposal
     * It holds a Boolean value of either "true" or "false".
     * example: true
     */
    val fees_included: Boolean,

    /**
     * The date and time when this proposal will expire, i.e. after which it can no longer be used to initiate a payment.
     * It holds an "ISO 8601" timestamp of the format. For JSON:YYYY-MMDDTHH:MM:SS±hh[:mm] , For XML: YYYY-MMDDTHH:MM:SS.SSS±hh[:mm]
     * example: "JSON : 2014-09-11T17:41:08-05:00, XML : 2014-09-11T17:41:08.301-05:00"
     */
    val expiration_date: String,

    /**
     * This parameter contains the rate used for the given corridor transaction. It is the rate used to calculate the sender or originating institution amount into the beneficiary amount.
     * The format for this value is 1 to 10 numbers to the left of the decimal and 1 to 10 numbers to the right of the decimal.
     * example: "3.7833456828"
     */
    val quote_fx_rate: String,

    /**
     * The amount to be charged to the sender.
     */

    val charged_amount: MastercardCrossBoardPaymentAmount,

    /**
     * The amount credited on the beneficiary's account.
     */
    val credited_amount: MastercardCrossBoardPaymentAmount,

    /**
     * The amount that was used to calculate fees.
     */
    val principal_amount: MastercardCrossBoardPaymentAmount,

    /**
     * This parameter contains the list of name/value pairs containing additional parameter values.
     * It is a conditional parameter where there can be zero to many fields sent in the payload.
     * The number of fields, and when they are required, is based on the specific RSP and transaction type.
     */
    // val additional_data_list    AdditionalDataList
)
