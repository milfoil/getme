package integration.mastercard.crossborder.quote

enum class MastercardCrossBoardFxTypes {
    Forward, Reverse
}

data class MastercardCrossBoardFxTypeRequest(
    val type: MastercardCrossBoardFxTypes,
    val currency: String,
    val feesIncluded: Boolean = true
) {
    fun create(): MastercardCrossBoardFxType {
        return when (type) {
            MastercardCrossBoardFxTypes.Forward -> MastercardCrossBoardFxType.Forward(
                forward = MastercardCrossBoardQuoteForward(
                    fees_included = feesIncluded,
                    receiver_currency = currency
                )
            )
            MastercardCrossBoardFxTypes.Reverse -> MastercardCrossBoardFxType.Reverse(
                reverse = MastercardCrossBoardQuoteReverse(
                    sender_currency = currency
                )
            )
        }
    }
}

data class MastercardCrossBoardQuoteForward(
    /**
     * This parameter indicates how payment fees will be paid.
     * If true, fees are subtracted from the sender's amount.
     * If false, then the sender will pay fees in addition to the sender's amount.
     * Note that if no type is used then it defaults to forward type with the fees_included field set to "true".
     * A forward and a reverse type shouldn't be used together. Using forward.fees_included along with reverse.sender_currency throws an error.
     * It's an optional parameter that holds a Boolean value. Valid values are "true" or "false".
     * example: "true"
     */
    val fees_included: Boolean,
    /**
     * This parameter contains the three-letter "ISO 4217" currency code of the account to receive the funds. This parameter is required for forward type where the beneficiary Account uri = pan.
     * It's an optional parameter that holds alphabet characters with an exact length of three.
     * example: "GBP
     */
    val receiver_currency: String,
)

data class MastercardCrossBoardQuoteReverse(
    /**
     * This parameter contains the three-letter "ISO 4217" currency code in which the sender will send the payment. This parameter is required for reverse type.
     * Note that if no type is used then it defaults to forward type with the fees_included field set to "true." A forward and a reverse type shouldn't be used together. Using forward.fees_included along with reverse.sender_currency throws an error.
     * It's an optional parameter that holds alphabet characters with an exact length of three
     * example: "USD"
     */
    val sender_currency: String
)

sealed class MastercardCrossBoardFxType() {
    data class Forward(
        val forward: MastercardCrossBoardQuoteForward
    ) : MastercardCrossBoardFxType()

    data class Reverse(
        val reverse: MastercardCrossBoardQuoteReverse
    ) : MastercardCrossBoardFxType()
}
