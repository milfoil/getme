package integration.mastercard

import com.mastercard.developer.oauth.OAuth
import com.mastercard.developer.utils.AuthenticationUtils
import com.typesafe.config.ConfigFactory
import io.ktor.config.HoconApplicationConfig
import utils.FileUtil
import java.net.URI
import java.nio.charset.StandardCharsets
import java.security.PrivateKey

open class MastercardConfiguration {
    private val env = HoconApplicationConfig(ConfigFactory.load()).config("mastercard")
    private val consumerKey = env.property("consumerKey").getString()
    val host = env.property("environment.sandbox").getString()
    private val keyAlias: String = env.property("keystore.keyAlias").getString()
    private val keyPassword: String = env.property("keystore.keyPassword").getString()
    private val keyFile: String = env.property("keystore.keyFile").getString()
    val certificateFile: String = env.property("encryption.certificateFile").getString()
    val fingerPrint: String = env.property("encryption.fingerPrint").getString()
    val decryptionKeyFile: String = env.property("decryption.keyFile").getString()
    val partnerId: String = env.property("partnerId").getString()
    /**
     * The Signing Key
     */
    private val signingKey: PrivateKey = AuthenticationUtils.loadSigningKey(
        FileUtil.load("/certs/$keyFile").absolutePath,
        keyAlias,
        keyPassword
    )
    /**
     * The Access Token
     * OAuth Authorization Header
     */
    fun getAccessToken(
        service: String,
        method: String,
        data: String
    ): String {
        return OAuth.getAuthorizationHeader(
            URI.create(service),
            method,
            data,
            StandardCharsets.UTF_8,
            consumerKey,
            signingKey
        )
    }
}
