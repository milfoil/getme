package integration.mastercard

import http.HttpException
import integration.mastercard.crossborder.MastercardServiceErrorWrapper
import integration.mastercard.crossborder.payment.MastercardCrossBoardPaymentRequestWrapper
import utils.JsonTool
import utils.loggerUtil
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

open class MastercardAPIClient : MastercardPayloadEncryption() {
    fun <T> post(url: String, payload: T): String {
        try {
            val client = HttpClient.newBuilder().build()
            val encryptedRequest = encryptedRequestPayload(
                JsonTool.stringify(payload)
            )
            /**
             * Use OAuth to authorize request
             */
            val accessToken = getAccessToken(
                service = url,
                method = "POST",
                data = encryptedRequest,
            )
            /**
             * handler response
             */
            val request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Authorization", accessToken)
                .header("x-encrypted", "true")
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .method("post", HttpRequest.BodyPublishers.ofString(encryptedRequest))
                .build()
            val response = client.send(request, HttpResponse.BodyHandlers.ofString())
            return decryptResponsePayload(response.body()).let {
                loggerUtil("Mastercard Service Response", it)
                return@let when (response.statusCode()) {
                    200 -> it
                    in 400..500 -> {
                        val responseError = JsonTool.toJson(it, MastercardServiceErrorWrapper::class.java)
                        throw Error(responseError.Errors.Error.Description)
                    }
                    else -> throw Error("The Mastercard Service API server encountered an unexpected condition which prevented it from fulfilling the request. ")
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            throw HttpException(e.message, e.cause)
        }
    }
    inline fun <reified T> get(url: String): T {
        try {
            val client = HttpClient.newBuilder().build()
            /**
             * Use OAuth to authorize request
             */
            val accessToken = getAccessToken(
                service = url,
                method = "GET",
                data = "",
            )
            /**
             * handler response
             */
            val request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Authorization", accessToken)
                .header("x-encrypted", "true")
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .method("get", HttpRequest.BodyPublishers.ofString(""))
                .build()

            val response = client.send(request, HttpResponse.BodyHandlers.ofString())
            return decryptResponsePayload(response.body()).let {
                loggerUtil("Mastercard Service Response", it)
                return@let when (response.statusCode()) {
                    200 -> JsonTool.toJson(it,  T::class.java)
                    in 400..500 -> {
                        val responseError = JsonTool.toJson(it, MastercardServiceErrorWrapper::class.java)
                        throw Error(responseError.Errors.Error.Description)
                    }
                    else -> throw Error("The Mastercard Service API server encountered an unexpected condition which prevented it from fulfilling the request. ")
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            throw HttpException(e.message, e.cause)
        }
    }
}
