package integration.travelport

/**
 * The Air Availability Search returns scheduled flights between a given city pair on a given day
 * , and indicates whether seats are available on those flights.
 * Note that the Air Availability Search returns availability, but does not include any pricing information.
 * To obtain pricing, a follow-up Air Pricing transaction must be made.
 */
suspend fun checkAvailability() {

}