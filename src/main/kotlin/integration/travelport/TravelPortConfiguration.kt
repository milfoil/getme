package integration.travelport

import com.typesafe.config.ConfigFactory
import io.ktor.config.HoconApplicationConfig


internal  val env = HoconApplicationConfig(ConfigFactory.load()).config("travelport.staging")

data class TravelPortConfiguration(
    val host: String = env.property("host").getString(),
    val username: String = env.property("username").getString(),
    val password: String = env.property("password").getString(),
    val branch: String = env.property("targetbranch").getString(),
    val pcc: String = env.property("pcc").getString(),
)
