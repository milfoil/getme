package integration.verifyID

import http.httpClient
import integration.verifyID.model.VerifyAuthResponseInterface
import io.ktor.client.request.get
import io.ktor.http.ContentType
import io.ktor.http.contentType

open class VerifyIdAccountService : VerifyIdConfiguration() {
    /**
     * Account Credits
     * Check to see how many credits you have on system
     * @return string
     */
    suspend fun getAccountBalance(): String {
        try {
            val accessKey = getAccessToken()
            val response: VerifyAuthResponseInterface =
                httpClient.get("$host/my_credits?api_key=$accessKey") {
                    contentType(ContentType.Application.Json)
                }
            if (response.Error == null) {
                if (response.Result.contains("credits")) {
                    val credit = response.Result["credits"]
                    if (!credit.isNullOrEmpty()) {
                        return credit
                    }
                }
            }
            throw Error(response.Error)
        } catch (ex: Throwable) {
            throw Error(ex)
        }
    }

    /**
     * This Function will return how many SADL credits you have left on the VerifyID System.
     * @return string
     */
    suspend fun getCredits(): String {
        try {
            val accessKey = getAccessToken()
            val response: VerifyAuthResponseInterface =
                httpClient.get("$host/my_sadl_credits?api_key=$accessKey") {
                    contentType(ContentType.Application.Json)
                }
            if (response.Error == null) {
                if (response.Result.contains("credits")) {
                    val credit = response.Result["credits"]
                    if (!credit.isNullOrEmpty()) {
                        return credit
                    }
                }
            }
            throw Error(response.Error)
        } catch (ex: Throwable) {
            throw Error(ex)
        }
    }
}
