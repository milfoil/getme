package integration.verifyID

import constant.ErrorMessage
import http.HttpException
import http.httpClient
import integration.verifyID.model.VerifyIDPhotoResponse
import integration.verifyID.model.VerifyIDResponse
import integration.verifyID.model.VerifyId
import integration.verifyID.model.VerifyIdPhoto
import integration.verifyID.model.VerifyIdRealTime
import integration.verifyID.model.VerifyIdRealTimeResponse
import integration.verifyID.model.VerifyMaritalStatus
import integration.verifyID.model.VerifyMaritalStatusResponse
import io.ktor.client.request.post
import io.ktor.http.ContentType
import io.ktor.http.contentType
import utils.loggerUtil

class VerifyIdService(private val idNumber: String) : VerifyIdAccountService() {

    private val idPhotoEnquiryReason: String = "We need to verify user identity for security reasons and " +
        "compliance with international KYC/AML regulations."

    /**
     * Verifies a given South African ID number against the live Home Affairs Database
     * @return [VerifyId]
     */
    suspend fun verifyId(): VerifyId {
        try {
            val verifyIdResponse: VerifyIDResponse =
                httpClient.post("$host/said_verification") {
                    contentType(ContentType.Application.Json)
                    body = mapOf(
                        "api_key" to getAccessToken(),
                        "id_number" to idNumber,
                    )
                }

            loggerUtil("VerifyID", verifyIdResponse)

            /**
             * @throws IllegalArgumentException
             */
            verifyIdResponse.Error?.let {
                throw HttpException(it)
            }

            return verifyIdResponse.Verification.let {
                it ?: throw HttpException(ErrorMessage.SERVICE_UNAVAILABLE)
            }
        } catch (err: Throwable) {
            throw HttpException(err.message, err.cause)
        }
    }

    /**
     * Verifies a given South African ID number against the live Home Affairs Database in real time
     * @return [VerifyIdRealTime]
     */
    suspend fun verifyIdInRealTime(): VerifyIdRealTime = runCatching {
        val endpoint = "$host/home_affairs_real_time_idv"
        val response: VerifyIdRealTimeResponse = httpClient.post(endpoint) {
            contentType(ContentType.Application.Json)
            body = mapOf(
                "api_key" to getAccessToken(),
                "id_number" to idNumber,
            )
        }
        loggerUtil("VerifyIDRealTime", response)
        /**
         * @throws IllegalArgumentException
         */
        response.Error?.let {
            throw HttpException(it)
        }
        response.RealTime_Verification.let {
            it ?: throw HttpException(ErrorMessage.SERVICE_UNAVAILABLE)
        }
    }.getOrElse {
        throw HttpException(it.message, it.cause)
    }

    /**
     * Verifies a given South African ID number against the live Home Affairs Databases with ID PHOTO
     * @param reason Your reason for doing the search
     * @return [VerifyIdPhoto]
     */
    suspend fun verifyIdPhoto(reason: String = idPhotoEnquiryReason): VerifyIdPhoto =
        runCatching {
            val endpoint = "$host/home_affairs_id_photo"
            val response: VerifyIDPhotoResponse = httpClient.post(endpoint) {
                contentType(ContentType.Application.Json)
                body = mapOf(
                    "api_key" to getAccessToken(),
                    "id_number" to idNumber,
                    "enquiry_reason" to reason
                )
            }

            loggerUtil("VerifyIDPhoto", response)
            /**
             * @throws IllegalArgumentException
             */
            response.Error?.let {
                throw HttpException(it)
            }
            response.realTimeResults.let {
                it ?: throw HttpException(ErrorMessage.SERVICE_UNAVAILABLE)
            }
        }.getOrElse {
            throw HttpException(it.message, it.cause)
        }

    /**
     * Checks a given South African ID number martial status against the live Home Affairs Databases
     * @return [VerifyMaritalStatus]
     */

    suspend fun checkMaritalStatus(): VerifyMaritalStatus =
        runCatching {
            val endpoint = "$host/home_affairs_id_photo"
            val response: VerifyMaritalStatusResponse = httpClient.post(endpoint) {
                contentType(ContentType.Application.Json)
                body = mapOf(
                    "api_key" to getAccessToken(),
                    "id_number" to idNumber,
                )
            }
            loggerUtil("VerifyIDPhoto", response)
            /**
             * @throws IllegalArgumentException
             */
            response.Error?.let {
                throw HttpException(it)
            }
            response.Marital_Status_Enquiry.let {
                it ?: throw HttpException(ErrorMessage.SERVICE_UNAVAILABLE)
            }
        }.getOrElse {
            throw HttpException(it.message, it.cause)
        }
}
