package integration.verifyID

import http.httpClient
import integration.verifyID.model.VerifyAuthResponseInterface
import io.ktor.client.request.post
import io.ktor.http.ContentType
import io.ktor.http.contentType

open class VerifyIdConfiguration {
    /**
     * username used on VerifyID
     * @var string
     */
    private val username: String = "moroa@milfoil.co.za"

    /**
     * password used on verifyID
     * @var string
     */
    val password: String = "Prof@816450"

    /**
     * verifyId web server address
     * @var string
     */
    val host: String = "https://www.verifyid.co.za/webservice"

    /**
     * Authenticate
     * validate given credentials and obtain access token
     * @return string
     */
    suspend fun getAccessToken(): String {
        try {
            val response: VerifyAuthResponseInterface = runCatching {
                httpClient.post<VerifyAuthResponseInterface>("$host/authenticate") {
                    contentType(ContentType.Application.Json)
                    body = mapOf(
                        "email_address" to username,
                        "password" to password
                    )
                }
            }.getOrElse {
                throw Error(it)
            }

            if (response.Status != "Failure") {
                // check whether there's access key
                if (response.Result.contains("API_KEY")) {
                    val key = response.Result["API_KEY"]
                    if (key.isNullOrEmpty()) {
                        throw Error(response.Error)
                    }
                    return key
                }
            }

            // bad request either username or password are missing
            throw Error(response.Error)
        } catch (ex: Throwable) {
            throw Error(ex.message)
        }
    }
}
