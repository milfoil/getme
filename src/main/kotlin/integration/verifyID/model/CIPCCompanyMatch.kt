package integration.verifyID.model

data class CIPCCompanyMatch(
    val CommercialID: String,
    val RegistrationNo: String,
    val Businessname: String,
    val EnquiryID: String,
    val EnquiryResultID: String
)


