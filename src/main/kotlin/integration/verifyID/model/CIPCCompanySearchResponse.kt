package integration.verifyID.model

data class CIPCCompanySearchResponse(
    val  CompanyMatches: CIPCCompanyMatch,
    val Status: String
)