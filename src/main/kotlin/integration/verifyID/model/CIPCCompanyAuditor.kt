package integration.verifyID.model

/**
 * AuditorInformation
 */
data class CIPCCompanyAuditor(
    val DisplayText: String,
    val AuditorName: String,
    val ProfessionNo: String,
    val ProfessionDesc: String,
    val AuditorTypeDesc: String,
    val AuditorStatusDesc: String,
    val LastUpdatedDate: String,
    val PostalAddress: String,
    val PhysicalAddress: String,
    val TelephoneNo: String,
    val YearswithAuditor: String,
    val ActStartdate: String,
    val ActEndDate: String,
    val NoOfYearsInbBusiness: String
)
