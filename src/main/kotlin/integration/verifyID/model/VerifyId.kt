package integration.verifyID.model

data class VerifyId(
    /**
     * Firstnames of the person
     * @var string
     */
    val Firstnames: String,
    /**
     * The family name of the person
     * @var string
     */
    val Lastname: String,
    /**
     * The birthdate of the person
     * @var string
     */
    val Dob: String,
    /**
     * The current age of the person
     * @var Int
     */
    val Age: String,
    /**
     * The gender of the person
     * @var string
     */
    val Gender: String,
    /**
     * The nationality of the person
     * @var string
     */
    val Citizenship: String,
    /**
     * The issue date for the person id card
     * @var string
     */
    val DateIssued: String
)
