package integration.verifyID.model

data class CIPCCompanyResponse(
    val Status: String,
    val Result: CIPCCompanyInfo
)

data class CIPCCompanyInfo(
    val CompanyInformation: CIPCCompany,
    val AuditorInformation: CIPCCompanyAuditor,
    val DirectorInformation: CIPCCompanyDirector
)
