package integration.verifyID.model

interface VerifyIdBaseResponse {
    val Status: String?
    val Error: String?
    val transaction_id: String?
}
