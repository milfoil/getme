package integration.verifyID.model

data class VerifyMaritalStatus(
    /**
     * Firstnames of the person
     * @var string
     */
    val FirstName: String,
    /**
     * The family name of the person
     * @var string
     */
    val Surname: String,
    /**
     * The birthdate of the person
     * @var string
     */
    val BirthDate: String,
    /**
     * The marital status of the person
     * @var string
     */
    val MaritalStatusDesc: String,
    /**
     * The marriage date of the person
     * @var string
     */
    val MarriageDate: String
)
