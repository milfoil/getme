package integration.verifyID.model

data class VerifyIdRealTime(
    /**
     * Id number of the person
     * @var string
     */
    val idNumber: String,
    /**
     * Firstnames of the person
     * @var string
     */
    val firstNames: String,
    /**
     * The family name of the person
     * @var string
     */
    val surName: String,
    /**
     * The birthdate of the person
     * @var string
     */
    val dob: String,
    /**
     * The current age of the person
     * @var Int
     */
    val age: Int,
    /**
     * The gender of the person
     * @var string
     */
    val gender: String,
    /**
     * The nationality of the person
     * @var string
     */

    val citizenship: String,
    /**
     * The deceased status for a person
     * @var string
     */
    val deceasedStatus: String,
    /**
     * The deceased date of a person
     * @var string
     */
    val deceasedDate: String,
)
