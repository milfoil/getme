package integration.verifyID.model

data class VerifyIdPhoto(
    /**
     * Id number of the person
     * @var string
     */
    val idNumber: String,

    /**
     * Firstnames of the person
     * @var string
     */
    val firstNames: String,

    /**
     * The family name of the person
     * @var string
     */
    val surname: String,

    /**
     * The birthdate of the person
     * @var string
     */
    val dob: String,

    /**
     * The current age of the person
     * @var Int
     */
    val age: Int,

    /**
     * The gender of the person
     * @var string
     */
    val gender: String,

    /**
     * The deceased status for a person
     * @var string
     */
    val deceasedStatus: String,

    /**
     * The photo of the person
     */
    val IDPhoto: String
)
