package integration.verifyID.model

data class CIPCCompanySearchRequest(
    /**
     * Api key obtained from the authentication function
     */
    val api_key: String,
    /**
     * Company registration number portion 1 - optional if Company name has been provided
     */
    val reg_1: String? = null,
    /**
     * Company registration number portion 2 - optional if Company name has been provided
     */
    val reg_2: String? = null,
    /**
     * Company registration number portion 3 - optional if Company name has been provided
     */
    val reg_3: String? = null,
    /**
     *  Company Name
     */
    val company_name: String,
)
