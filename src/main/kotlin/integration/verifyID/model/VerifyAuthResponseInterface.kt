package integration.verifyID.model

data class VerifyAuthResponseInterface(
    val Status: String,
    val Result: Map<String, String>,
    val Error: String? = null
)
