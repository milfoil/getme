package integration.verifyID.model

data class CIPCCompanyRequest(
    val EnquiryID: String,
    val EnquiryResultID: String
)
