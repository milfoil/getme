package integration.verifyID.model

data class VerifyIDResponse(
    override val Status: String? = null,
    override val Error: String? = null,
    override val transaction_id: String? = null,
    val Verification: VerifyId? = null,
) : VerifyIdBaseResponse

data class VerifyIDPhotoResponse(
    override val Status: String? = null,
    override val Error: String? = null,
    override val transaction_id: String? = null,
    val realTimeResults: VerifyIdPhoto? = null,
) : VerifyIdBaseResponse

data class VerifyMaritalStatusResponse(
    override val Status: String? = null,
    override val Error: String? = null,
    override val transaction_id: String? = null,
    val Marital_Status_Enquiry: VerifyMaritalStatus? = null,
) : VerifyIdBaseResponse

data class VerifyIdRealTimeResponse(
    override val Status: String? = null,
    override val Error: String? = null,
    override val transaction_id: String? = null,
    val RealTime_Verification: VerifyIdRealTime? = null,
) : VerifyIdBaseResponse
