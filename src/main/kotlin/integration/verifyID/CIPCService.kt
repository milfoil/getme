package integration.verifyID

import http.HttpException
import http.httpClient
import integration.verifyID.model.CIPCCompanyRequest
import integration.verifyID.model.CIPCCompanyResponse
import integration.verifyID.model.CIPCCompanySearchRequest
import integration.verifyID.model.CIPCCompanySearchResponse
import io.ktor.client.request.post
import io.ktor.http.ContentType
import io.ktor.http.contentType
import utils.loggerUtil

/**
 * The API service provides an efficient and fast way to retrieve commercial company principle (Directorship in different companies) information
 * from CIPC (Companies and Intellectual Properties Commission)
 */
class CIPCService : VerifyIdConfiguration() {

    /**
     * CIPC - Companies and Intellectual Property Commission
     *  Search the CIPC Database to match results before final search
     *   Cost : 0 Credit
     */
    suspend fun companySearch(companyName: String, registrationNumber: String? = null) = runCatching {
        val registrationNumber = registrationNumber?.let { it.split("/") }
        println(registrationNumber)
        val response: CIPCCompanySearchResponse =
            httpClient.post("$host/cipc_company_match") {
                contentType(ContentType.Application.Json)
                body = CIPCCompanySearchRequest(
                    api_key = getAccessToken(),
                    reg_1 = registrationNumber?.get(0),
                    reg_2 = registrationNumber?.get(1),
                    reg_3 = registrationNumber?.get(2),
                    company_name = companyName
                )
            }

        loggerUtil("CIPC COMPANY MATCH", response)
        response

        // /**
        //  * @throws IllegalArgumentException
        //  */
        // verifyIdResponse.Error?.let {
        //     throw HttpException(it)
        // }

        // return verifyIdResponse.Verification.let {
        //     it ?: throw HttpException(ErrorMessage.SERVICE_UNAVAILABLE)
        // }
    }.getOrElse {
        throw HttpException(it.message, it.cause)
    }

    /**
     * The API service  retrieve commercial company principle (Directorship in different companies) information from
     * CIPC (Companies and Intellectual Properties Commission)
     * Cost : 25 Credit
     */
    suspend fun getCompany(request: CIPCCompanyRequest) = runCatching {
        val response: CIPCCompanyResponse =
            httpClient.post("$host/cipc_company_search") {
                contentType(ContentType.Application.Json)
                body = mapOf(
                    "api_key" to getAccessToken(),
                    "EnquiryID" to request.EnquiryID,
                    "EnquiryResultID" to request.EnquiryResultID
                )
            }

        loggerUtil("CIPC COMPANY RESPONSE", response)
        response

    }.getOrElse {
        throw HttpException(it.message, it.cause)
    }
}