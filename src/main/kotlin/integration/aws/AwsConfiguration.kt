package integration.aws

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.regions.Region

object AwsConfiguration {

    /**
     * The AWS access key, used to identify the user interacting with AWS.
     * @var accessKeyId
     */
    val accessKeyId: String = "AKIA53CC5B3MITPCZZGA"
    /**
     * The AWS secret access key, used to authenticate the user interacting with AWS.
     * @var secretAccessKey
     * */
    val secretAccessKey: String = "XXfOkZw7uhN2x2qoGZRtiDbu3EpgTmSi342rrsjr"
    val userPoolClientId: String = "3kldol06ru98pub2vqkhucb0g1"
    val userPoolClientSecretKey: String = "1a64gt4l744479cfve48l7g7l61vbfd3rg50pl2ckbpl0ilem13c"
    val region: Region = Region.US_EAST_1
    val credential: AwsBasicCredentials = AwsBasicCredentials.create(
        accessKeyId,
        secretAccessKey
    )
}
