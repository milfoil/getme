package integration.aws

import http.HttpException
import kotlinx.coroutines.future.await
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.awscore.exception.AwsServiceException
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderAsyncClient
import software.amazon.awssdk.services.cognitoidentityprovider.model.AttributeType
import software.amazon.awssdk.services.cognitoidentityprovider.model.AuthFlowType
import software.amazon.awssdk.services.cognitoidentityprovider.model.CognitoIdentityProviderException
import software.amazon.awssdk.services.cognitoidentityprovider.model.ConfirmForgotPasswordRequest
import software.amazon.awssdk.services.cognitoidentityprovider.model.ConfirmForgotPasswordResponse
import software.amazon.awssdk.services.cognitoidentityprovider.model.ConfirmSignUpRequest
import software.amazon.awssdk.services.cognitoidentityprovider.model.ConfirmSignUpResponse
import software.amazon.awssdk.services.cognitoidentityprovider.model.ForgotPasswordRequest
import software.amazon.awssdk.services.cognitoidentityprovider.model.ForgotPasswordResponse
import software.amazon.awssdk.services.cognitoidentityprovider.model.InitiateAuthRequest
import software.amazon.awssdk.services.cognitoidentityprovider.model.InitiateAuthResponse
import software.amazon.awssdk.services.cognitoidentityprovider.model.ResendConfirmationCodeRequest
import software.amazon.awssdk.services.cognitoidentityprovider.model.ResendConfirmationCodeResponse
import software.amazon.awssdk.services.cognitoidentityprovider.model.SignUpRequest
import software.amazon.awssdk.services.cognitoidentityprovider.model.SignUpResponse

object AwsCognitoServiceProvider {
    private val configuration: AwsConfiguration = AwsConfiguration
    private val webserver: CognitoIdentityProviderAsyncClient =
        CognitoIdentityProviderAsyncClient.builder()
            .region(configuration.region)
            .credentialsProvider(
                StaticCredentialsProvider.create(configuration.credential)
            ).build()

    /**
     * SignUp
     * Registers the user in the specified user pool
     */
    suspend fun signUp(username: String, password: String): SignUpResponse {
        try {
            val phoneNumber = "+27${username.drop(1)}"
            return webserver.signUp(
                SignUpRequest.builder()
                    .clientId(configuration.userPoolClientId)
                    .username(username).password(password)
                    .userAttributes(
                        mutableListOf(
                            AttributeType.builder()
                                .name("phone_number").value(phoneNumber).build()
                        )
                    ).build()
            ).await()
        } catch (err: CognitoIdentityProviderException) {
            throw HttpException(err.awsErrorDetails().errorMessage(), err.cause)
        } catch (err: AwsServiceException) {
            throw HttpException(err.awsErrorDetails().errorMessage(), err.cause)
        }
    }

    /**
     * ConfirmSignUp
     * Confirms registration of a user and handles the existing alias from a previous user.
     */
    suspend fun confirmSignUp(username: String, confirmationCode: String): ConfirmSignUpResponse {
        try {
            return webserver.confirmSignUp(
                ConfirmSignUpRequest.builder()
                    .clientId(configuration.userPoolClientId)
                    .username(username)
                    .confirmationCode(confirmationCode)
                    .build()

            ).await()
        } catch (err: CognitoIdentityProviderException) {
            throw HttpException(err.awsErrorDetails().errorMessage(), err.cause)
        } catch (err: AwsServiceException) {
            throw HttpException(err.awsErrorDetails().errorMessage(), err.cause)
        }
    }

    /**
     * ResendConfirmationCode
     * Resends the confirmation (for confirmation of registration) to a specific user in the user pool.
     * @param username - The user name of the user to whom you wish to resend a confirmation code.
     */
    suspend fun resendConfirmationCode(username: String): ResendConfirmationCodeResponse {
        try {
            return webserver.resendConfirmationCode(
                ResendConfirmationCodeRequest.builder()
                    .clientId(configuration.userPoolClientId)
                    .username(username)
                    .build()
            ).await()
        } catch (err: CognitoIdentityProviderException) {
            throw HttpException(err.awsErrorDetails().errorMessage(), err.cause)
        } catch (err: AwsServiceException) {
            throw HttpException(err.awsErrorDetails().errorMessage(), err.cause)
        }
    }

    /**
     * ForgotPassword
     * Calling this API causes a message to be sent to the end user with a confirmation code
     * that is required to change the user's password.
     *
     * @param username - The user name of the user for whom you want to
     *                  enter a code to reset a forgotten password.
     *
     */
    suspend fun forgotPassword(username: String): ForgotPasswordResponse {
        try {
            return webserver.forgotPassword(
                ForgotPasswordRequest.builder()
                    .clientId(configuration.userPoolClientId)
                    .username(username)
                    .build()
            ).await()
        } catch (err: CognitoIdentityProviderException) {
            throw HttpException(err.awsErrorDetails().errorMessage(), err.cause)
        } catch (err: AwsServiceException) {
            throw HttpException(err.awsErrorDetails().errorMessage(), err.cause)
        }
    }

    /**
     * ConfirmForgotPassword
     * Allows a user to enter a confirmation code to reset a forgotten password.
     */
    suspend fun confirmForgotPassword(
        username: String,
        password: String,
        confirmationCode: String
    ): ConfirmForgotPasswordResponse {
        try {
            return webserver.confirmForgotPassword(
                ConfirmForgotPasswordRequest.builder()
                    .clientId(configuration.userPoolClientId)
                    .username(username)
                    .password(password)
                    .confirmationCode(confirmationCode)
                    .build()
            ).await()
        } catch (err: CognitoIdentityProviderException) {
            throw HttpException(err.awsErrorDetails().errorMessage(), err.cause)
        } catch (err: AwsServiceException) {
            throw HttpException(err.awsErrorDetails().errorMessage(), err.cause)
        }
    }

    /**
     * Initiates the authentication flow.
     * @param username - The user name of the user for whom you want to access the application
     * @param password - The secret key of a user for whom you want to access the application
     */
    suspend fun initiateAuth(username: String, password: String): InitiateAuthResponse {
        try {
            return webserver.initiateAuth(
                InitiateAuthRequest.builder()
                    .clientId(configuration.userPoolClientId)
                    .authParameters(
                        mapOf(
                            "USERNAME" to username,
                            "PASSWORD" to password,
                        )
                    )
                    .authFlow(AuthFlowType.USER_PASSWORD_AUTH)
                    .build()
            ).await()
        } catch (err: CognitoIdentityProviderException) {
            throw HttpException(err.awsErrorDetails().errorMessage(), err.cause)
        } catch (err: AwsServiceException) {
            throw HttpException(err.awsErrorDetails().errorMessage(), err.cause)
        }
    }
}
