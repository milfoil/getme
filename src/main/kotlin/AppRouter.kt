import features.company.companyRouter
import features.notification.merchantRouter
import features.payment.paymentRouter
import features.transport.taxi.taxiRouter
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import user.userRouter

fun Application.appRouter() {
    routing {
        /**
         * user router
         */
        userRouter()

        /**
         * merchants router
         */
        merchantRouter()

        /**
         * payments router
         */
        paymentRouter()

        /**
         * taxi router
         */
        taxiRouter()

        /**
         *  company  router
         */
        companyRouter()

        get("/") {
            call.respondText("hello")
        }
    }
}
